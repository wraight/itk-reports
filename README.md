# Reporting Notes

Notes for python based reporting scripts to define, extract, format, visualise and distribute data from Atlas ITk Production Database (PDB).

> esse est percipi -- [George Berkeley](https://en.wikipedia.org/wiki/A_Treatise_Concerning_the_Principles_of_Human_Knowledge)  

The _mk-docs_ version of these notes: [here](https://itk-reports.docs.cern.ch).

### Required packages

- [itkdb](https://pypi.org/project/itkdb/#description) - accessing PDB
- [pandas](https://pandas.pydata.org) - wrangling extracted data
- [altair](https://altair-viz.github.io) - visualisating data
- [datapane](https://datapane.com) - distributing reports

You can find a repository of reports [here](https://itk-reporting-repository.docs.cern.ch).

---

## Contents

1. [Overview](#overview) - general structure of reporting
2. [Getting Started](#getting-started) - how to run
3. [Tutorial Notebooks](#tutorial-notebooks) - illustrate reporting structure
4. [Example Notebooks](#example-notebooks) - illustrate particular but common reports
5. [Advanced Notebooks](#advanced-notebooks) - illustrate particular but common reports - reading functions from _commonCode_ directories.
6. [Reporting Scripts](#reporting-scripts) - command line versions of particular but common reports - reading functions from _commonCode_ directories.
    - [Steering Files](#steering-files)
    - [Running Scripts](#running-scripts)

--- 

## Overview

The purpose of this repository is to aid ITk users to gather data from the ITk Production Database (PDB).

- not intended for large scale reporting (this is centrally coordinated)

The reporting scripts implemented here have four main parts:

1. Population Definition
    
    A collection of components is defined from user inputs and component codes retrieved from the PDB.

2. Data Extraction

    Using PDB component codes data and user inputs data is extracted from PDB and converted to formatted pandas dataframes.

3. Visualisation

    Ordered lists of tables (pandas) and plots (altair) are made from formatted dataframes.

4. Report Sharing

    Ordered lists are converted into local or remote (datapane) html reports.

Scripts use _itkdb_ to interface with the PDB via an API. Much of the data wrangling is done using _pandas_ dataframes. Plots are generated using _altair_ data visualisation package. Each package is well documented and supported (see links [above](#required-packages)).

---

## Getting Started

To run reports:

- clone repository:
> git clone https://gitlab.cern.ch/wraight/itk-reports
- navigate to repository:
> cd itk-reports
- use the same code from AUW tutorial (tag: AUW_Nov23)
> git checkout -b tutorial AUW_Nov23
- install requirements:
> python -m pip install -r requirements.txt
- open notebook in editor or run script

Suggested developement path:

- _basic notebooks_ some simple examples of reporting
- _tutorial notebooks_ to get idea of structure & funcitonality
- _example notebooks_ to see examples of various report types with function code
- _advanced notebooks_ to remove function code
- _reporting script_ translate (advanced) notebooks to run from commandline
- set up cron jobs (not included in tutorial)

---

## Basic Notebooks

Simple reporting examples

- basic retrieval - get components, identify tests, extract test data, review
- basic visualisation - using same testRun dataset as *basic_retrieval* example

---

## Tutorial Notebooks

Tutorial scripts can be found in *tutorial_notebooks* covering:

 - population definitions - *population_tutorial*
 - extraction specifications - *extraction_tutorial*
 - data visualisations - *visualisation_tutorial*
 - report distributions - *distribution_tutorial*

The notebooks are steered by the _settingDict_. See [below](#steering-files) for details.

---

## Example Notebooks

Several reporting tasks a ubiquitious across projects, work packages and activities.
Some example notebooks to generate reports can be found in *example_notebooks*.

Example notebooks:

 - Data Integrity:

    - checking data quality of PDB uploads per component type - *componentType_dashboard_tutorial*
    - checking data quality of PDB uploads per test type - *testType_dashboard_tutorial*

 - Overviews:

    - checking component locations, stage progression - *part_populations_dashboard_tutorial*
    - checking components at institution - *inventory_dashboard_tutorial*

 - Consistency:

    - comparing test parameters for correlations - *parameter_comparison_tutorial*
    - comparing consistency of test runs per component over production - *parameter_consistency_tutorial*

The notebooks are steered by the _settingDict_. See [below](#steering-files) for details.

---

## Advanced Notebooks

These notebooks duplicate the [example notebooks](#example-notebooks) but use the function definitions in the _commonCode_ directory

- tidier notebooks
- closer to commandLine running
- helpful to debugging _commoncode_ functions


---

## Reporting Scripts

Examples of scripts to run directly form the commandline are provided in the *reporting_scripts*  directory. These are equivalent to the [example notebooks](#example-notebooks).


The scripts are quite generic. Reporting is made specific using a steering file to define the report details. A _json_ formatted file is used, examples are found in *reporting_specs*.

### Steering Files

An example template is provided in *reporting_specs/report_template.json*

The steering file sections reflect the general reporting strategy:

1. __population__ - a list of specifications to find components
    - each element has a _spec_ dictionary to define components
        - a project code (_projCode_)
        - component type code (_compTypeCode_) to select components at institutions
        - further specification can be made using a cluster code (_clusCode_), institution code (_instCode_), list of institutions (_instList_) or list of component PDB _serialNumbers_  or _codes_ (_compList_)
        - any additional filters (_filters_) to be applied, e.g. child component specifications
    - a label (_alias_) is used to identify elements in the list
2. __extraction__ - a list of specifications to extract parameters from component test results
    - each element has a _spec_ list in case more than one test type is required per component type. Each element of the list is a dictionary to define test run parameters to extract per test type
        - a test type code (_testcode_)
        - a parameter code (_paraCode_)
        - a list of sub-parameter codes or list positions (_subs_)
    - a label (_usePopulations_) is used to associate data extraction to components defined in __population__
    - a label (_alias_) is used to identify elements in the list
3. __visualisation__ - a list of visualisation specifications
    - each element can toggle predefined data quality control plots (_DQ_). These will produce upload timelines, histograms and tables for each parameter defined in __data__
    - a list of custom plots (_custom_) can be defined. For each element a dictionary is defined to map parameters to (_altair_) visualisation channels: minimum _x_ & _y_ must be defined; other channels supported include _color_, _size_ & _shape_.
    - a label (_useExtractions_) is used to associate data extraction to components defined in __extraction__
    - a label (_alias_) is used to identify elements in the list
4. __distribution__ - a list of report specifications
    - the destination of the report can be set (_location_) to be to the _local_ files system or to _remote_ (datapane) servers
    - a report name (_reportName_) is used to identify the report
    - a list of visualisations (_useVisualisations_) to include in the report
    - if a _remote_ report is required, a _datapane_ token must be provided (_datapaneCode_)
    - if an _local_ report is required, a local directory can be provided (_reportDir_)

### Running Scripts

Scripts can be run from the repository's top directory. 

To run:

- setup environment credentials: AC1, AC2
- run report type with spec file using *--file* argument

Example command to run testType report:

> export AC1=ACCESS_CODE1

> export AC2=ACCESS_CODE2

> python reporting_scripts/testType_dashboard.py --file reporting_specs/testType_dashboard_example.json

---

## Reporting Repository

Several automated reports can be found [ITk reporting repository](https://itk-reporting-repository.docs.cern.ch/)

Reports collected by:

- general
- pixels
- strips
- tests

