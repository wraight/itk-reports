### get itkdb for PDB interaction
import os
import sys
import pandas as pd
import altair as alt
import numpy as np
import copy
import itkdb
import itkdb.exceptions as itkX
###


# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/commonCode')
from commonSpecReader import *
from commonCredentials import *
from commonPopulation import *
from commonExtraction import *
from commonVisualisation import *
from commonDistribution import *


#######
# main
#######
if __name__ == '__main__':

    settingDict=[]

    args=GetArgs('componentType dashboard')
    chunk = 100
    if args.chunk!=None:
        chunk=args.chunk

    if args.file==None:
        print("No specification file provide.")
        print("Please provide spec file. See \"$(pwd)/testSpecs\" for examples")
        exit(0)
        # print(args)
    else:
        print("Reading specification file")
        settingDict=GetSpecFromFile(args.file)
        # print("\n### Initial settingDict ###\n",settingDict)
        # add any commandline stuff
        if args.keys!=None:
            settingDict=UpdateSpec(settingDict,args)
            if settingDict==None:
                print("Issue with spec update :(")
                exit(0)

    ### copy original dictionary to upload to report later
    origDict=copy.deepcopy(settingDict)

    print("\n### Final settingDict ###\n")
    print(json.dumps(settingDict, indent=4))

    ### PDB Client
    myClient=SetClient()

    #######
    # get components
    #######

    ### collect populations
    for pop in settingDict['population']:
        print(f"working on pop: {pop['alias']}")
        compInfo=[]
        ### if compList is available use it (priority)
        if "compList" in pop['spec'].keys():
            print(f"found component list: {pop['spec']['compList']}")
            compInfo=[{'code': x} for x in pop['spec']['compList']]
        elif "batchId" in pop['spec'].keys():
            print(f"found batchId: {pop['spec']['batchId']}")
            compInfo=[x for x in myClient.get('getBatch', json={'id':pop['spec']['batchId']})['components']]
        elif "batchType" in pop['spec'].keys() and "batchNumber" in pop['spec'].keys():
            print(f"found batchNumber: {pop['spec']['batchNumber']} ({pop['spec']['batchType']})")
            compInfo=[x for x in myClient.get('getBatchByNumber', json={'project':pop['spec']['projCode'],'batchType':pop['spec']['batchType'],'number':pop['spec']['batchNumber']})['components']]
        elif "file" in pop['spec'].keys():
            df_csv=pd.read_csv(pop['spec']['file'])
            compInfo=[{'code': x} for x in df_csv['serialNumber'].to_list()]
        ### else use XXXcode
        else:
            if 'clusCode' in pop['spec'].keys():
                print(f"found clusCode: {pop['spec']['clusCode']}")
                instList=GetClusterInstitutes(myClient, pop['spec']['clusCode'])
            elif 'instCode' in pop['spec'].keys():
                print(f"found instCode: {pop['spec']['instCode']}")
                instList=pop['spec']['instCode']
            else:
                print("no compList, instCode or clusCode found. Using project code")
                instList=GetProjectInstitutes(myClient, pop['spec']['projCode'])
            compInfo=GetComponentInfo(myClient, instList, pop['spec'])
        pop['compInfo']=compInfo
        print("===============")
        print(f"Found components for {pop['alias']}: {len(pop['compInfo'])}")
        print("===============\n")


    if sum([len(pop['compInfo']) for pop in settingDict['population'] if "compInfo" in pop.keys()])<0:
        print("no components found in any populations. exiting")
        exit(0)

    #######
    # get the component IDs
    #######

    ### get component IDs
    for ext in settingDict['extraction']:
        print(f"working on ext: {ext['alias']}")
        componentInfo=[]
        for pop in settingDict['population']:
            if pop['alias'] in ext['usePopulations']:
                print(f"found pop: {pop['alias']}")

                components=GetComponentData(myClient, [x['code'] for x in pop['compInfo']], 100)
                if len(components)<1:
                    print(f"no components info for: {pop['alias']}")
                else:
                    componentInfo.extend(components)
        
        ext['componentInfo']=componentInfo
        print("===============")
        print(f"components for {ext['alias']} : {len(ext['componentInfo'])}")
        print("===============\n")


    #######
    # formatting
    #######

    ### list for matching testRuns
    for ext in settingDict['extraction']:
        print(f"working on ext: {ext['alias']}")

        if "compSummary" in ext.keys():
            compInfo=[]
            ### list for matching testRuns
            print(f"- Component Summary")
            df_compInfo=FormatComponentData(ext['componentInfo'], False)
            ext['df_compInfo']=df_compInfo
            print("===============")
            print(f"df_compInfo for {ext['alias']}: {ext['df_compInfo'].columns}")
            print("===============\n")

        if "stageSummary" in ext.keys():
            stageInfo=[]
            ### list for matching testRuns
            print(f"- Stage Summary")
            df_stageInfo=FormatStageData(ext['componentInfo'])
            ext['df_stageInfo']=df_stageInfo                        
            print("===============")
            print(f"stageSummary info. for {ext['alias']}: {ext['df_stageInfo'].count()}")
            print("===============\n")


    #######
    # plotting
    #######

    ### list for matching testRuns
    for vis in settingDict['visualisation']:
        print(f"working on vis: {vis['alias']}")

        vis['standard_comp_plots']=[]
        df_compInfo=pd.DataFrame()
        vis['standard_stage_plots']=[]
        df_stageInfo=pd.DataFrame()
        for ext in settingDict['extraction']:
            if ext['alias'] in vis['useExtractions']:
                print(f"found ext:{ext['alias']}")

                if "compSummary" in vis.keys():
                    if df_compInfo.empty:
                        df_compInfo=ext['df_compInfo']
                    else:
                        df_compInfo=pd.concat([df_compInfo,ext['df_compInfo']])

                if "stageSummary" in vis.keys():
                    if df_stageInfo.empty:
                        df_stageInfo=ext['df_stageInfo']
                    else:
                        df_stageInfo=pd.concat([df_stageInfo,ext['df_stageInfo']])

        if not df_compInfo.empty:
            print("getting plots...")
            if "clusInfo" in vis.keys():
                print("retrieving cluster info for Overview plots")
                vis['standard_over_plots']=OverviewSummary(myClient, df_compInfo, None, vis['clusInfo'])
                vis['standard_comp_plots']=ComponentSummary(myClient, df_compInfo, None, vis['clusInfo'])
            else:
                vis['standard_over_plots']=OverviewSummary(myClient, df_compInfo)
                vis['standard_comp_plots']=ComponentSummary(myClient, df_compInfo)
        
        print("===============")
        print(f"compSummary info. for {vis['alias']}: {len(vis['standard_comp_plots'])}")
        print("===============\n")

        if not df_stageInfo.empty:
            print("getting plots...")
            vis['standard_stage_plots']=StageSummary(myClient, df_stageInfo)
            # vis['standard_stage_plots'].extend(StageTimeLines(df_stageInfo))
        
        print("===============")
        print(f"stageSummary info. for {vis['alias']}: {len(vis['standard_stage_plots'])}")
        print("===============\n")



    #######
    # sharing
    #######

    for dis in settingDict['distribution']:
        print(f"working on {dis['alias']}")
        # print(dis.keys())
        specChain={'distribution':dis, 'useVisualisations':[]}
        standard_plots=[]
        custom_plots=[]
        dic_plots={}
        for vis in settingDict['visualisation']:
            if vis['alias'] in dis['useVisualisations']:
                print(f"found vis:{vis['alias']}")
                for pk in [k for k in vis.keys() if "_plots" in k]:
                    ### standard plot keys
                    if "standard_over" in pk:
                        print(f"found {len(vis[pk])} {pk} standard plots")
                        standard_plots.insert(0, *vis[pk])
                    elif "standard" in pk:
                        print(f"found {len(vis[pk])} {pk} standard plots")
                        standard_plots.extend(vis[pk])
                    ### custom plot keys
                    elif "custom" in pk:
                        print(f"found {len(vis[pk])} {pk} custom plots")
                        custom_plots.extend(vis[pk])
                    ### other?
                    else:
                        print(f"don't recognise {len(vis[pk])} {pk} plots")
        dic_plots['standard_plots']=standard_plots
        dic_plots['custom_plots']=custom_plots
        
        # print(json.dumps(origDict, indent=4))
        DataPaneChunk(myClient, dic_plots['standard_plots'], dic_plots['custom_plots'], dis, origDict)
