### get itkdb for PDB interaction
import os
import sys
import pandas as pd
import altair as alt
import numpy as np
import copy
import itkdb
import itkdb.exceptions as itkX
###


# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/commonCode')
from commonSpecReader import *
from commonCredentials import *
from commonPopulation import *
from commonExtraction import *
from commonVisualisation import *
from commonDistribution import *


#######
# main
#######
if __name__ == '__main__':

    settingDict=[]

    args=GetArgs('testType dashboard')
    chunk = 100
    if args.chunk!=None:
        chunk=args.chunk

    if args.file==None:
        print("No specification file provide.")
        print("Please provide spec file. See \"$(pwd)/testSpecs\" for examples")
        exit(0)
        # print(args)
    else:
        print("Reading specification file")
        settingDict=GetSpecFromFile(args.file)
        # print("\n### Initial settingDict ###\n",settingDict)
        # add any commandline stuff
        if args.keys!=None:
            settingDict=UpdateSpec(settingDict,args)
            if settingDict==None:
                print("Issue with spec update :(")
                exit(0)

    ### copy original dictionary to upload to report later
    origDict=copy.deepcopy(settingDict)

    print("\n### Final settingDict ###\n")
    print(json.dumps(settingDict, indent=4))

    ### PDB Client
    myClient=SetClient()

    #######
    # get components
    #######

    ### collect populations
    for pop in settingDict['population']:
        print(f"working on pop: {pop['alias']}")
        compInfo=[]
        ### if compList is available use it (priority)
        if "compList" in pop['spec'].keys():
            print(f"found component list: {pop['spec']['compList']}")
            compInfo=[{'code': x} for x in pop['spec']['compList']]
        elif "batchId" in pop['spec'].keys():
            print(f"found batchId: {pop['spec']['batchId']}")
            compInfo=[x for x in myClient.get('getBatch', json={'id':pop['spec']['batchId']})['components']]
        elif "batchType" in pop['spec'].keys() and "batchNumber" in pop['spec'].keys():
            print(f"found batchNumber: {pop['spec']['batchNumber']} ({pop['spec']['batchType']})")
            compInfo=[x for x in myClient.get('getBatchByNumber', json={'project':pop['spec']['projCode'],'batchType':pop['spec']['batchType'],'number':pop['spec']['batchNumber']})['components']]
        elif "file" in pop['spec'].keys():
            df_csv=pd.read_csv(pop['spec']['file'])
            compInfo=[{'code': x} for x in df_csv['serialNumber'].to_list()]
        ### else use XXXcode        
        else:
            if 'clusCode' in pop['spec'].keys():
                print(f"found clusCode: {pop['spec']['clusCode']}")
                instList=GetClusterInstitutes(myClient, pop['spec']['clusCode'])
            elif 'instCode' in pop['spec'].keys():
                print(f"found instCode: {pop['spec']['instCode']}")
                instList=pop['spec']['instCode']
            else:
                print("no compList, instCode or clusCode found. Using project code")
                instList=GetProjectInstitutes(myClient, pop['spec']['projCode'])
            compInfo=GetComponentInfo(myClient, instList, pop['spec'])
        pop['compInfo']=compInfo
        print("===============")
        print(f"Found components for {pop['alias']}: {len(pop['compInfo'])}")
        print("===============\n")


    if sum([len(pop['compInfo']) for pop in settingDict['population'] if "compInfo" in pop.keys()])<0:
        print("no components found in any populations. exiting")
        exit(0)

    #######
    # get the testRun IDs
    #######

    for ext in settingDict['extraction']:
        print(f"working on ext: {ext['alias']}")
        compTestRunsInfo=[]
        for pop in settingDict['population']:
            if pop['alias'] in ext['usePopulations']:
                print(f"found pop:{pop['alias']}")

                ### get test IDs
                if "spec" in ext.keys():
                    compTestRuns=GetTestRunIDs(myClient, [x['code'] for x in pop['compInfo']], 100)
                    if len(compTestRuns)<1:
                        print(f"no test info for: {pop['alias']}")
                    else:
                        compTestRunsInfo.append(compTestRuns)
        
        ext['compTestRunsInfo']=compTestRunsInfo
        print("===============")
        print(f"testTypes for {ext['alias']} : {sum([len(ctri.keys()) for ctri in ext['compTestRunsInfo']])}")
        print("===============\n")

    #######
    # get the testRun data
    #######

    ### list for matching testRuns
    for ext in settingDict['extraction']:
        print(f"working on ext: {ext['alias']}")
        matchedTestRuns=[]
        # get testTypes defined in settings
        if "spec" in ext.keys():
            if ext['spec']==None:
                print("skipping matchedTestRuns")
                matchedTestRuns=None
            elif type(ext['spec'])==type("str") and  ext['spec'].lower()=="all":
                print("use *all* for matchedTestRuns")
                for ctri in ext['compTestRunsInfo']:
                    print(ctri)
                    for k,v in ctri.items():
                        print(k)
                        matchedTestRuns.extend(v)
            elif type(ext['spec'])==type([]):
                print("use subset for matchedTestRuns")
                for tc in set([x['testCode'] for x in ext['spec']]):
                    for ctri in ext['compTestRunsInfo']:
                        try:
                            matchedTestRuns.extend(ctri[tc])
                            print(f"found testType: {tc}")
                        except KeyError:
                            print(f"no matching testType: {tc}")
                            continue
            else:
                print("don't understand extraction spec:",ext['spec'])
        else:
            matchedTestRuns=None
        ### get test run data
        if matchedTestRuns==None:
            print(f"skipping testRuns for {ext['alias']}")
        else:
            testInfo=GetTestRunsData(myClient, matchedTestRuns)
            ext['testInfo']=testInfo
            print("===============")
            print(f"testRuns for {ext['alias']}: {len(ext['testInfo'])}")
            print("===============\n")

    if sum([len(ext['testInfo']) for ext in settingDict['extraction'] if "testInfo" in ext.keys()])<1:
        print("no testRun data found in any extractions. exiting")
        sys.exit()

    #######
    # formatting
    #######

    ### list for matching testRuns
    for ext in settingDict['extraction']:
        print(f"working on ext: {ext['alias']}")
        df_testRuns=FormatTestRunData(ext['testInfo'])
        ext['df_testRuns']=df_testRuns
        print("===============")
        print(f"df_testRuns for {ext['alias']}: {ext['df_testRuns'].columns}")
        print("===============\n")
    
    if any([ext['df_testRuns'].empty for ext in settingDict['extraction']]):
        print("no formatted data found in any extractions. exiting")
        sys.exit()

    #######
    # plotting
    #######

    ### standard plots (per institute)
    for vis in settingDict['visualisation']:
        print(f"working on vis: {vis['alias']}")
        
        df_testRuns=pd.DataFrame()
        customPlots=[]
        for ext in settingDict['extraction']:
            if ext['alias'] in vis['useExtractions']:
                print(f"found ext:{ext['alias']}")

                if "DQ" in vis.keys():
                    if df_testRuns.empty:
                        df_testRuns=ext['df_testRuns']
                    else:
                        pd.concat([df_testRuns])
                
                if "custom" in vis.keys():
                    for cus in vis['custom']:
                        # display(ext['df_testRuns'])
                        cusPlot=MakeCustomChart(ext['df_testRuns'],cus)
                        # display(cusPlot)
                        customPlots.append(cusPlot)
        
        standardPlots=DQPlotting_testRuns(myClient, df_testRuns)
        vis['standard_plots']=standardPlots
        print("===============")
        print(f"standard plots for {vis['alias']}: {len(vis['standard_plots'])}")
        print("===============\n")
                
        vis['custom_plots']=customPlots
        print("===============")
        print(f"custom plots for {vis['alias']}: {len(vis['custom_plots'])}")
        print("===============\n")


    if sum([len(vis['standard_plots']) for vis in settingDict['visualisation'] if "standard_plots" in vis.keys()])<1:
        if sum([len(vis['custom_plots']) for vis in settingDict['visualisation'] if "custom_plots" in vis.keys()])<1:
            print("no plots found in any visualisation. exiting")
            sys.exit()
        
    #######
    # sharing
    #######

    for dis in settingDict['distribution']:
        print(f"working on {dis['alias']}")
        # print(dis.keys())
        specChain={'distribution':dis, 'useVisualisations':[]}
        standard_plots=[]
        custom_plots=[]
        for vis in settingDict['visualisation']:
            if vis['alias'] in dis['useVisualisations']:
                print(f"found vis:{vis['alias']}")
                for pk in [k for k in vis.keys() if "_plots" in k]:
                    ### standard plot keys
                    if "standard" in pk:
                        print(f"found {len(vis[pk])} {pk} standard plots")
                        standard_plots.extend(vis[pk])
                    ### custom plot keys
                    elif "custom" in pk:
                        print(f"found {len(vis[pk])} {pk} custom plots")
                        custom_plots.extend(vis[pk])
                    ### other?
                    else:
                        print(f"don't recognise {len(vis[pk])} {pk} plots")
        # print(json.dumps(origDict, indent=4))
        DataPaneChunk(myClient, standard_plots, custom_plots, dis, origDict)
