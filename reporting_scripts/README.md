# Example scripts

Scripts with examples of report generation.

### Required packages

- [itkdb](https://pypi.org/project/itkdb/#description)
- [pandas](https://pandas.pydata.org)
- [altair](https://altair-viz.github.io)
- [datapane](https://datapane.com)

---

## Reporting examples

Example notebooks...

Dashboards:

 - **testType dashboard**: checking data quality of PDB uploads per test type
     - population: strips (S) MODULEs at Glasgow (GL)
     - extraction: 
         - testType: MODULE_METROLOGY
     - visualisation: data quality plots
     - distribution: local

 - **componentType dashboard**: checking component locations, stage progression, quality control
    - population: strips (S) MODULEs at Rutherford (RAL)
    - extraction: 
        - component summary information
        - stage summary information
        - test summary information
    - visualisation: 
        - component summary plots
        - stage summary plots
        - test summary plots
    - distribution: local

 - **inventory dashboard**: checking components at institution
    - population: strips (S) components at Oxford (OX)
    - extraction: 
        - stages
        - tests
    - visualisation: stages & tests
    - distribution: local

 Comparisons:
 
 - **parameter comparison**: comparing test parameters for correlations
     - population: strips (S) MODULEs at Rutherford (RAL) and child SENSORs
     - extraction: 
         - parent: MODULE_METROLOGY_>PB_POSITION_>PB_P1[0]
         - child: ATLAS18_SHAPE_METROLOGY_V1->BOWING
     - visualisation: data quality plots and comparison
     - distribution: local

 - **parameter consistency**: comparing consistency of test runs per component over production
     - population: strips (S) MODULEs at Glasgow (GL) and child SENSORs
     - extraction: 
         - parent: MODULE_IV_PS_V1->VOLTAGE, MODULE_IV_PS_V1->CURRENT
         - child: ATLAS18_IV_TEST_V1->VOLTAGE, ATLAS18_IV_TEST_V1->CURRENT
     - visualisation: per component plots (serve as data quality) and parent-child comparison
     - distribution: local

Reports are steered using a spec (_json_) file. Examples are found in the *reporting_specs* directory.

## Running Scripts

__NB__ You will need to export your credentials:
> export ac1=ACCESS_CODE1
<br> export ac2=ACCESS_CODE2

Running reporting scripts from the commandline follow the pattern:
> python [reporting_script_name] --file [steering_file_name]

For example, running *testType_dashboard* report (from repository top directory)
> python reporting_scripts/testType_dashboard.py --file reporting_specs/testType_dashboard_example.json

Hopefully the output ends with:
> ### Report Spec
> Saving locally.
<br> No reportDir specified. Saving locally.
<br> App saved to ./GL_mod_met_DQ_local.html

You should now be able to open the report via a browser, e.g.:
> open ./GL_mod_met_DQ_local.html

To make changes to the steering file _on-the-fly_:
> python [reporting_script_name] --file [steering_file_name] --keys [formatted list of keys to replace (space separated)] --vals [corresponding list of values (space seperated)]

For example, changing the *reportName* (i.e. a key in the _0th_ element of _distribution_ list):
>  python reporting_scripts/testType_dashboard.py --file reporting_specs/testType_dashboard_example.json --keys distribution#0:reportName --vals thisGuy

Hopefully the output ends with:
> ### Report Spec
> Saving locally.
<br> No reportDir specified. Saving locally.
<br> App saved to ./thisGuy.html


<!-- ```python
def mthfunc():
    print("thing")
    return None
``` -->