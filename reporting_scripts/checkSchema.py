### 
import jsonschema 
import argparse
import json

###############
### read-in parts
###############

### standard inputs
def GetArgs(descStr=None):
    my_parser = argparse.ArgumentParser(description=descStr)
    # Add the arguments
    my_parser.add_argument('--file', '-f', type=str, help='input settings file to check')
    my_parser.add_argument('--ref', '-r', action='store_true', help='Show reference')

    args = my_parser.parse_args()
    return args


def GetSchema(schemaPath=None):
    ### get json
    combSchema=None
    with open(schemaPath, 'r') as setJson:
        combSchema=json.load(setJson)
    return combSchema


### Validation function
def CheckJson(inJson, refSchema):
    ### check the json
    try:
        jsonschema.validate(inJson, schema=refSchema)
        print("- Validation is PASSED!")
        return True
    except jsonschema.ValidationError as e:
        print(" - Validation has FAILED:",e.schema.get("error_msg", e.message))
    
    return False


#######
# main
#######
if __name__ == '__main__':

    ### get arguments
    args=GetArgs('Check json schema')

    ### read-in schema file
    schemaPath="../reporting_specs/settingSchema.json"
    combSchema=GetSchema(schemaPath)

    ### print reference schema
    if args.ref:
        print("### Reference schema")
        print(json.dumps(combSchema, indent=4))
        exit(1)

    ### do validation
    if args.file==None:
        print("No file to check. Please input json.")
    else:        
        print("Validate input json...")
        with open(args.file, 'r') as inJson:
            CheckJson(json.load(inJson), combSchema)

    print("Done")
    exit(1)

