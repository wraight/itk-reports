### get itkdb for PDB interaction
import os
import sys
import numpy as np
import copy
import datetime
import json
# access ITk PDB
import itkdb
import itkdb.exceptions as itkX
### visualisation
import pandas as pd
import altair as alt
### distribution
import datapane as dp
### common code
# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/commonCode')
from commonSpecReader import *
from commonCredentials import *
from commonPopulation import *
from commonExtraction import *
from commonVisualisation import *
from commonDistribution import *
from commonAUX import *


#######
# main
#######
if __name__ == '__main__':

    settingDict=[]

    args=GetArgs('testType dashboard')
    chunk = 100
    if args.chunk!=None:
        chunk=args.chunk

    if args.file==None:
        print("No specification file provide.")
        print("Please provide spec file. See \"$(pwd)/testSpecs\" for examples")
        exit(0)
        # print(args)
    else:
        print("Reading specification file")
        settingDict=GetSpecFromFile(args.file)
        # print("\n### Initial settingDict ###\n",settingDict)
        # add any commandline stuff
        if args.keys!=None:
            settingDict=UpdateSpec(settingDict,args)
            if settingDict==None:
                print("Issue with spec update :(")
                exit(0)

    ### copy original dictionary to upload to report later
    origDict=copy.deepcopy(settingDict)

    print("\n### Final settingDict ###\n")
    print(json.dumps(settingDict, indent=4))

    ### PDB Client
    myClient=SetClient()

    #######
    # get components
    #######

    ### collect populations
    for pop in settingDict['population']:
        print(f"working on pop: {pop['alias']}")
        compTypeInfo=[]
        ### if compList is available use it (priority)
        if "compList" in pop['spec'].keys():
            print(f"found component list: {pop['spec']['compList']}")
            compTypeInfo=[{'code': x} for x in pop['spec']['compList']]
        ### else use XXXcode
        else:
            if 'clusCode' in pop['spec'].keys():
                print(f"found clusCode: {pop['spec']['clusCode']}")
                instList=GetClusterInstitutes(myClient, pop['spec']['clusCode'])
            elif 'instCode' in pop['spec'].keys():
                print(f"found instCode: {pop['spec']['instCode']}")
                instList=pop['spec']['instCode']
            elif 'instList' in pop['spec'].keys():
                print(f"found instList: {pop['spec']['instList']}")
                instList=pop['spec']['instList']
            else:
                print("no compList, instCode or clusCode found. Using project code")
                instList=GetProjectInstitutes(myClient, pop['spec']['projCode'])
            compTypeInfo=getComponentType(myClient, instList, pop['spec'])
            compInfo=GetComponentInfoII(myClient, instList, pop['spec'])
        pop['compTypeInfo']=compTypeInfo
        pop['compInfo']=compInfo
        # pprint(pop['compTypeInfo'])
        # print("===============")
        # pprint(pop['compInfo'])
        print("===============")
        print(f"Found componentTypes for {pop['alias']}: {len(pop['compTypeInfo'])}")
        print(f"Found components for {pop['alias']}: {len(pop['compInfo'])}")
        print("===============\n")

    if sum([len(pop['compInfo']) for pop in settingDict['population'] if "compInfo" in pop.keys()])<0:
        print("no components found in any populations. exiting")
        exit(0)
    
    #######
    # get the testRun IDs
    #######

    ### get component testRun IDs
    for ext in settingDict['extraction']:
        print(f"working on ext: {ext['alias']}")
        compTestRunsInfo=[]
        for pop in settingDict['population']:
            if pop['alias'] in ext['usePopulations']:
                print(f"found pop:{pop['alias']}")

                ### get test IDs
                if "spec" in ext.keys():
                    compTestRuns=GetTestRunIDs(myClient, [x['code'] for x in pop['compInfo']], 100)
                    if len(compTestRuns)<1:
                        print(f"no test info for: {pop['alias']}")
                    else:
                        compTestRunsInfo.append(compTestRuns)
        
        ext['compTestRunsInfo']=compTestRunsInfo
        print("===============")
        print(f"testTypes for {ext['alias']} : {sum([len(ctri.keys()) for ctri in ext['compTestRunsInfo']])}")
        print("===============\n")
    
    ### list for matching testRuns
    for ext in settingDict['extraction']:
        print(f"working on ext: {ext['alias']}")
        matchedTestRuns=[]
        # get testTypes defined in settings
        if "spec" in ext.keys():
            if ext['spec']==None:
                print("skipping matchedTestRuns")
                matchedTestRuns=None
            elif ext['spec'].lower()=="all":
                print("use *all* for matchedTestRuns")
                for ctri in ext['compTestRunsInfo']:
                    # print(ctri)
                    for k,v in ctri.items():
                        print(f"{k}: {len(v)}")
                        matchedTestRuns.extend(v)
                        # print(f" - running total: {len(matchedTestRuns)}")
            elif type(ext['spec'])==type([]):
                print("use subset for matchedTestRuns")
                for tc in set([x['testCode'] for x in ext['spec']]):
                    for ctri in ext['compTestRunsInfo']:
                        try:
                            matchedTestRuns.extend(ctri[tc])
                            print(f"found testType: {tc}")
                        except KeyError:
                            print(f"no matching testType: {tc}")
                            continue
            else:
                print("don't understand extraction spec:",ext['spec'])
        else:
            matchedTestRuns=None
        ### get test run data
        if matchedTestRuns==None:
            print(f"skipping testRuns for {ext['alias']}")
        else:
            testInfo=GetTestRunsData(myClient, matchedTestRuns)
            ext['testInfo']=testInfo
            print("===============")
            print(f"testRuns for {ext['alias']}: {len(ext['testInfo'])}")
            print("===============\n")

    
    #######
    # formatting
    #######

    ### list for matching testRuns
    for ext in settingDict['extraction']:
        print(f"working on ext: {ext['alias']}")

        if "compSummary" in ext.keys():
            compInfo=[]
            for pop in settingDict['population']:
                if pop['alias'] in ext['usePopulations']:
                    print(f"found pop:{pop['alias']}")
                    # get ids from dictionary of testTypes for matching testTypes (use set incase multiple same)
                    compInfo.extend(pop['compInfo'])
            df_compInfo=FormatComponentDataII(myClient, compInfo)
            ext['df_compInfo']=df_compInfo
                            
            print("===============")
            print(f"compSummary info. for {ext['alias']}: {ext['df_compInfo'].count()}")
            print("===============\n")

        if "stageSummary" in ext.keys():
            stageInfo=[]
            for pop in settingDict['population']:
                if pop['alias'] in ext['usePopulations']:
                    print(f"found pop:{pop['alias']}")
                    # get ids from dictionary of testTypes for matching testTypes (use set incase multiple same)
                    stageInfo.extend(pop['compInfo'])
            df_stageInfo=FormatStageDataII(myClient, stageInfo)
            ext['df_stageInfo']=df_stageInfo
                            
            print("===============")
            print(f"stageSummary info. for {ext['alias']}: {ext['df_stageInfo'].count()}")
            print("===============\n")

        if "testSummary" in ext.keys():
            df_testInfo=FormatTestDataII(myClient, testInfo)
            ext['df_testInfo']=df_testInfo
                            
            print("===============")
            print(f"testSummary info. for {ext['alias']}: {ext['df_testInfo'].count()}")
            print("===============\n")
    
    #######
    # plotting
    #######

    ### list for matching testRuns
    for vis in settingDict['visualisation']:
        print(f"working on vis: {vis['alias']}")

        vis['standard_overview_plot']=[]
        df_compType=pd.DataFrame()
        vis['standard_comp_plots']=[]
        df_compInfo=pd.DataFrame()
        vis['standard_test_plots']=[]
        df_testInfo=pd.DataFrame()
        vis['standard_stage_plots']=[]
        df_stageInfo=pd.DataFrame()
        for ext in settingDict['extraction']:
            if ext['alias'] in vis['useExtractions']:
                print(f"found ext:{ext['alias']}")

                if "compSummary" in vis.keys():
                    if df_compInfo.empty:
                        df_compInfo=ext['df_compInfo']
                    else:
                        df_compInfo=pd.concat([df_compInfo,ext['df_compInfo']])

                if "stageSummary" in vis.keys():
                    if df_stageInfo.empty:
                        df_stageInfo=ext['df_stageInfo']
                    else:
                        df_stageInfo=pd.concat([df_stageInfo,ext['df_stageInfo']])

                if "testSummary" in vis.keys():
                    if df_testInfo.empty:
                        df_testInfo=ext['df_testInfo']
                    else:
                        df_testInfo=pd.concat([df_testInfo,ext['df_testInfo']])

        if not df_compInfo.empty:
            print("getting plots...")
    #         vis['standard_over_plots']=OverviewPlot(df_compInfo)
            vis['standard_over_plots']=OverviewChartII(myClient, df_compInfo)
            vis['standard_comp_plots']=CustomComponentSummaryII(myClient, df_compInfo)

        
        print("===============")
        print(f"compSummary info. for {vis['alias']}: {len(vis['standard_comp_plots'])}")
        print("===============\n")

        if not df_stageInfo.empty:
            print("getting plots...")
            vis['standard_stage_plots']=StageSummaryII(myClient, df_stageInfo)
            vis['standard_stage_plots'].extend(StageTimeLinesII(myClient, df_stageInfo))
        
        print("===============")
        print(f"stageSummary info. for {vis['alias']}: {len(vis['standard_stage_plots'])}")
        print("===============\n")
        
        if not df_testInfo.empty:
            print("getting plots...")
            # vis['standard_test_plots']=TestSummaryII(myClient, df_testInfo)
            # vis['standard_test_plots'].extend(TestTimeLinesII(myClient, df_testInfo))
            vis['standard_test_plots']=StageTestTable(df_testInfo)

        print("===============")
        print(f"testSummary info. for {vis['alias']}: {len(vis['standard_test_plots'])}")
        print("===============\n")
        
        exclusionList=[]        
        for i in range(len(vis['standard_comp_plots'])):
            for j in range(len(vis['standard_comp_plots'][i]['dictList'])):
                if 'text' in vis['standard_comp_plots'][i]['dictList'][j].keys():
                    if vis['standard_comp_plots'][i]['dictList'][j]['text'] == '### Current Locations':
                        exclusionList.append([i, j])
        
        exclusionList.sort(reverse=True)
        n=len(exclusionList)
        while n != 0:
            for x in exclusionList:
                del vis['standard_comp_plots'][x[0]]['dictList'][x[1]]
                n-=1

    #######
    # sharing
    #######

    for dis in settingDict['distribution']:
        print(f"working on {dis['alias']}")
        # print(dis.keys())
        specChain={'distribution':dis, 'useVisualisations':[]}
        standard_plots=[]
        custom_plots=[]
        dic_plots={}
        for vis in settingDict['visualisation']:
            if vis['alias'] in dis['useVisualisations']:
                print(f"found vis:{vis['alias']}")
                for pk in [k for k in vis.keys() if "_plots" in k]:
                    ### standard plot keys
                    if "standard" in pk:
                        print(f"found {len(vis[pk])} {pk} standard plots")
                        standard_plots.extend(vis[pk])
                    ### custom plot keys
                    elif "custom" in pk:
                        print(f"found {len(vis[pk])} {pk} custom plots")
                        custom_plots.extend(vis[pk])
                    ### other?
                    else:
                        print(f"don't recognise {len(vis[pk])} {pk} plots")
        NewStdPlots=sorted(standard_plots, key=lambda d: d['name']) 
        NewCustPlots=sorted(custom_plots, key=lambda d: d['name']) 
        
        newStdListCopy=[]
        for x in NewStdPlots:
            checkItem=next((item for item in newStdListCopy if item['name']== x['name'].split(' ')[0]), None)
            if checkItem==None:
                newStdListCopy.append({'name':x['name'].split(' ')[0], 'dictList':x['dictList']})
            else: 
                checkItem['dictList'].extend(x['dictList'])
        
        newCustList=[]
        for x in NewCustPlots:
            checkItem=next((item for item in newCustList if item['name']== x['name'].split(' ')[0]), None)
            if checkItem==None:
                newCustList.append({'name':x['name'].split(' ')[0], 'dictList':x['dictList']})
            else: 
                checkItem['dictList'].extend(x['dictList'])
        
        k='Overview'
        newStdList=[]
        b=[]
        i=0
        for x in newStdListCopy:
            if x['name']==k and i==0:
                newStdList.append(x)
                i+=1
            elif x['name']==k and i!=0: continue
            else:
                b.append(x)
        newStdList.extend(b)
        
        
        # dic_plots['standard_plots']=newStdList
        # dic_plots['custom_plots']=newCustList
        # DataPaneChunkII(myClient, dic_plots, dis, origDict)
        
    #     for k in dic_plots['standard_plots']:
    #         print(k['name'])
        
        # print(json.dumps(origDict, indent=4))
        DataPaneChunk(myClient, newStdList, newCustList, dis, origDict)

