### get itkdb for PDB interaction
import os
import sys
import pandas as pd
import altair as alt
import numpy as np
import copy
import itkdb
import itkdb.exceptions as itkX
###


# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/commonCode')
from commonSpecReader import *
from commonCredentials import *
from commonPopulation import *
from commonExtraction import *
from commonVisualisation import *
from commonDistribution import *


#######
# main
#######
if __name__ == '__main__':

    settingDict=[]

    args=GetArgs('testType dashboard')
    chunk = 100
    if args.chunk!=None:
        chunk=args.chunk

    if args.file==None:
        print("No specification file provide.")
        print("Please provide spec file. See \"$(pwd)/testSpecs\" for examples")
        exit(0)
        # print(args)
    else:
        print("Reading specification file")
        settingDict=GetSpecFromFile(args.file)
        # print("\n### Initial settingDict ###\n",settingDict)
        # add any commandline stuff
        if args.keys!=None:
            settingDict=UpdateSpec(settingDict,args)
            if settingDict==None:
                print("Issue with spec update :(")
                exit(0)

    ### copy original dictionary to upload to report later
    origDict=copy.deepcopy(settingDict)

    print("\n### Final settingDict ###\n")
    print(json.dumps(settingDict, indent=4))

    ### PDB Client
    myClient=SetClient()

    #######
    # get components
    #######

    ### collect populations
    for pop in settingDict['population']:
        print(f"working on pop: {pop['alias']}")
        compInfo=[]
        ### if compList is available use it (priority)
        if "compList" in pop['spec'].keys():
            print(f"found component list: {pop['spec']['compList']}")
            compInfo=[{'code': x} for x in pop['spec']['compList']]
        elif "batchId" in pop['spec'].keys():
            print(f"found batchId: {pop['spec']['batchId']}")
            compInfo=[x for x in myClient.get('getBatch', json={'id':pop['spec']['batchId']})['components']]
        elif "batchType" in pop['spec'].keys() and "batchNumber" in pop['spec'].keys():
            print(f"found batchNumber: {pop['spec']['batchNumber']} ({pop['spec']['batchType']})")
            compInfo=[x for x in myClient.get('getBatchByNumber', json={'project':pop['spec']['projCode'],'batchType':pop['spec']['batchType'],'number':pop['spec']['batchNumber']})['components']]
        ### else use XXXcode
        else:
            if 'clusCode' in pop['spec'].keys():
                print(f"found clusCode: {pop['spec']['clusCode']}")
                instList=GetClusterInstitutes(myClient, pop['spec']['clusCode'])
            elif 'instCode' in pop['spec'].keys():
                print(f"found instCode: {pop['spec']['instCode']}")
                instList=pop['spec']['instCode']
            else:
                print("no compList, instCode or clusCode found. Using project code")
                instList=GetProjectInstitutes(myClient, pop['spec']['projCode'])
            compInfo=GetComponentInfo(myClient, instList, pop['spec'])
        pop['compInfo']=compInfo

        ### filter child components if required
        if len(pop['spec']['filters'])<1:
            continue
        if "child" in pop['spec']['filters'].keys():
            childInfo=GetChildInfo(myClient, [p['code'] for p in compInfo], pop['spec']['filters']['child'], 100)
            ### tidying
            for ci in childInfo:
                for sn in ['parentSN','childSN']:
                    try:
                        # remove non standard Atlas serialNumbers
                        if "20U" not in ci[sn]:
                            ci[sn]=None
                    except TypeError:
                        ci[sn]=None
            # fill return list (children only)
            childInfo=[c['childCode'] for c in childInfo]
            # remove Nones - information cannot be processed
            childInfo=[{'code':x} for x in childInfo if x==x and x!=None]
            pop['compInfo']=childInfo
        print("===============")
        print(f"Found components for {pop['alias']}: {len(pop['compInfo'])}")
        print("===============\n")

    if sum([len(pop['compInfo']) for pop in settingDict['population']])<0:
        print("no components found in any populations. exiting")
        exit(0)


    #######
    # get the testRun IDs
    #######

    ### get component testRun IDs
    for ext in settingDict['extraction']:
        print(f"working on ext: {ext['alias']}")
        compTestRunsInfo=[]
        for pop in settingDict['population']:
            if pop['alias'] in ext['usePopulations']:
                print(f"found pop:{pop['alias']}")

                ### get test IDs
                if "spec" in ext.keys():
                    compTestRuns=GetTestRunIDs(myClient, [x['code'] for x in pop['compInfo']], 100)
                    if len(compTestRuns)<1:
                        print(f"no test info for: {pop['alias']}")
                    else:
                        compTestRunsInfo.append(compTestRuns)
        
        ext['compTestRunsInfo']=compTestRunsInfo
        print("===============")
        print(f"testTypes for {ext['alias']} : {sum([len(ctri.keys()) for ctri in ext['compTestRunsInfo']])}")
        print("===============\n")

    if sum([len(ext['compTestRunsInfo']) for ext in settingDict['extraction']])<1:
        print("no testType info found in any populations. exiting")
        sys.exit()

    #######
    # get the testRun data
    #######

    for ext in settingDict['extraction']:
        print(f"working on ext: {ext['alias']}")
        matchedTestRuns=[]
        # get testTypes defined in settings
        if "spec" in ext.keys():
            if ext['spec']==None:
                print("skipping matchedTestRuns")
                matchedTestRuns=None
            elif type(ext['spec'])==type("str") and  ext['spec'].lower()=="all":
                print("use *all* for matchedTestRuns")
                for ctri in ext['compTestRunsInfo']:
                    print(ctri)
                    for k,v in ctri.items():
                        print(k)
                        matchedTestRuns.extend(v)
            elif type(ext['spec'])==type([]):
                print("use subset for matchedTestRuns")
                for tc in set([x['testCode'] for x in ext['spec']]):
                    for ctri in ext['compTestRunsInfo']:
                        try:
                            matchedTestRuns.extend(ctri[tc])
                            print(f"found testType: {tc}")
                        except KeyError:
                            print(f"no matching testType: {tc}")
                            continue
            else:
                print("don't understand extraction spec:",ext['spec'])
        else:
            matchedTestRuns=None
        ### get test run data
        if matchedTestRuns==None:
            print(f"skipping testRuns for {ext['alias']}")
        else:
            testInfo=GetTestRunsData(myClient, matchedTestRuns)
            ext['testInfo']=testInfo
            print("===============")
            print(f"testRuns for {ext['alias']}: {len(ext['testInfo'])}")
            print("===============\n")

    if sum([len(ext['testInfo']) for ext in settingDict['extraction']])<1:
        print("no testRun data found in any extractions. exiting")
        sys.exit()


    #######
    # formatting
    #######

    ### list for matching testRuns
    for ext in settingDict['extraction']:
        print(f"working on ext: {ext['alias']}")
        df_testRuns=FormatTestRunData(ext['testInfo'])
        ext['df_testRuns']=df_testRuns
        print("===============")
        print(f"df_testRuns for {ext['alias']}: {ext['df_testRuns'].columns}")
        print("===============\n")
    
    ### additional paramater formatting
    for ext in settingDict['extraction']:
        print(ext['spec'])
        df_testRuns=SubKeyFormatting(ext['df_testRuns'],ext['spec'])
        ext['df_testRuns']=df_testRuns
        print("===============")
        print(f"df_testRuns for {ext['alias']}: {ext['df_testRuns'].count()}")
        print("===============\n")

    if any([ext['df_testRuns'].empty for ext in settingDict['extraction']]):
        print("no formatted data found in any extractions. exiting")
        sys.exit()

    #######
    # plotting
    #######

    for vis in settingDict['visualisation']:
        # df_testRuns=pd.DataFrame()
        extractions=[]
        for ext in settingDict['extraction']:
            if ext['alias'] in vis['useExtractions']:
                print(f"found ext:{ext['alias']} adding...")
                # if df_testRuns.empty:
                #     df_testRuns=ext['df_testRuns']
                # else:
                #     df_testRuns=pd.concat([df_testRuns,ext['df_testRuns']])
                extractions.append(ext)
                ### standard plots (per institute)
                if "DQ" in vis.keys():
                    # display(ext['df_testParameters'])
                    dqPlots=DQPlotting_testList(ext['df_testRuns'],ext['spec'])
                    if "standard_plots" in vis.keys():
                        vis['standard_plots'].extend(dqPlots)
                    else:
                        vis['standard_plots']=dqPlots
        print("===============")
        print(f"standard plots for {vis['alias']}: {len(vis['standard_plots'])}")
        print("===============\n")
        # print(df_testRuns['paraCode'].unique())
        ### combine dfs
        df_testRuns=pd.concat([e['df_testRuns'] for e in extractions])
        if "match_pc" in vis.keys():
            df_testRuns=GetRelativesMap(myClient, df_testRuns, vis['match_pc'])
        ### custom plots
        if "correlations" in vis.keys():
            customPlots=[]
            for cor in vis['correlations']:
                corPlots=CorrelationPlot(df_testRuns, cor['x'], cor['y'], vis['match_pc'])
                customPlots.extend(corPlots)
            vis['custom_plots']=customPlots
            print("===============")
            print(f"custom plots for {vis['alias']}: {len(vis['custom_plots'])}")
            print("===============\n")


    if sum([len(vis['standard_plots']) for vis in settingDict['visualisation']])<1:
        if sum([len(vis['custom_plots']) for vis in settingDict['visualisation']])<1:
            print("no plots found in any visualisation. exiting")
            sys.exit()
        
    #######
    # sharing
    #######

    for dis in settingDict['distribution']:
        print(f"working on {dis['alias']}")
        # print(dis.keys())
        specChain={'distribution':dis, 'useVisualisations':[]}
        standard_plots=[]
        custom_plots=[]
        for vis in settingDict['visualisation']:
            if vis['alias'] in dis['useVisualisations']:
                print(f"found vis:{vis['alias']}")
                for pk in [k for k in vis.keys() if "_plots" in k]:
                    ### standard plot keys
                    if "standard" in pk:
                        print(f"found {len(vis[pk])} {pk} standard plots")
                        standard_plots.extend(vis[pk])
                    ### custom plot keys
                    elif "custom" in pk:
                        print(f"found {len(vis[pk])} {pk} custom plots")
                        custom_plots.extend(vis[pk])
                    ### other?
                    else:
                        print(f"don't recognise {len(vis[pk])} {pk} plots")
        # print(json.dumps(origDict, indent=4))
        DataPaneChunk(myClient, standard_plots, custom_plots, dis, origDict)
