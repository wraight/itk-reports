settingDict={
  "comp":
  {
    "projCode": "S",
    "compTypeCode": "MODULE",
    "testTypes": 
      [
          {"testCode": "MODULE_METROLOGY", "paraCode": "HYBRID_POSITION", "subs":[{"H_X_P1":0},{"H_X_P1":1},{"H_X_P2":0},{"H_X_P2":1}], "stageCode": "GLUED"}, 
          {"testCode": "MODULE_METROLOGY", "paraCode": "PB_POSITION", "subs":[{"PB_P1":0},{"PB_P1":1},{"PB_P2":0},{"PB_P2":1}], "stageCode": "GLUED"},
#           {"testCode": "MODULE_WIRE_BONDING", "paraCode": "FAILED_HYBRID_TO_PB", "stageCode": "BONDED"},
#           {"testCode": "MODULE_WIRE_BONDING", "paraCode": "FAILED_MODULE_TO_FRAME", "stageCode": "BONDED"},
          {"testCode": "MODULE_WIRE_BONDING", "paraCode": "TOTAL_PERC_UNCON_SENSOR_CHAN", "stageCode": "BONDED"}
          
      ]
  },
#   "clusCode": "STRIPS-LC-UKCHINA"
#    "instCode": "RAL"
    'instList':["BNL","UCSC","LBNL_STRIP_MODULES"]
}


### match formatted string to dictionary key 
def MatchDictKey(inThing,matchStr,keyStr=None):
    if keyStr!=None:
        if matchStr==keyStr:
            print("Found it! --",keyStr)
            return keyStr
        else:
            nextStr=keyStr+":"

    else:
        nextStr=""
    
    if type(inThing)==type({}):
        for k,v in inThing.items():
            retVal=MatchDictKey(v,matchStr,nextStr+k)
            if retVal!=None:
                return retVal
    elif type(inThing)==type([]):
        for e,i in enumerate(inThing):
            retVal=MatchDictKey(i,matchStr,nextStr[0:-1]+'_'+str(e))
            if retVal:
                return retVal
    else:
        print(keyStr)

    return None


### match formatted string to dictionary keys and update value
def FindReplaceValue(inDict,keyStr,newVal,sep=":"):

    keyArr=keyStr.split(sep)
    print(f"keyArr: {keyArr}")

    tempDict=inDict
    try:
        for k in (keyArr[0:-1]):
            if "_" not in k:
                try:
                    print(f"getting key: {k}")
                    tempDict=tempDict[k]
                except KeyError:
                    print(f"no {k} key found")
                    print("available keys:",tempDict.keys())
                    break
            else:
                print("found index marker")
                iKey=k.split('_')[0]
                idx=int(k.split('_')[1])
                try:
                    print(f"getting key: {iKey}")
                    tempDict=tempDict[iKey][idx]
                except KeyError:
                    print(f"no {iKey} key found")
                    print("available keys:",tempDict.keys())
                    break
                
    except IndexError:
        print(f"only {len(keyArr)} keys in array")
        pass
    
    # print("postDict:")
    # print(tempDict)

    try:
        print(f"setting key: {keyArr[-1]}")
        tempDict[keyArr[-1]]=newVal
    except KeyError:
        print(f"no {keyArr[-1]} key found")
        print("available keys:",tempDict.keys())
    
    return None
    
    

keyStr=MatchDictKey(settingDict,"comp:testTypes_0:testCode")
keyStr

FindReplaceValue(settingDict,keyStr,"SOMETHINGNEW")

settingDict