### get itkdb for PDB interaction
import os
import sys
# !{sys.executable} -m pip install pandas==1.2.4
import pandas as pd
import altair as alt
import numpy as np
import argparse
import json
import operator
# import copy
# !{sys.executable} -m pip install itkdb==0.3.16
import itkdb
import itkdb.exceptions as itkX
import datetime
###
import datapane as dp

###############
### read-in parts
###############

### standard inputs
def GetArgs(descStr=None):
    my_parser = argparse.ArgumentParser(description=descStr)
    # Add the arguments
    my_parser.add_argument('--file', '-f', type=str, help='settings file')
    my_parser.add_argument('--keys','-k', type=str, nargs='+', help='ordered list of argument keys')
    my_parser.add_argument('--vals','-v', type=str, nargs='+', help='ordered list of argument values')
    my_parser.add_argument('--chunk','-c', type=int, help='query chunk size (bulk request size), e.g. 100')
    
    args = my_parser.parse_args()
    return args


### match formatted string to dictionary key 
def MatchDictKey(inThing,matchStr,keyStr=None,sep=":",subSep="#"):
    if keyStr!=None:
        if matchStr==keyStr:
            print("Found it! --",keyStr)
            return keyStr
        else:
            nextStr=keyStr+sep

    else:
        nextStr=""
    
    if type(inThing)==type({}):
        for k,v in inThing.items():
            retVal=MatchDictKey(v,matchStr,nextStr+k)
            if retVal!=None:
                return retVal
    elif type(inThing)==type([]):
        for e,i in enumerate(inThing):
            retVal=MatchDictKey(i,matchStr,nextStr[0:-1]+subSep+str(e))
            if retVal:
                return retVal
    else:
        print(keyStr)

    return None


### match formatted string to dictionary keys and update value
def FindReplaceValue(inDict,keyStr,newVal,sep=":",subSep="#"):

    keyArr=keyStr.split(sep)
    print(f"keyArr: {keyArr}")

    tempDict=inDict
    try:
        for k in (keyArr[0:-1]):
            if subSep not in k:
                try:
                    print(f"getting key: {k}")
                    tempDict=tempDict[k]
                except KeyError:
                    print(f"no {k} key found")
                    print("available keys:",tempDict.keys())
                    break
            else:
                print("found index marker")
                iKey=k.split(subSep)[0]
                idx=int(k.split(subSep)[1])
                try:
                    print(f"getting key: {iKey}")
                    tempDict=tempDict[iKey][idx]
                except KeyError:
                    print(f"no {iKey} key found")
                    print("available keys:",tempDict.keys())
                    break
                
    except IndexError:
        print(f"only {len(keyArr)} keys in array")
        pass
    
    # print("postDict:")
    # print(tempDict)

    try:
        print(f"setting key: {keyArr[-1]}")
        tempDict[keyArr[-1]]=newVal
    except KeyError:
        print(f"no {keyArr[-1]} key found")
        print("available keys:",tempDict.keys())
    
    return None


### building the spec
def UpdateSpec(existDict,args):

    if len(args.keys)!=len(args.vals):
        print("arguments and values array lengths do not match please check.")
        print(f"args({len(args.keys)}):",args.keys)
        print(f"vals({len(args.vals)}):",args.vals)
        return None
    
    for e,a in enumerate(args.keys):
        keyStr=MatchDictKey(existDict,a)
        if keyStr==None:
            print(f"No matching key {a} found in existing dictionary.")
            continue
        else:
            FindReplaceValue(existDict,keyStr,args.vals[e])

    return existDict


### read spec file
def GetSpecFromFile(specFile):
    specDict=None
    # Get spec file
    with open(specFile) as json_file:
        specDict = json.load(json_file)
    if specDict==None:
        print("No specifications file found")
        return None
    return specDict

