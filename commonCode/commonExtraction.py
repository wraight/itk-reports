# !{sys.executable} -m pip install pandas==1.3.4
import pandas as pd
import numpy as np
# import copy
# !{sys.executable} -m pip install itkdb==0.4
import itkdb
import itkdb.exceptions as itkX
from commonPopulation import MagicDigger
import requests


###################
### cleaner extractions - avoid exceptions
###################

def CleanerReturn(x,digDict):

    if type(digDict)==type("str"):
        print("return string")
        try:
            return x[digDict]
        except KeyError:
            return None
        except TypeError:
            return None
    elif type(digDict)==type({}):
        print("digging...")
        return MagicDigger(digDict,x)
    else:
        return None


###################
### PDB extraction parts
###################

### get component lists
def GetComponentData(myClient, compInfo, chunk=100):
    # list of components
    components=[]
    # loop over IDs

    # loop over components
    for x in range(0,int(np.ceil(len(compInfo)/chunk))):
        print(f"component loop {x}: [{x*chunk}:{(x+1)*chunk}]")
        # display(pd.DataFrame(foundComps[x*chunk:(x+1)*chunk]))
        bulkFail=False
        for z in range(0,10,1):
            try:
                compChunk=myClient.get('getComponentBulk',json={'component':compInfo[x*chunk:(x+1)*chunk]})
                break
            except requests.exceptions.ConnectionError as e:
                print(f"ERROR\n{e}")
                print(f"CONNECTION ISSUE - TRY AGAIN {z}")
            except itkdb.exceptions.ServerError as e:
                print(f"ERROR\n{e}")
                print(f"SERVER ISSUE - TRY AGAIN {z}")
            if z==9:
                bulkFail=True
        if bulkFail:
            print("- bulk retrieval has failed. Skipping chunk!")
            continue
        components.extend(compChunk)
        print(f"update testRun results: {len(compChunk)}")

    print(f"final testRun results: {len(components)}")
    return components

### get testRun ID lists of all tests
### (don't have to keep component ID with testRun as comp ID will be returned with testRun info. )
### chunk used to limit size of request to database and avoid timeout errors 
def GetTestRunIDs(myClient, foundComps, chunk=100):
    # dictionary of test types and testRun IDs
    testRunDict={}

    # loop over components
    for x in range(0,int(np.ceil(len(foundComps)/chunk))):
        print(f"component loop {x}: [{x*chunk}:{(x+1)*chunk}]")
        # display(pd.DataFrame(foundComps[x*chunk:(x+1)*chunk]))
        bulkFail=False
        for z in range(0,10,1):
            try:
                foundList=myClient.get('getComponentBulk', json={'component':foundComps[x*chunk:(x+1)*chunk] })
                break
            except requests.exceptions.ConnectionError as e:
                print(f"ERROR\n{e}")
                print(f"CONNECTION ISSUE - TRY AGAIN {z}")
            except itkdb.exceptions.ServerError as e:
                print(f"ERROR\n{e}")
                print(f"SERVER ISSUE - TRY AGAIN {z}")
            if z==9:
                bulkFail=True
        if bulkFail:
            print("- bulk retrieval has failed. Skipping chunk!")
            continue
        # loop over components and extract test information
        for e,comp in enumerate(foundList):
            if len(comp['tests'])<1:
                print(f"no tests for {comp['code']}")
                continue
            # get test ids
            for ct in comp['tests']:
                # skip missing data
                if type(ct)!=type({}) or "code" not in ct.keys():
                    continue
                # add to dictionary (try: assuming key already exists, exeption: add key)
                try:
                    testRunDict[ct['code']].extend([tr['id'] for tr in ct['testRuns'] if type(tr)==type({}) and "id" in tr.keys()])
                except KeyError:
                    testRunDict[ct['code']]=[tr['id'] for tr in ct['testRuns'] if type(tr)==type({}) and "id" in tr.keys()]
    # print extracted info.
    for k,v in testRunDict.items():
        print(f"{k} : {len(v)}")
    return testRunDict


### get all testRun data based ID codes
### order (parent/child) doesn't matter as testRun info. will specify
### chunk used to limit size of request to database and avoid timeout errors 
def GetTestRunsData(myClient, matchedTestRuns, chunk=100):
    # list of test runs
    testRuns=[]
    # loop over IDs
    for x in range(0,int(np.ceil(len(matchedTestRuns)/chunk))):
        print(f"testRun loop {x}: [{x*chunk}:{(x+1)*chunk}]")
        bulkFail=False
        for z in range(0,10,1):
            try:
                testRunChunk=myClient.get('getTestRunBulk',json={'testRun':matchedTestRuns[x*chunk:(x+1)*chunk]})
                break
            except requests.exceptions.ConnectionError as e:
                print(f"ERROR\n{e}")
                print(f"CONNECTION ISSUE - TRY AGAIN {z}")
            except itkdb.exceptions.ServerError as e:
                print(f"ERROR\n{e}")
                print(f"SERVER ISSUE - TRY AGAIN {z}")
            if z==9:
                bulkFail=True
        if bulkFail:
            print("- bulk retrieval has failed. Skipping chunk!")
            continue
        # add to test run list
        testRuns.extend(testRunChunk)
        print(f"update testRun results: {len(testRunChunk)}")

    print(f"final testRun results: {len(testRuns)}")
    return testRuns


### get test schema using component dictionary and test code 
def GetTestParameters(myClient,compDict,testCode):
    testSchema=myClient.get('getTestTypeByCode', json={'project':compDict['projCode'], 'componentType':compDict['compTypeCode'], 'code':testCode})
    df_testParameters=pd.json_normalize(testSchema['parameters'],sep='_')
    return df_testParameters


################
### data formatting part
################### 

# code if in dict, as is if not dict, non if no code and dict
def FindKey(x, fk="code"):
    if type(x)==type({}):
        if fk in x.keys():
            return x[fk]
        else:
            return None
    return x

### get/invent test index - used to distinguish repeated testTypes with same institute and serialNumber
def GetIndex(inst, sn, combCollection):
    # if this inst NOT recorded then add to list
    if inst not in set([cc['inst'] for cc in combCollection]): ### no inst found
        combCollection.append({'inst':inst,'SN':sn})
        # return initial count
        return 0
    else:
        instCollection=[cc for cc in combCollection if cc['inst']==inst] ### get inst collection
        # if this inst-sn combination NOT recorded then add to list
        if sn not in set([ic['SN'] for ic in instCollection]): ### no SN found
            combCollection.append({'inst':inst,'SN':sn})
            # return initial count
            return 0
        else:
            # if this inst-sn combination IS recorded then append toexisint entry in list
            SNCollection=[ic for ic in instCollection if ic['SN']==sn]
            combCollection.append({'inst':inst,'SN':sn})
            # return appended count
            return len(SNCollection)
    return -1


### Get connection of parent to child
def GetConnection(conList,code):
    # match code to parent in list
    try:
        parMatch = next((item for item in conList if item['parentCode'] == code), None)
    except TypeError:
        print("connection TypeError:",conList)
        return None
    # if no match, try to match code to child in list
    if parMatch==None:
        childMatch = next((item for item in conList if item['childCode'] == code), None)
        if childMatch==None:
            # give up
            return None
        else:
            # return connection
            return str(childMatch['parentSN'])+"-"+str(childMatch['childSN'])
    else:
        # return connection
        return str(parMatch['parentSN'])+"-"+str(parMatch['childSN'])
    return None


### return value form dictionary matching key
def GetDictVal(inDict,vKey,idx=None):
    for k,v in inDict.items():
        if k==vKey:
            # print(f"got {k}: {v}")
            if idx!=None:
                try:
                    return inDict[k][idx]
                except IndexError:
                    print("index error")
                    return None
            else:
                return inDict[k]
        else:
            if type(v)==type({}):
                return GetDictVal(v,vKey)
    return None


### additional formatting to get sub-keys from dictionary objects
def SubKeyFormatting(df_testRuns,testTypes):
    for tt in testTypes:
        print("working on:",tt['paraCode'],"@",tt['testCode'])
        if "subs" in tt.keys():
            print("\tchecking:",tt['subs'])
            df_totSub=pd.DataFrame()
            for pSub in tt['subs']:
                for pKey,pVal in pSub.items():
                    print("\ttry:",pKey)
                    df_sub=df_testRuns.query('paraCode=="'+tt['paraCode']+'"')
        #             display(df_sub)
                    ### if an int --> get index==int from paraValue
                    if type(pVal)==type(1):
                        print("\t\tidx:",pVal)
                        df_sub['paraValue']=df_sub['paraValue'].apply(lambda x: GetDictVal(x,pKey,pVal) )
                        df_sub['paraCode']=tt['paraCode']+"->"+pKey+"["+str(pVal)+"]"
                    ### for the moment just leave everything else alone
                    else:
                    ### if None --> use paraValue as is
                    # if pVal==None or type(pVal)==type([]):
                        print("-- leaving substructure alone supported")
                        df_sub['paraValue']=df_sub['paraValue'].apply(lambda x: GetDictVal(x,pKey) )
                        df_sub['paraCode']=tt['paraCode']+"->"+pKey
                    # ### if an list --> explode the values
                    # elif type(pVal)==type([]):
                    #     print("\t\texplode list:",pVal)
                    #     df_sub['paraValue']=df_sub['paraValue'].apply(lambda x: GetDictVal(x,pKey) )
                    #     df_sub['paraCode']=tt['paraCode']+"->"+pKey
                    #     df_sub=df_sub.explode('paraValue')
                    ### stuck
                    # else:
                    #     print(type(pVal),"not currently supported")
    #                 display(df_sub)
                    if df_totSub.empty:
                        df_totSub=df_sub.copy(deep=True)
                    else:
                        df_totSub=pd.concat([df_totSub,df_sub])
                    print(df_totSub['paraCode'].unique())
            ### drop unformatted rows
            print(f"Update of testRun data for {tt['subs']}")
            print("Previous...")
            print(df_testRuns['paraCode'].unique())
            df_testRuns = df_testRuns[df_testRuns['paraCode'] != tt['paraCode']]
            ### add formatted rows
            df_testRuns = pd.concat([df_testRuns,df_totSub])
            print("Updated...")
            print(df_testRuns['paraCode'].unique())

    return df_testRuns


### get related paraCode to dictionary
def GetRelatedPC(paraCode, paraCodeList, exact=False):
    for pc in paraCodeList:
        # if not looking for exact match then loop through
        if exact==False:
            # if can't find related code then skip
            if paraCode not in pc:
                continue
            else: 
                return pc
        # if exact match sought
        else:
            if paraCode==pc:
                return pc
    return None

### component summary
def FormatComponentData(compInfo, explode=True):
    # list of test runs

    df_comps=pd.DataFrame(compInfo)
    print(df_comps.columns)
    # display(df_comps[['project','componentType','type','institution','currentLocation','currentStage']])
    # get codes if possible
    for k in df_comps.columns:
        df_comps[k]= df_comps[k].apply(lambda x: FindKey(x))
    # # code
    # for col in ['project','componentType','type','institution','currentLocation','currentStage']:
    #     df_comps[col]= df_comps[col].apply(lambda x: x['code'])
    # make sums
    for k in ["stages","tests"]:
        try:
            df_comps[f'No.{k.title()}']= df_comps[k].apply(lambda x: len(x) if x!=None else 0)
        except KeyError:
            pass

    # no column explosion
    if explode!=True:
        return df_comps
    
    ### test info. part
    df_explode=pd.DataFrame()
    for pr in ["properties"]:
        # unpack data
        df_pr=df_comps.explode(pr)
        for up in ["valueType","dataType"]:
            df_pr[up]=df_pr[pr].apply(lambda x: x[up] if type(x)==type({}) and up in x.keys() else x)
        for k,v in {'code':"paraCode",'value':"paraValue"}.items():
            df_pr[v]=df_pr[pr].apply(lambda x: x[k] if type(x)==type({}) and k in x.keys() else x)
        # after all unpacking, reset the dataframe index
        df_pr=df_pr.reset_index(drop=True)
        # add to total df
        if df_explode.empty:
            df_explode=df_pr
        else:
            df_explode=pd.concat([df_explode, df_pr])

    return df_explode


### stage info.
def FormatStageData(compInfo, explode=True):
    # list of test runs

    # convert to dataframe
    df_stages=pd.DataFrame(compInfo)
    # check non-empty
    if "stages" not in df_stages.columns:
        print("No stages here")
        return pd.DataFrame()
    # print(df_stages.columns)
    
    df_stages=df_stages[['id','code','project','componentType','serialNumber','alternativeIdentifier','stages','currentLocation','type']]
    # get codes if possible
    for k in df_stages.columns:
        df_stages[k]= df_stages[k].apply(lambda x: FindKey(x))

    # no column explosion
    if explode!=True:
        return df_stages

    # get stage info.
    df_explode=df_stages.explode('stages')
    for k in ['code','dateTime']:
        df_explode['stage_'+k]=df_explode['stages'].apply(lambda x: x[k] if type(x)==type({}) and k in x.keys() else None)
    # necessary hack to deal with strange formatting (datetime.datetime instead of pandas._libs.tslibs.timestamps.Timestamp)
    df_explode['stage_dateTime']= df_explode['stage_dateTime'].apply(lambda x: str(x).split(':')[0] if x!=None else None)
    df_explode['stage_dateTime']= pd.to_datetime(df_explode['stage_dateTime'],format='%Y-%m-%dT%H:%M:%S.%fZ',errors='coerce')
    df_explode=df_explode.reset_index(drop=True)

    return df_explode


### List of formatting commands
def FormatTestData(myClient, testInfo, chunk=100):
    # list of test runs
    testList=[]
    # loop over IDs
    for x in range(0,int(np.ceil(len(testInfo)/chunk))):
        print(f"test loop {x}: [{x*chunk}:{(x+1)*chunk}]")
        bulkFail=False
        for z in range(0,10,1):
            try:
                testChunk=myClient.get('getTestRunBulk',json={'testRun':[ti['id'] for ti in testInfo[x*chunk:(x+1)*chunk]]})
                break
            except requests.exceptions.ConnectionError as e:
                print(f"ERROR\n{e}")
                print(f"CONNECTION ISSUE - TRY AGAIN {z}")
            except itkdb.exceptions.ServerError as e:
                print(f"ERROR\n{e}")
                print(f"SERVER ISSUE - TRY AGAIN {z}")
            if z==9:
                bulkFail=True
        if bulkFail:
            print("- bulk retrieval has failed. Skipping chunk!")
            continue
        testList.extend(testChunk)

    # convert data to pandas dataFrame
    df_testRuns=pd.DataFrame([x for x in testList if x!=None])

    # check non-empty
    if df_testRuns.empty:
        print("No tests found")
        return pd.DataFrame()
    
    # print(df_testRuns.columns)   
    # some things don't have state
    if "state" in df_testRuns.columns:
        df_testRuns=df_testRuns.query('state=="ready"')[['components','institution','testType','date','properties','results','passed']]
    else:
        df_testRuns=df_testRuns[['components','institution','testType','date','properties','results','passed']]

    # get codes if possible
    for k in df_testRuns.columns:
        df_testRuns[k]= df_testRuns[k].apply(lambda x: FindKey(x))

    # convert dateTime format
    df_testRuns['date']= pd.to_datetime(df_testRuns['date'],format='%Y-%m-%dT%H:%M:%S.%fZ')
    # simple unpacking
    for k in ['serialNumber','alternativeIdentifier']:
        try:
            df_testRuns[k]=df_testRuns['components'].apply(lambda x: x[k] if type(x)==type({}) and k in x.keys() else None)
        except KeyError: # except missing
            pass
    # get component info.
    df_testRuns=df_testRuns.explode('components')
    df_testRuns['compCode']=df_testRuns['components'].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else None)
    for k,v in {'compTypeCode':"componentType", 'projCode':"project", 'typeCode':"type", 'stage':"testedAtStage"}.items():
        df_testRuns[k]=df_testRuns['components'].apply(lambda x: x[v]['code'] if type(x)==type({}) and v in x.keys() and type(x[v])==type({}) and "code" in x[v].keys() else None)
    df_testRuns['localName']=df_testRuns['components'].apply(lambda x: next((item['value'] for item in x['properties'] if item['code']=="LOCALNAME"), None) if type(x)==type({}) and "properties" in x.keys() and type(x['properties'])==type([]) else None)
    # identifier per test
    # combCollection=[]
    # df_testRuns['testIndex']=df_testRuns.apply(lambda row: GetIndex(row['institution'],row['compCode'],combCollection), axis=1)

    ### test info. part
    df_testRuns=df_testRuns.explode('results')
    for k in ["valueType", "dataType"]:
        df_testRuns[k]=df_testRuns['results'].apply(lambda x: x[k] if type(x)!=type(None) else None)
    df_testRuns['paraCode']=df_testRuns['results'].apply(lambda x: x['code'] if type(x)!=type(None) else None)
    df_testRuns['paraValue']=df_testRuns['results'].apply(lambda x: x['value'] if type(x)!=type(None) else None)
    # after all unpacking, reset the dataframe index
    df_testRuns=df_testRuns.reset_index(drop=True)

    return df_testRuns


### List of formatting commands
def FormatTestRunData(testData, childInfo=None):
    # convert data to pandas dataFrame
    df_testRuns=pd.DataFrame(testData)
    # display(df_testRuns)
    print(df_testRuns.columns)
    
    ### skip empty
    if df_testRuns.empty:
        return pd.DataFrame()
    
    ### use subset of data
    # some things don't have state
    if "state" in df_testRuns.columns:
        df_testRuns=df_testRuns.query('state=="ready"')[['components','institution','testType','date','properties','results','passed','problems']]
    else:
        df_testRuns=df_testRuns[['components','institution','testType','date','properties','results','passed','problems']]
    # convert institute info. from dictionary to code
    df_testRuns['institution']=df_testRuns['institution'].apply(lambda x: x['code'])
    df_testRuns['testCode']=df_testRuns['testType'].apply(lambda x: x['code'])
    # convert dateTime format
    df_testRuns['date']= pd.to_datetime(df_testRuns['date'],format='%Y-%m-%dT%H:%M:%S.%fZ')

    ### component info. part - unpack dictionary object
    df_testRuns=df_testRuns.explode('components')
    # simple unpacking
    for k in ['serialNumber','alternativeIdentifier']:
        try:
            df_testRuns[k]=df_testRuns['components'].apply(lambda x: x[k])
        except KeyError: # except missing
            pass
    # manual unpacking
    df_testRuns['compCode']=df_testRuns['components'].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else None)
    # get component things
    for k,v in {'componentType':"compTypeCode", 'type':"subType", 'project':"projCode", 'testedAtStage':"stage"}.items():
        df_testRuns[v]=df_testRuns['components'].apply(lambda x: x[k]['code'] if type(x)==type({}) and k in x.keys() and type(x[k])==type({}) and "code" in x[k].keys() else None)
    df_testRuns['localName']=df_testRuns['components'].apply(lambda x: next((item['value'] for item in x['properties'] if type(item)==type({}) and "code" in item.keys() and item['code']=="LOCALNAME"), None)  if type(x)==type({}) and "properties" in x.keys() and type(x['properties'])==type([]) else None)

#     df_testRuns['currentLocation']=df_testRuns['compCode'].apply(lambda x: next((item['curLoc'] for item in childInfo if item['parentCode'] == x), None) )
    if childInfo!=None:
        df_testRuns['connection']=df_testRuns['compCode'].apply(lambda x: GetConnection(childInfo,x))
    # identifier per test
    combCollection=[]
    df_testRuns['testIndex']=df_testRuns.apply(lambda row: GetIndex(row['institution'],row['compCode'],combCollection), axis=1)

    ### test info. part
    df_explode=pd.DataFrame()
    for pr in ["properties","results"]:
        # unpack data
        df_pr=df_testRuns.explode(pr)
        for up in ["valueType","dataType"]:
            df_pr[up]=df_pr[pr].apply(lambda x: x[up] if type(x)==type({}) and up in x.keys() else x)
        for k,v in {'code':"paraCode",'value':"paraValue"}.items():
            df_pr[v]=df_pr[pr].apply(lambda x: x[k] if type(x)==type({}) and k in x.keys() else x)
        # after all unpacking, reset the dataframe index
        df_pr=df_pr.reset_index(drop=True)
        # add to total df
        if df_explode.empty:
            df_explode=df_pr
        else:
            df_explode=pd.concat([df_explode, df_pr])

    return df_explode
