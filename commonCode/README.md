# Common Code

Common functions used for reporting scripts collected according to reporting module - _population_, _extraction_, _visualisation_ and _distribution_. 
In addition, functions to read in specifications files and user credentials are found in _commonSpecReader_ and _commonCredentials_, respectively.

### Required packages

- [itkdb](https://pypi.org/project/itkdb/#description)
- [pandas](https://pandas.pydata.org)
- [altair](https://altair-viz.github.io)
- [datapane](https://datapane.com)

You can find a repository of reports [here](https://itk-reporting-repository.docs.cern.ch).

---
