import numpy as np
import itkdb
import itkdb.exceptions as itkX
import datetime
import pandas as pd
from commonPopulation import MagicDigger
import pandas as pd
import altair as alt
import numpy as np
import requests
import copy
import math
import datapane as dp
from commonPopulation import CheckWithSpec
import os
import sys
import json
import gitlab
import subprocess
import paramiko
from scp import SCPClient
from commonExtraction import FindKey
from commonVisualisation import SpecialString
from commonDistribution import ScriptCommit, GetDatetime, MakeCreditStr, AltCopy

### Get list of components (of type in compDict) from institutes in instList
### based on matching currentLocation
def getComponentType(myClient, instList, compDict):
    # list for matching components
    foundComps=[]
    # check input is a list (if not make it one)
    if type(instList)!=type([]):
        print("casting input as list")
        myInstCodes=[instList]
    else:
        myInstCodes=instList
    # loop through codes in list
    for inst in myInstCodes:
        print(f"listing components at {inst}")
        # get components with institute as currentLocation
        listFail=False
        for z in range(0,10,1):
            try:
                compList=myClient.get('listComponentTypes', json={'filterMap':{'project':compDict['projCode']}})
                # compList=myClient.get('listComponents', json={'filterMap':{'project':compDict['projCode'], 'state':"ready"} } )
                break
            except requests.exceptions.ConnectionError as e:
                print(f"ERROR\n{e}")
                print(f"CONNECTION ISSUE - TRY AGAIN {z}")
            if z==9:
                listFail=True
        if listFail:
            print("- list retrieval has failed! Skipping")
            continue
        # add exception for case where code is missing
        try:
            compCodes=[x['code'] for x in compList.data]
        except AttributeError:
            compCodes=[x['code'] for x in compList]
        except KeyError:
            print("no code key found. skipping")
            continue
        # add to list of matching components
        foundComps.extend([{'code':c} for c in compCodes])
        print(f"found componentTypes: {len(foundComps)}")
    print(f"found componentTypes: {len(foundComps)}")
    return foundComps


def GetComponentInfoII(myClient, instList, compDict):
    # list for matching components
    foundComps=[]
    # check input is a list (if not make it one)
    if type(instList)!=type([]):
        print("casting input as list")
        myInstCodes=[instList]
    else:
        myInstCodes=instList
    # loop through codes in list
    for inst in myInstCodes:
        print(f"working on: {inst}")
        # get components with institute as currentLocation
        total = myClient.get('listComponents', json={'filterMap':{'project':compDict['projCode'], 'currentLocation':inst, 'state':"ready"}, 'pageInfo': {'pageSize': 1}}).total
        print(f" - components to retrieve: {total}")
        ### set up loop
        pageSize=100
        count= int(total/pageSize)
        if total%pageSize>0:
            count=count+1
        ### do loop
        loop_count=1
        compList=[]
        for pi in range(0,count,1):
            listFail=False
            for z in range(0,10,1):
                try:
                    # retVal=myClient.get('listComponents', json={'filterMap':{'project':compDict['projCode'],'currentLocation':inst},'pageInfo':{ 'pageIndex': pi, 'pageSize': pageSize }})
                    retVal=myClient.get('listComponents', json={'filterMap':{'project':compDict['projCode'],'currentLocation':inst, 'state':"ready"},'pageInfo':{ 'pageIndex': pi, 'pageSize': pageSize }})                    
                    try:
                        compList.extend( retVal.data)
                    except AttributeError:
                        compList.extend( retVal )                    
                    break
                except requests.exceptions.ConnectionError as e:
                    print(f"ERROR\n{e}")
                    print(f"CONNECTION ISSUE - TRY AGAIN {z}")
                if z==9:
                    listFail=True
            if listFail:
                print("- list retrieval has failed! Skipping")
                continue
            loop_count+=1
        # add exception for case where code is missing
        try:
            compDictTry=[{'code':x['code'], 'type':x['type'], 'currentStage':x['currentStage']} for x in compList]
#             compCodes=[x['code'] for x in compList.data]
#             compType=[x['type'] for x in compList.data]
#             compCurrentStage=[x['currentStage'] for x in compList.data]
        except KeyError:
            print("no code key found. skipping")
            continue
        # add to list of matching components
#         foundComps.extend([{'code':c} for c in compCodes])
        foundComps.extend(compDictTry)
        print(f"found components: {len(foundComps)}")
    print(f"found components: {len(foundComps)}")
    return foundComps

def GetComponentInfoChild(myClient, instList, compDict):
    # list for matching components
    foundComps=[]
    # check input is a list (if not make it one)
    if type(instList)!=type([]):
        print("casting input as list")
        myInstCodes=[instList]
    else:
        myInstCodes=instList
    # loop through codes in list
    for inst in myInstCodes:
        print(f"working on: {inst}")
        # get components with institute as currentLocation
        total = myClient.get('listComponents', json={'filterMap':{'project':compDict['projCode'], 'currentLocation':inst, 'state':"ready"}, 'pageInfo': {'pageSize': 1}}).total
        print(f" - components to retrieve: {total}")
        ### set up loop
        pageSize=100
        count= int(total/pageSize)
        if total%pageSize>0:
            count=count+1
        ### do loop
        loop_count=1
        compListTemp=[]
        compList=[]
        for pi in range(0,count,1):
            print(f"listing components at {inst}")
            # get components with institute as currentLocation
            listFail=False
            for z in range(0,10,1):
                try:
                    retVal=myClient.get('listComponents', json={'filterMap':{'project':compDict['projCode'],'currentLocation':inst, 'componentType':compDict['compTypeCode'], 'state':"ready"}, 'pageInfo':{ 'pageIndex': pi, 'pageSize': pageSize }})
                    try:
                        compListTemp.extend( retVal.data)
                    except AttributeError:
                        compListTemp.extend( retVal )
                    break
                except requests.exceptions.ConnectionError as e:
                    print(f"ERROR\n{e}")
                    print(f"CONNECTION ISSUE - TRY AGAIN {z}")
                if z==9:
                    listFail=True
            if listFail:
                print("- list retrieval has failed! Skipping")
                continue
            loop_count+=1
        # add exception for case where code is missing
        compList = [ myClient.get('getComponent', json={'component':compListTemp[x]['code'] }) for x in range(0, len(compListTemp))]
        try:
            compDictTry=[{'code':x['code'], 'type':x['type'], 'currentStage':x['currentStage'], 'children':x['children']} for x in compList]
#             compCodes=[x['code'] for x in compList.data]
#             compType=[x['type'] for x in compList.data]
#             compCurrentStage=[x['currentStage'] for x in compList.data]
        except KeyError:
            print("no code key found. skipping")
            continue
        # add to list of matching components
#         foundComps.extend([{'code':c} for c in compCodes])
        foundComps.extend(compDictTry)
        print(f"found components: {len(foundComps)}")
    print(f"found components: {len(foundComps)}")
    return foundComps


### component summary
def FormatComponentDataII(myClient, compInfo, chunk=100):
    # list of test runs
    compList=[]
    # loop over IDs
    for x in range(0,int(np.ceil(len(compInfo)/chunk))):
        print(f"component loop {x}: [{x*chunk}:{(x+1)*chunk}]")
        bulkFail=False
        for z in range(0,10,1):
            try:
                compChunk=myClient.get('getComponentBulk',json={'component':[ci['code'] for ci in compInfo[x*chunk:(x+1)*chunk]]})
                break
            except requests.exceptions.ConnectionError:
                print(f"ERROR\n{e}")
                print(f"CONNECTION ISSUE - TRY AGAIN {z}")
            except itkdb.exceptions.ServerError as e:
                print(f"ERROR\n{e}")
                print(f"SERVER ISSUE - TRY AGAIN {z}")
            if z==9:
                bulkFail=True
        if bulkFail:
            print("- bulk retrieval has failed. Skipping chunk!")
            continue
        compList.extend(compChunk)

    df_comps=pd.DataFrame(compList)
    # print(df_comps.columns)
    # display(df_comps[['project','componentType','type','institution','currentLocation','currentStage']])
    # get codes if possible
    for k in df_comps.columns:
        df_comps[k]= df_comps[k].apply(lambda x: FindKey(x))
    # # code
    # for col in ['project','componentType','type','institution','currentLocation','currentStage']:
    #     df_comps[col]= df_comps[col].apply(lambda x: x['code'])
    # make sums
    for st in ['stages','tests']:
        if st in df_comps.columns:
            df_comps['No.'+st.title()]= df_comps[st].apply(lambda x: len(x) if x!=None else 0)
        else:
            df_comps['No.'+st.title()]=0

    return pd.DataFrame(df_comps)

### stage info.
def FormatStageDataII(myClient, compInfo, chunk=100):
    # list of test runs
    compList=[]
    # loop over IDs
    for x in range(0,int(np.ceil(len(compInfo)/chunk))):
        print(f"component loop {x}: [{x*chunk}:{(x+1)*chunk}]")
        bulkFail=False
        for z in range(0,10,1):
            try:
                compChunk=myClient.get('getComponentBulk',json={'component':[ci['code'] for ci in compInfo[x*chunk:(x+1)*chunk]]})
                break
            except requests.exceptions.ConnectionError:
                print(f"ERROR\n{e}")
                print(f"CONNECTION ISSUE - TRY AGAIN {z}")
            except itkdb.exceptions.ServerError as e:
                print(f"ERROR\n{e}")
                print(f"SERVER ISSUE - TRY AGAIN {z}")
            if z==9:
                bulkFail=True
        if bulkFail:
            print("- bulk retrieval has failed. Skipping chunk!")
            continue
        compList.extend(compChunk)

    df_stages=pd.DataFrame(compList)
    ### use overlap of wants with is
    print(f"### this is the columns:\n{df_stages.columns}")
    colList=['id','code','project','componentType','serialNumber','alternativeIdentifier','stages','currentLocation','type']
    colList=list(set(df_stages.columns) & set(colList))
    print(f"### this is the list:\n{colList}")
    ### catch empties
    df_stages=df_stages[colList]
    if df_stages.empty:
        return df_stages
    # get codes if possible
    for k in df_stages.columns:
        df_stages[k]= df_stages[k].apply(lambda x: FindKey(x))
    # get stage info.
    df_stages=df_stages.explode('stages')
    for k in ['code','dateTime']:
        df_stages['stage_'+k]=df_stages['stages'].apply(lambda x: x[k] if type(x)==type({}) and k in x.keys() else None)
    # necessary hack to deal with strange formatting (datetime.datetime instead of pandas._libs.tslibs.timestamps.Timestamp)
    df_stages['stage_dateTime']= df_stages['stage_dateTime'].apply(lambda x: str(x).split(':')[0] if x!=None else None)
    df_stages['stage_dateTime']= pd.to_datetime(df_stages['stage_dateTime'],format='%Y-%m-%dT%H:%M:%S.%f',errors='coerce')
    df_stages=df_stages.reset_index(drop=True)

    return df_stages



### List of formatting commands
def FormatTestDataII(myClient, testInfo, chunk=100):
    # list of test runs
    testList=[]
    # loop over IDs
    for x in range(0,int(np.ceil(len(testInfo)/chunk))):
        print(f"test loop {x}: [{x*chunk}:{(x+1)*chunk}]")
        bulkFail=False
        for z in range(0,10,1):
            try:
                testChunk=myClient.get('getTestRunBulk',json={'testRun':[ti['id'] for ti in testInfo[x*chunk:(x+1)*chunk]]})
                break
            except requests.exceptions.ConnectionError:
                print(f"ERROR\n{e}")
                print(f"CONNECTION ISSUE - TRY AGAIN {z}")
            except itkdb.exceptions.ServerError as e:
                print(f"ERROR\n{e}")
                print(f"SERVER ISSUE - TRY AGAIN {z}")
            if z==9:
                bulkFail=True
        if bulkFail:
            print("- bulk retrieval has failed. Skipping chunk!")
            continue
        testList.extend(testChunk)

    # convert data to pandas dataFrame
    df_testRuns=pd.DataFrame([x for x in testList if x!=None])

    # check non-empty
    if df_testRuns.empty:
        print("No tests found")
        return pd.DataFrame()
    
    print(df_testRuns.columns)    
    # use ready things if possible
    if "state" in df_testRuns.columns:
        df_testRuns=df_testRuns.query('state=="ready"')[['components','institution','testType','date','properties','results','passed']]
    else: 
        df_testRuns=df_testRuns[['components','institution','testType','date','properties','results','passed']]

    # get codes if possible
    for k in df_testRuns.columns:
        df_testRuns[k]= df_testRuns[k].apply(lambda x: FindKey(x))

    # convert dateTime format
    df_testRuns['date']= pd.to_datetime(df_testRuns['date'],format='%Y-%m-%dT%H:%M:%S.%f')
    # simple unpacking
    for k in ['serialNumber','alternativeIdentifier']:
        try:
            df_testRuns[k]=df_testRuns['components'].apply(lambda x: x[k] if type(x)==type({}) and k in x.keys() else None)
        except KeyError: # except missing
            pass
    # get component info.
    df_testRuns=df_testRuns.explode('components')
    df_testRuns['compCode']=df_testRuns['components'].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else None)
    for k,v in {'compTypeCode':"componentType", 'projCode':"project", 'typeCode':"type", 'stage':"testedAtStage"}.items():
        df_testRuns[k]=df_testRuns['components'].apply(lambda x: x[v]['code'] if type(x)==type({}) and v in x.keys() and type(x[v])==type({}) and "code" in x[v].keys() else None)
    for k,v in {'serialNumber':"serialNumber", 'alternativeIdentifier':"alternativeIdentifier"}.items():
        df_testRuns[k]=df_testRuns['components'].apply(lambda x: x[v] if type(x)==type({}) and v in x.keys() and type(x[v])==type("str") else None)
    df_testRuns['localName']=df_testRuns['components'].apply(lambda x: next((item['value'] for item in x['properties'] if type(item)==type({}) and "code" in item.keys() and item['code']=="LOCALNAME"), None) if type(x)==type({}) and "properties" in x.keys() and type(x['properties'])==type([]) else None)
    # identifier per test
    # combCollection=[]
    # df_testRuns['testIndex']=df_testRuns.apply(lambda row: GetIndex(row['institution'],row['compCode'],combCollection), axis=1)

    ### test info. part
    df_testRuns=df_testRuns.explode('results')
    for k in ["valueType", "dataType"]:
        df_testRuns[k]=df_testRuns['results'].apply(lambda x: x[k] if type(x)!=type(None) else None)
    df_testRuns['paraCode']=df_testRuns['results'].apply(lambda x: x['code'] if type(x)!=type(None) else None)
    df_testRuns['paraValue']=df_testRuns['results'].apply(lambda x: x['value'] if type(x)!=type(None) else None)
    # after all unpacking, reset the dataframe index
    df_testRuns=df_testRuns.reset_index(drop=True)

    return df_testRuns


def OverviewChartII(myClient, df_compInfo, nameStr=None):

    uploads=[]
    df_sum=df_compInfo.copy(deep=True)
        
    for compType in df_sum['componentType'].unique():
        if nameStr==None:
            uploads.append({'dictList':[], 'name':"Overview"+ " Comp DI Summary"})
        else:
            uploads.append({'dictList':[], 'name':"Overview"+ " Comp DI Summary"})

        print(f"look for {compType}")
        df_compType=df_sum.query('componentType=="'+compType+'"')
        if df_compType.empty:
            print(f"But I don't see any: {compType}")
            continue
    
        projCode=df_compType['project'].unique()[0]
        ### get compType schema
        compTypeInfo=myClient.get('getComponentTypeByCode', json={'project':projCode,'code':compType})
        ### and stage order
        stageOrderList=None
        if "stages" in compTypeInfo.keys():
            stageOrderList=[x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]
        
        ### and stage order
        if "stages" in compTypeInfo.keys():
            stageOrderList=[x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]
    
    # column for plotting
    df_sum['assembly']=df_sum['assembled'].apply(lambda x: "Yes" if x==True else "No or Unset")
    # plot
    subChart=alt.Chart(df_sum).mark_bar().encode(
        y=alt.Y('componentType:N'),
    #     x=alt.X('date:N', axis = alt.Axis(title = "Date", format = ("%b %Y"))),
        x=alt.X('count(type):Q'),
        color=alt.Color('type:N'),
        opacity=alt.Opacity('assembly:N'),
        detail=alt.Detail('currentLocation:N'),
        tooltip=['currentStage:N','type:N','currentLocation:N','count(type):Q','assembly:N']
        ).configure_point(size=60).properties(width=600, height=300, title="Sub Types")
    #             uploads[-1]['dictList'].append({'text':"### Current Stage", 'plot':curChart})
    # remove plotting column
    df_sum=df_sum.drop(columns=['assembly'])

    # count populations
    df_pop=df_sum.groupby(by=["componentType"]).count().reset_index()
    df_pop=df_pop[['componentType','code']].rename(columns={'code':"count"})
    df_pop['assembled_Y']= df_pop['componentType'].apply(lambda x: len(df_sum.query(f'componentType=="{x}" & assembled==True') ) ) 
    df_pop['assembled_N']= df_pop['componentType'].apply(lambda x: len(df_sum.query(f'componentType=="{x}" & assembled==False') ) ) 
    df_pop['assembled_?']= df_pop['componentType'].apply(lambda x: len(df_sum.query(f'componentType=="{x}" & assembled!=True & assembled!=False') ) )      
    uploads[-1]['dictList'].append({'text':"### Overview"})
    uploads[-1]['dictList'].append({'text':f"__Total component count {len(df_sum)} @ institution__ - assembled (Y, N, ?): {len(df_sum.query('assembled==True'))}, {len(df_sum.query('assembled==False'))}, {len(df_sum.query('assembled!=True and assembled!=False'))}", 'df':df_pop, 'plot':subChart})

#   uploads[-1]['dictList'].append({'text':"### Summary Info.", 'df':df_compType})
    uploads[-1]['dictList'].append({'text':"### Full Inventory Info.", 'df':df_sum})
            
    # ### make report page per type
    # for ct in df_sum['type'].unique():
    #     uploads.append({'dictList':[], 'name':ct})
    return uploads


def CustomComponentSummaryII(myClient, df_compInfo, nameStr=None):

    uploads=[]
    df_sum=df_compInfo.copy(deep=True)
    
    for compType in df_sum['componentType'].unique():
        if nameStr==None:
            uploads.append({'dictList':[], 'name':compType+" Comp DI Summary"})
        else:
            uploads.append({'dictList':[], 'name':nameStr+" Comp DI Summary"})

        print(f"look for {compType}")
        df_compType=df_sum.query('componentType=="'+compType+'"')
        if df_compType.empty:
            print(f"But I don't see any: {compType}")
            continue
    
        projCode=df_compType['project'].unique()[0]
        ### get compType schema
        compTypeInfo=myClient.get('getComponentTypeByCode', json={'project':projCode,'code':compType})
        ### and stage order
        stageOrderList=None
        if "stages" in compTypeInfo.keys():
            stageOrderList=[x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]
        
        ### and stage order
        if "stages" in compTypeInfo.keys():
            stageOrderList=[x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]
            
            df_pop=df_compType.groupby(by=["type"]).count().reset_index() 
            df_pop=df_pop[['type','code']].rename(columns={'code':"count"})
            df_pop['assembled_Y']= df_pop['type'].apply(lambda x: len(df_compType.query(f'type=="{x}" & assembled==True') ) ) 
            df_pop['assembled_N']= df_pop['type'].apply(lambda x: len(df_compType.query(f'type=="{x}" & assembled==False') ) ) 
            df_pop['assembled_?']= df_pop['type'].apply(lambda x: len(df_compType.query(f'type=="{x}" & assembled!=True & assembled!=False') ) )    
            # uploads[-1]['dictList'].append({'text':f"__Total {compType} count {len(df_compType)}__", 'df':df_pop})
            uploads[-1]['dictList'].append({'text':f"__Total {compType} count {len(df_compType)}__ - assembled (Y, N, ?): {len(df_compType.query('assembled==True'))}, {len(df_compType.query('assembled==False'))}, {len(df_compType.query('assembled!=True and assembled!=False'))}", 'df':df_pop})

            # column for plotting
            df_compType['assembly']=df_compType['assembled'].apply(lambda x: "Yes" if x==True else "No or Unset")
            # plot
            df_compType['assembled']=df_compType['assembled'].astype(str)

            colList=['serialNumber','alternativeIdentifier','componentType','type','currentStage','currentLocation','institution','assembly','state','reworked','trashed','completed','dummy']
            df_compType=df_compType[colList]

            subChart=alt.Chart(df_compType).mark_bar().encode(
                y=alt.Y('type:N'),
            #     x=alt.X('date:N', axis = alt.Axis(title = "Date", format = ("%b %Y"))),
                x=alt.X('count(type):Q'),
                color=alt.Color('type:N'),
                opacity=alt.Opacity('assembly:N'),
                detail=alt.Detail('currentLocation:N'),
                tooltip=['currentStage:N','type:N','currentLocation:N','count(type):Q','assembly:N']
                ).transform_calculate(
                    assembly= 'datum.assembled=="True" ? "Yes" : "No or Unset"'
                ).configure_point(size=60).properties(width=600, height=300, title="Sub Types")
            #             uploads[-1]['dictList'].append({'text':"### Current Stage", 'plot':curChart})
            uploads[-1]['dictList'].append({'text':"### Sub Types", 'plot':subChart})
            # remove plotting column
            df_compType=df_compType.drop(columns=['assembly'])

            curChart=alt.Chart(df_compType).mark_bar().encode(
                y=alt.Y('currentStage:N', sort=stageOrderList),
            #     x=alt.X('date:N', axis = alt.Axis(title = "Date", format = ("%b %Y"))),
                x=alt.X('count(curStage):Q', sort=stageOrderList),
                color=alt.Color('type:N'),
                detail=alt.Detail('currentLocation:N'),
                tooltip=['currentStage:N','type:N','currentLocation:N','count(currentStage):Q']
                ).configure_point(size=60).properties(width=600, height=300, title="Current Stage")
#             uploads[-1]['dictList'].append({'text':"### Current Stage", 'plot':curChart})
            uploads[-1]['dictList'].append({'text':"### Current Stage", 'plot':curChart})

            uploads[-1]['dictList'].append({'text':"### Summary Info.", 'df':df_compType})
            
    # ### make report page per type
    # for ct in df_sum['type'].unique():
    #     uploads.append({'dictList':[], 'name':ct})
    return uploads


def StageSummaryII(myClient, df_stageInfo):

    repPage=[]
    df_sum=df_stageInfo.copy(deep=True)
    # print(df_sum.columns)
    
    for compType in df_sum['componentType'].unique():
        repPage.append({'dictList':[], 'name':str(compType)+" Stage DI Summary"})

        df_compType=df_sum.query('componentType=="'+compType+'"')
    
        projCode=df_compType['project'].unique()[0]
        ### get compType schema
        compTypeInfo=myClient.get('getComponentTypeByCode', json={'project':projCode,'code':compType})
        ### and stage order
        try:
            stageOrderList=[x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]
        except KeyError:
            stageOrderList="descending"
        
        df_scl_hoz=pd.DataFrame()
        df_scl_vert=pd.DataFrame()

        # ### try to pick useful identifier (use code if nothing else)
        # idCol="serialNumber"
        # for id in ['serialNumber','code']: #,'alternativeIdentifier'
        #     if id in df_compType.columns and len(df_compType[id].unique())!=1:
        #         idCol=id
        #         break
        idCol="code" # stage plots don't use identifier, only count total objects
        print(f"using identifier: {idCol}")
        
#         display(df_compType)
        ### stage check list
        for e,sn in enumerate(df_compType[idCol].unique()):
#             serNum=df_compType.query(idCol+'=="'+str(sn)+'"')[idCol].values[0]
            try:
                serNum=df_compType.query(f'{idCol}=="{sn}"')[idCol].values[0]
            except IndexError:
                print(f"Can't work with {sn}")
                # serNum=None
                continue
            print("working on:",serNum)
            if "stages" not in compTypeInfo.keys():
                print(" - skipping: no stage data")
                continue
            df_scl=pd.DataFrame(compTypeInfo['stages'])[['code','name','order','final','initial']]
            df_scl['special']=None
            for c in ['final','initial']:
                df_scl['special']=df_scl.apply(lambda row: SpecialString(row,c,"special"), axis=1)
            df_scl=df_scl.drop(columns=['initial','final']).reset_index(drop=True)
            df_scl['compCheck']="False"
            df_scl['compCheck']=df_scl.apply(lambda row: "True" if row['code'] in df_compType.query(f'code=="{sn}"')['stage_code'].to_list() else "False", axis=1)
#             df_scl['compCheck']=df_scl.apply(lambda row: "True" if row['code'] in df_compType.query('code=="'+str(sn)+'"')['stage_code'].to_list() else "False", axis=1)
            # note current stage
            # rowInd=df_scl.index[df_scl['code'] == compInfo[e]['currentStage']['code']].tolist()[0]
            # df_scl.at[rowInd,'compCheck']="current"
            df_scl['date']=None
            df_scl['date']=df_scl.apply(lambda row: df_compType.query(f'code=="{sn}" | stage_code=="{row["code"]}"')['stage_dateTime'].values[0] if row['code'] in df_compType.query(f'code=="{sn}"')['stage_code'].to_list() else None, axis=1)
#             df_scl['date']=df_scl.apply(lambda row: df_compType.query('code=="'+str(sn)+'" | stage_code=="'+row['code']+'"')['stage_dateTime'].values[0] if row['code'] in df_compType.query('code=="'+sn+'"')['stage_code'].to_list() else None, axis=1)
        #     display(df_scl)
            df_scl['ident']=serNum # can't call it code! - confusion with stage code
#             df_scl['type']=df_compType.query(idCol+'=="'+sn+'"')['type'].values[0]
            df_scl['type']=df_compType.query(f'{idCol}=="{sn}"')['type'].values[0]
            if serNum==None:
                serNum="None_"+str(e)
            # print(f"e_{e}: {serNum}")
            if e==0:
                df_scl_hoz=df_scl.rename(columns={'compCheck':serNum+"_compCheck",'date':serNum+"_date"}).drop(columns=[idCol])
                df_scl_vert=df_scl
            else:
                df_scl_hoz=pd.concat([df_scl_hoz, df_scl[['compCheck','date']] ], axis=1).rename(columns={'compCheck':serNum+"_compCheck",'date':serNum+"_date"})
                df_scl_vert=pd.concat([df_scl_vert, df_scl], axis=0)
        
        print(df_compType[idCol].unique())
        # display(df_scl_vert)

        if df_scl_vert.empty:
            print(f"No data here: {compType}")
            continue
        df_scl_vert=df_scl_vert.query('compCheck!="False"').reset_index(drop=True)

        # another hack to avoid JSON serilsation error
        df_scl_vert['date']=df_scl_vert['date'].astype(str)

        repPage[-1]['dictList'].append({'text':"## Stages Visited"})
        repPage[-1]['dictList'].append({'text':"Per componentType plots"})
        for ct in df_scl_vert['type'].unique():
            if ct==None: continue
            df_plot=df_scl_vert.query('type=="'+str(ct)+'"')
            rect=alt.Chart(df_plot).mark_rect().encode(
                y=alt.Y('code:N', axis=None, sort=stageOrderList),
                color=alt.Color('count(ident):Q', legend=None, scale=alt.Scale(scheme=alt.SchemeParams(name='purples'))), # title="#entries"),
                tooltip=['code:N','count(ident):Q']
            )
            text=alt.Chart(df_plot).mark_text(lineBreak=r'\n').encode(
                y=alt.Y('code:N', sort=stageOrderList),
                text=alt.Text('label:N')
            ).transform_joinaggregate(
                count='count(ident):Q',
                groupby=["code"]
            ).transform_calculate(
                label=alt.datum.code + " \ncount: " + alt.datum.count
            )
            stageCombPlot=(rect+text).properties(width=1000, height=200, title=str(ct)+" Stages Visited")
            repPage[-1]['dictList'].append({'text':"### "+str(ct), 'plot':stageCombPlot})

    return repPage


def StageTimeLinesII(myClient, df_stageInfo):
    
    repPage=[]
    df_sum=df_stageInfo.copy(deep=True)
    # print(df_sum.columns)

    for compType in df_sum['componentType'].unique():
        repPage.append({'dictList':[], 'name':compType+" Stage Timeline"})

        df_compType=df_sum.query('componentType=="'+compType+'"')
    
        projCode=df_compType['project'].unique()[0]
        ### get compType schema
        compTypeInfo=myClient.get('getComponentTypeByCode', json={'project':projCode,'code':compType})
        ### and stage order
        try:
            stageOrderList=[x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]
        except KeyError:
            stageOrderList="descending"

        ### try to pick useful identifier (use code if nothing else)
        idCol="serialNumber"
        for id in ['serialNumber','code']: #,'alternativeIdentifier'
            if id in df_compType.columns and len(df_compType[id].unique())!=1:
                idCol=id
                break
        print(f"using identifier: {idCol}")

        df_compType.sort_values(by=[idCol,'stage_dateTime'], inplace=True)
        df_compType=df_compType.reset_index(drop=True)

        ### upload timelines per type
        repPage[-1]['dictList'].append({'text':"## Stage History"})
        repPage[-1]['dictList'].append({'text':"Per componentType plots followed by full data table"})
        for ct in df_compType['type'].unique():
            print(f"### {ct}")
            stageChart=alt.Chart(df_compType.query('type=="'+str(ct)+'"')).mark_line(point=True).encode(
                x=alt.X('stage_dateTime:T',axis = alt.Axis(title = "Date", format = ("%b %Y"))),
            #     x=alt.X('date:N', axis = alt.Axis(title = "Date", format = ("%b %Y"))),
                y=alt.Y('stage_code:N',title="Stage",sort=stageOrderList),
                color=alt.Color(idCol+':N', legend=None),
                order='stage_dateTime',
                tooltip=['stage_dateTime:T','stage_code:N',idCol+':N']
            ).configure_point(size=60).properties(width=600, height=300, title=str(ct)+" Stage History").interactive()
            repPage[-1]['dictList'].append({'text':"### "+str(ct), 'timeline':stageChart})
        repPage[-1]['dictList'].append({'text':"### Full stage history data",'df':df_compType})

    return repPage


def TestSummaryII(myClient, df_runInfo):

    repPage=[]
    df_sum=df_runInfo.copy(deep=True)
    print(df_sum.columns)

    for compType in df_sum['compTypeCode'].unique():
        repPage.append({'dictList':[], 'name':compType+" Test DI Summary"})

        df_compType=df_sum.query('compTypeCode=="'+compType+'"')
    
        projCode=df_compType['projCode'].unique()[0]
        ### get compType schema
        compTypeInfo=myClient.get('getComponentTypeByCode', json={'project':projCode,'code':compType})
        ### and stage order
        stageOrderList=[x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]

        ### get compType schema
        compTypeInfo=myClient.get('getComponentTypeByCode', json={'project':projCode,'code':compType})

        df_scl=pd.DataFrame(compTypeInfo['stages'])
        df_tcl_vert=None
        df_tcl_vert_comb=pd.DataFrame()
        # display(df_scl)
        for stgCode in df_scl['code'].unique():
            print("working on:",stgCode)
            df_tcl=df_scl.query('code=="'+stgCode+'"').copy(deep=True).reset_index(drop=True)
            df_tcl=df_tcl.explode('testTypes').reset_index(drop=True) #.explode('testTypes')
        #     display(df_tcl)
            df_tcl['testType']=None
            for i,row in df_tcl.iterrows():
                try:
                    print(i,row['testTypes']['testType'])
                    retVal=myClient.get('getTestType', json={'id':row['testTypes']['testType']['id']})
                    df_tcl.at[i,'testType']=retVal['code']
                except TypeError:
                    pass
        #     df_tcl=df_tcl['testType']=df_tcl['testTypes'].apply(lambda x: GetTT(x['testType']) if x!=None else x)
            for c in ['order','receptionTest','receptionTestOnly','nextStage']:
                try:
                    df_tcl[c]=df_tcl['testTypes'].apply(lambda x: x[c])
                except TypeError:
                    df_tcl[c]=None
                except KeyError:
                    df_tcl[c]=None
            df_tcl['special']=None
            for c in ['alternative','final','initial']:
                df_tcl['special']=df_tcl.apply(lambda row: SpecialString(row,c,"special"), axis=1)
            df_tcl=df_tcl.drop(columns=['alternative','final','initial'])
            df_tcl['special2']=None
            for c in ['receptionTest','receptionTestOnly','nextStage']:
                df_tcl['special2']=df_tcl.apply(lambda row: SpecialString(row,c,"special2"), axis=1)
            df_tcl_hoz=df_tcl.copy(deep=True)
            df_tcl_vert=df_tcl.copy(deep=True)
            
            ### per component loop
            ### try to pick useful identifier (use code if nothing else)
            idCol="serialNumber"
            for id in ['serialNumber','alternativeIdentifier','compCode']:
                if id in df_compType.columns and len(df_compType[id].unique())!=1:
                    idCol=id
                    break
            print(f"using identifier: {idCol}")
            
            for sn in df_runInfo[idCol].unique():
                if sn==None: continue
                # print("\t- checking:",sn)
                df_tcl_vert[idCol]=sn
                df_tcl_vert['compCheck']=False
                if not df_runInfo.query(f'{idCol}=="{sn}"').empty:
                    df_tcl_vert['typeCode']=df_runInfo.query(f'{idCol}=="{sn}"')['typeCode'].values[0]
                    df_tcl_vert['compCheck']=df_tcl_vert['testType'].apply(lambda x: True if x in df_runInfo.query(idCol+'=="'+sn+'" & stage=="'+stgCode+'"')['testType'].to_list() else False)
                else:
                    df_tcl_vert['typeCode']=f'UNKNOWN ({idCol}:{sn})'
                for c in ['passed','date']:
                    df_tcl_vert[c]=None
                    df_tcl_vert[c]=df_tcl_vert.apply(lambda row: df_runInfo.query('stage=="'+stgCode+'" & testType=="'+row['testType']+'"')[c].values[0] if row['compCheck']==True else None, axis=1)
                try:
                    df_tcl_vert_comb=pd.concat([df_tcl_vert_comb,df_tcl_vert]).reset_index(drop=True)
                except TypeError:
                    df_tcl_vert_comb=df_tcl_vert
        if df_tcl_vert_comb.empty: continue
        for col in ['receptionTest','receptionTestOnly','nextStage','testTypes']:
            try:
                df_tcl_vert_comb=df_tcl_vert_comb.drop(columns=col)
            except KeyError:
                pass
        try:
            df_tcl_vert_comb=df_tcl_vert_comb.rename(columns={'code':"stageCode"})
            df_tcl_vert_comb=df_tcl_vert_comb[['testType','typeCode','order','code','special','special2']+[idCol,'compCheck','date','passed'] ]
        except KeyError:
            pass
        
        # another hack to avoid JSON serilsation error
        df_tcl_vert_comb['date']=df_tcl_vert_comb['date'].astype(str)

        ### test population bubble map with pass=True/False seperation
        for ct in df_tcl_vert_comb['typeCode'].unique():
            repPage[-1]['dictList'].append({'text':"## Populations Per Test"})
            df_plot=df_tcl_vert_comb.query('compCheck==True & typeCode=="'+str(ct)+'"')
            testOrderList=list(df_plot.sort_values(by=['order'])['testType'].unique())
            testOrderList.reverse()
            bubTrue=alt.Chart(df_plot.query('passed==True')).mark_circle(color="green", xOffset=15).encode(
                x=alt.X('stageCode:N', title=None, axis=alt.Axis(labelAngle=-45), sort=stageOrderList),
                y=alt.Y('testType:N', axis=None, sort=testOrderList),
                size=alt.Size('count('+idCol+'):Q', legend=None), #title="#entries"),
                tooltip=['count('+idCol+'):Q',]
            )
            bubFalse=alt.Chart(df_plot.query('passed==False')).mark_circle(color="red", xOffset=-15).encode(
                x=alt.X('stageCode:N', sort=stageOrderList),
                y=alt.Y('testType:N', sort=testOrderList),
                size=alt.Size('count('+idCol+'):Q',),
                tooltip=['testType:N','count('+idCol+'):Q',]
            )
            rect=alt.Chart(df_plot).mark_rect(filled=False).encode(
                x=alt.X('stageCode:N', sort=stageOrderList),
                y=alt.Y('testType:N', sort=testOrderList)
            )
            text=alt.Chart(df_plot).mark_text().encode(
                x=alt.X('stageCode:N', sort=stageOrderList),
                y=alt.Y('testType:N', sort=testOrderList),
                text=alt.Text('testType:N')
            )
            testCombPlot=(bubTrue+bubFalse+rect+text).properties(width=1000, height=700, title={
                "text": ["passed tests"],
                "subtitle": ["failed tests"],
                "color": "green",
                "subtitleColor": "red",
                }).interactive()
            repPage[-1]['dictList'].append({'text':str(ct), 'plot':testCombPlot})


        ### stage-test bar map
        for ct in df_tcl_vert_comb['typeCode'].unique():
            repPage[-1]['dictList'].append({'text':"## Tests Per Stage"})
            df_plot=df_tcl_vert_comb.query('compCheck==True & typeCode=="'+str(ct)+'"')
            testOrderList=list(df_plot.sort_values(by=['order'])['testType'].unique())
            bar=alt.Chart(df_plot).mark_bar().encode(
                x=alt.X('stageCode:N', title=None, axis=alt.Axis(labelAngle=-45), sort=stageOrderList),
                y=alt.Y('count(testType):Q'),
                color=alt.Color('testType:N', sort=testOrderList),
                size=alt.Size('passed:N'),
                tooltip=['count(testType):Q','testType:N']
            ).properties(width=300, title=str(ct)+" Tests Per Stage")
            # display(bar)
            repPage[-1]['dictList'].append({'text':str(ct), 'plot':bar})

    
    return repPage


def TestTimeLinesII(myClient, df_runInfo):
    
    repPage=[]
    df_sum=df_runInfo.copy(deep=True)
    # print(df_sum.columns)

    for compType in df_sum['compTypeCode'].unique():
        repPage.append({'dictList':[], 'name':compType+" Test Timeline"})

        df_compType=df_sum.query('compTypeCode=="'+compType+'"')
    
        projCode=df_compType['projCode'].unique()[0]
        ### get compType schema
        compTypeInfo=myClient.get('getComponentTypeByCode', json={'project':projCode,'code':compType})
        ### and stage order
        stageOrderList=[x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]

        # timeline
        for ct in df_runInfo['typeCode'].unique():
            repPage[-1]['dictList'].append({'text':"## Tests Uploads"})
            df_plot=df_runInfo.query('typeCode=="'+str(ct)+'"').reset_index(drop=True)
            ### try to pick useful identifier (use code if nothing else)
            idCol="serialNumber"
            for id in ['serialNumber','alternativeIdentifier','compCode']:
                if id in df_compType.columns and len(df_compType[id].unique())!=1:
                    idCol=id
                    break
            print(f"using identifier: {idCol}")
            testChart=alt.Chart(df_plot).mark_circle(size=60).encode(
                x=alt.X('date:T',axis = alt.Axis(title = "Date", format = ("%b %Y"))),
            #     x=alt.X('date:N', axis = alt.Axis(title = "Date", format = ("%b %Y"))),
                y=alt.Y('stage:N',title="stage",sort=stageOrderList),
                color=alt.Color('testType:N'),
                shape=alt.Shape('passed:N'),
                tooltip=[idCol+':N','date:T','stage:N','testType:N','passed:N']
                ).properties(width=600, height=300, title=str(ct)+" Test History").interactive()
            repPage[-1]['dictList'].append({'text':str(ct), 'timeline':testChart, 'df':df_runInfo.query(f'typeCode=="{ct}"')})
            # display(testChart)
        
    return repPage


def DataPaneChunkII(myClient, dic_plots, repSpec, fullSpec):
    ### datetime & credit
    dateStr=GetDatetime("date")
    timeStr=GetDatetime("time")
    credStr=MakeCreditStr(myClient)
    print(f"temporal location: {timeStr} @ {dateStr}")

    ### object map to cast types
    dpMap={'plot':dp.Plot,'line':dp.Plot,'timeline':dp.Plot,'hist':dp.Plot,'df':dp.DataTable,'tab':dp.Table,'text':dp.Text}

    ### datapane pagination
    pages=[]
    # front page
    madeStr="### Made on: "+dateStr+" @ "+timeStr
    madeStr+=", by "+credStr
    quoteTxt="esse est percipi"
    if "quote" in repSpec.keys():
        quoteTxt=repSpec['quote']
    pages.append(dp.Page(title="Notes", blocks=[
        dp.Text("# "+repSpec['reportName']),
        dp.Text(madeStr),
        dp.Text(" --- "),
        dp.Text("## Brought to you by ITk-reports "),
        dp.Text(f"> {quoteTxt}"),
        dp.Text("Check *Broom Cupboard* tab for report json"),
        dp.Text("### git repo: [itk-reports](https://gitlab.cern.ch/wraight/itk-reports)"),
        dp.Text("## Tot ziens! ")
            ]))

    for a, b in dic_plots.items():
        # if a=='standard_plots':
        #     print("### Standard Plots")
        for sp in b:
            print("packing:",sp['name'])
            blockList=[]
            for dl in sp['dictList']:
                print("in dictlist")
                for k,v in dl.items():
                    print(f"in dictlist {k}")
                    if "DataFrame" not in str(type(v)):
                        if v==None:
                            print("\t this is None. skipping")
                            continue
                    else:
                        if v.empty:
                            print("\t this is empty. skipping")
                            continue
                    print(f"\t uploading: {dpMap[k]}") #"({v})")
                    print(f"\t {str(type(dpMap[k]))}")
                    if type(v)==type("str") and len(v)<1:
                        v="[empty string]"
                    try:
                        blockList.append(dpMap[k](v))
                    except TypeError:
                        print("Type Error for ("+k+"):",dpMap[k])
            print(len(blockList))
            if len(blockList)==0: continue
            pages.append(dp.Page(title=sp['name'], blocks=blockList))
            print(sp['name'])

    # report specifications
    print("### Report Spec")
    ### remove sensitive stuff
    theSpec=copy.deepcopy(fullSpec)
    for k in ['user','password','path','datapaneCode','token','id','alert']:
        for dist in theSpec['distribution']:
            if k in dist.keys():
                dist[k]="CENSORED"
    ### write specs page
    pages.append(dp.Page(title="Broom Cupboard", blocks=[
        dp.Text(f"### This report: {repSpec['alias']}"),
        dp.Code(code=str(json.dumps(theSpec, indent=4)), language="python")
    # dp.Text("## Input Specifications"),
    # *[str(k)+": "+str(v) for k,v in repSpec.items()]
        ]))

    repInfo=repSpec
    ### saving report

    # remote flag - check
    remoteSave=False
    if "remote" in repInfo['location'].lower():
        print("Uploading remotely.")
        ### datapane
        if "datapane".lower() in repInfo['location']:
            print("- datapane host")
            if "code" not in repInfo.keys() or repInfo['code']==None:
                print("### Cannot upload report. Please enter datapane code to generate URL. Skip remote save.")
            else:
                print("- Got datapane info.")
                dp.login(token=repInfo['code'])
                dp.upload_report(pages, name=repInfo['reportName'], publicly_visible=True)
                remoteSave=True
        if "gitlab".lower() in repInfo['location']:
            print("- gitlab host")
            ### gitLab
            if "id" not in repInfo.keys() or repInfo['id']==None or "token" not in repInfo.keys() or repInfo['token']==None :
                print("Cannot upload report. Please enter git ID & token to generate URL. Skip remote save.")
            else:
                print("- Got gitRepo info.")
                dp.save_report(pages, path="./temp_report.html")
                if ScriptCommit("https://gitlab.cern.ch", repInfo['id'], "docs/"+repInfo['reportName']+".html", repInfo['token'], "master", "./temp_report.html"):
                    print("Sent to gitlab successfully")
                    remoteSave=True
                else:
                    print("Something went awry")
        if "eos".lower() in repInfo['location']:
            print("- eos host")
            if "user" not in repInfo.keys() or repInfo['user']==None or "password" not in repInfo.keys() or repInfo['password']==None or "path" not in repInfo.keys() or repInfo['path']==None:
                print("### Cannot upload report. Please enter eos user, password and path to generate URL. Skip remote save.")
            else:
                print("- Got eos info.")
                dp.save_report(pages, path="./temp_report.html")
                if repInfo['path'][-1]==".":
                    repInfo['path']=repInfo['path'][0:-1]
                if repInfo['path'][-1]!="/":
                    repInfo['path']=repInfo['path']+"/"
                if AltCopy(repInfo['user'], repInfo['password'], repInfo['path']+repInfo['reportName']+".html", "./temp_report.html"):
                    print("Sent to eos successfully")
                    remoteSave=True
                else:
                    print("Something went awry")
        if remoteSave==False:
            print("### No remote save successful. Save locally...")
        else:
            print("### Remote saving successful :)")


    ### save local report
    if "local" in repInfo['location'].lower() or remoteSave==False:
        print("Saving locally.")
        if "reportDir" not in repInfo.keys() or repInfo['reportDir']==None:
            print("- No reportDir specified. Saving locally.")
            if "reportName" not in repInfo.keys() or repInfo['reportName']==None:
                print(f"  - No reportName specified. Saving: {madeStr.replace(' ','_')}")
                dp.save_report(pages, path="report_"+madeStr.replace(' ','_')+".html")
            else:
                dp.save_report(pages, path=repInfo['reportName']+".html")
        else:
            dp.save_report(pages, path=repInfo['reportDir']+"/"+repInfo['reportName']+".html")
    
def StageTestTable(df_runInfo):
    
    repPage=[]
    df_sum=df_runInfo.copy(deep=True)
    print(df_sum.columns)

    processed=0
    for ct in df_sum['compTypeCode'].unique():
        repPage.append({'dictList':[], 'name':ct+" Stage and Test Table"})
        df_ct=df_sum.query(f'compTypeCode=="{ct}"') 
        print(f"Working on compType: {ct} ({len(df_ct) }/{len(df_sum) } )")
        for st in df_ct['typeCode'].unique():
            df_st=df_ct.query(f'typeCode=="{st}"') 
            print(f"  - sub-type: {ct} ({len(df_st) } )")
            df_temp=pd.DataFrame()
            for tt in df_st['testType'].unique():
                df_tt=df_st.query(f'testType=="{tt}"') 
                print(f"    - testType: {tt} ({len(df_tt) })")
                temp_test=[]
                temp_stage=[]
                count_rows=0
                test_stage_list=['serialNumber', 'alternativeIdentifier', 'institution', 'compTypeCode', 'projCode', 'typeCode']
                i=0
                temp_df_runInfo=df_tt
                # print(f'length temp_df_runInfo: {len(temp_df_runInfo)}')
                for k,v in temp_df_runInfo['components'].items():
                    if v['testedAtStage']['code'] not in temp_stage:
                        temp_stage.append(v['testedAtStage']['code'])
                print(f"    - test_stage: {temp_stage}")
                for k,v in temp_df_runInfo['testType'].items():
                    if v not in temp_test:
                        temp_test.append(v)
                print(f"    - temp_test: {temp_test}")
    #             print(df_sum['components'][0])
                for a in temp_test:
                    for b in temp_stage:
                        df_sum[str(a)+"@"+str(b)]=None
                        test_stage_list.append(str(a)+"@"+str(b))
                count_rows=df_sum.shape[0]
                for i in range(0, count_rows):
                    for a in temp_test:
                        for b in temp_stage:
                            if str(a)==df_sum.at[i, 'testType'] and str(b)== df_sum.at[i, 'stage']:
                                df_sum.at[i, str(a)+"@"+str(b)]=df_sum.at[i, 'passed']
                # print(f'length df_sum -1: {len(df_sum)}')

                df_sry=df_sum.sort_values(by='date', ascending = False)
                # print(f'length df_sry 0: {len(df_sry)}')
                df_sry=df_sry.query(f'typeCode=="{st}" and testType=="{tt}"')[test_stage_list].drop_duplicates().reset_index(drop=True).copy()
                # print(f'length df_sry A: {len(df_sry)}')
                bool_columns = df_sry.select_dtypes(include=bool).columns.tolist()
                non_bool_columns = [col for col in df_sry.columns if col not in bool_columns]
                agg_funcs = {col: 'max' for col in bool_columns}
                agg_funcs.update({col: 'first' for col in non_bool_columns if col != 'serialNumber'})
                df_sry = df_sry.groupby(['serialNumber']).agg(agg_funcs).reset_index()
                # print(f'length df_sry Z: {len(df_sry)}')
#                 display(df_sry)
                if df_temp.empty:
                    df_temp=df_sry
                else:
                    df_temp=df_temp.merge(df_sry, how='outer', on=['serialNumber', 'alternativeIdentifier', 'institution', 'compTypeCode', 'projCode', 'typeCode']).reset_index(drop=True)
                processed+=len(df_tt)
                print(f"    processed: {processed}")
#             display(df_temp.drop_duplicates().reset_index(drop=True))
            # print(f"add to dictList: {len(df_temp)}")
            repPage[-1]['dictList'].append({'text':"## Stages & Tests Table"})
            repPage[-1]['dictList'].append({'text':str(st), 'df':df_temp.drop_duplicates().reset_index(drop=True)})
        
    return repPage
