### get itkdb for PDB interaction
import os
import sys
# !{sys.executable} -m pip install itkdb==0.4
import itkdb
import itkdb.exceptions as itkX


###############
### PDB credentials parts
###############

# def PrintPackageVersions():
#     packList=[pd,alt,np,itkdb]
#     for p in packList:
#         print(p.__name__+" : "+p.__version__)

def CheckEnv(ref):
    try:
        env=os.environ[ref]
        print(ref,"found :)")
        return env
    except KeyError:
        print(ref,"not found :(")
    return None

def CheckCredential(ref):
    for r in [ref,ref.upper(),ref.lower()]:
        cred=CheckEnv(r)
        if cred!=None:
            return cred
    return None

def SetClient():
    ac1=CheckCredential("ac1")
    ac2=CheckCredential("ac2")
    if ac1==None:
        print("missing ac1 credential")
    if ac2==None:
        print("missing ac2 credential")
    if ac1==None or ac2==None:
        print("Please check credentials")
        sys.exit(1)

    print("Set up user:")
    user = itkdb.core.User(access_code1=ac1, access_code2=ac2)
    user.authenticate()
    myClient = itkdb.Client(user=user)
    print("\n###\n"+user.name+" your token expires in "+str(myClient.user.expires_in)+" seconds\n###")

    return myClient
