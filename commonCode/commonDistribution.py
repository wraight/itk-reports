### get itkdb for PDB interaction
import os
import sys
import copy
# !{sys.executable} -m pip install pandas==1.3.4
import pandas as pd
import altair as alt
import json
import numpy as np
# import copy
# !{sys.executable} -m pip install itkdb==0.4
import itkdb
import itkdb.exceptions as itkX
import datetime
###
import datapane as dp
import gitlab
import subprocess
import paramiko
from scp import SCPClient
import smtplib 
from email.mime.text import MIMEText


###############
### sharing part
###############

### Get date-time for when report is made
def GetDatetime(opt="date"):
    now = datetime.datetime.now()
    if "time" in opt.lower():
        return now.strftime("%H:%M:%S")
    elif "date" in opt.lower():
        return now.strftime("%Y-%m-%d")
    return None

### get user info. for report authorship
def MakeCreditStr(myClient):
    ### make string with user info.
    userInfo=myClient.get('getUser', json={'userIdentity': myClient.user.identity})
    credStr=userInfo['firstName']+" "+userInfo['lastName']+", e: "+userInfo['email']
    return credStr

### based on https://stackoverflow.com/questions/56921192/how-to-place-a-created-file-in-an-existed-gitlab-repository-through-python
### - but use project.commits instead of project.files
### - for commit actions (create, update, move, delete, chmod) see: https://docs.gitlab.com/ee/api/commits.html
def ScriptCommit(gitlab_url, project_id, repo_path, private_token, branch, infile_path):

    ### gitlab client
    gl = gitlab.Gitlab(gitlab_url, private_token=private_token)

    try:
        # get the project by the project id
        project = gl.projects.get(project_id)
    except gitlab.exceptions.GitlabGetError as get_error:
        # project does not exists
        print(f"could not find no project with id {project_id}: {get_error}")
        return False
    
    ### build json
    file_data = {
                'branch': 'master',
                'commit_message': repo_path.split('/')[-1]+" commit: "+datetime.datetime.now().strftime("%H:%M, %d %B %Y"),
                'actions': [
                    {
                        'action': 'create',
                        'file_path': repo_path,
#                         'content': "the new content: "+datetime.date.today().strftime("%H:%M, %d %B %Y")
                        'content': open(infile_path,'r').read()
                    }
                ]
            }
    
    ### commit
    try:
        print(f"Committing: {file_data['commit_message']}")
        file_stats = os.stat(infile_path)
        print(f'File Size in MegaBytes is {file_stats.st_size / (1024 * 1024)}')
        resp = project.commits.create(file_data)
    except gitlab.exceptions.GitlabCreateError as create_error:
        print(f"Cannot create: {create_error} (may already exist).\nTry update instead...")
        file_data['actions'][0]['action']="update"
        try:
            resp = project.commits.create(file_data)
        except gitlab.exceptions.GitlabCreateError as update_error:
            print(f"Cannot update: {update_error}. Exiting ")
            return False

    return True


def AltCopy(usr, pswd, eos_path, infile_path):
    print(f"Use file: {infile_path}")
    print("Run ssh & scp clients:")
    
    host = "lxplus.cern.ch"

    client = paramiko.client.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(host, username=usr, password=pswd)

    retVal=False
    with SCPClient(client.get_transport()) as scp:
        scp.put(infile_path, eos_path) # Copy my_file.txt to the server
        retVal=True
        print("uploaded to:",eos_path)

    return retVal


def RsyncCopy(usr, pswd, eos_path, infile_path):
    print(f"Use file: {infile_path}")
    print("Run rsync as _subprocess_:")

    ### build command
    cmdArr=['/usr/bin/rsync','-progress','-avzcP']
    cmdArr.append(f"--rsh='/usr/bin/sshpass -p \"{pswd}\" ssh -o StrictHostKeyChecking=no -l {usr}'")
    # cmdArr.extend(["--rsh='/opt/local/bin/sshpass","-p",f'"{pswd}"',"ssh","-o","StrictHostKeyChecking=no","-l",f"{usr}'"])
    cmdArr.append(infile_path)
    cmdArr.append(f"{usr}@lxplus.cern.ch:{eos_path}")

    print(f"Running subprocess:\n> {' '.join(cmdArr)}")
    session = subprocess.Popen(cmdArr, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = session.communicate()

    if stdout:
        print("subprocess success!")
        print(stdout.decode("utf-8"))
        return True

    if stderr:
        print("Error "+stderr.decode("utf-8"))

    return False


def ScpCopy(usr, pswd, eos_path, infile_path):
    print(f"Use file: {infile_path}")
    print("Run scp as _subprocess_:")

    ### build command
    cmdArr=["/usr/bin/sshpass","-p",f"'{pswd}'"]
    cmdArr.extend(["/usr/bin/scp",infile_path,f"{usr}@lxplus.cern.ch:{eos_path}"])

    print(f"Running subprocess:\n> {' '.join(cmdArr)}")
    session = subprocess.Popen(cmdArr, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    sts = os.waitpid(session.pid, 0)
    stdout, stderr = session.communicate()

    print("STDOUT:\n",stdout)
    print("STDERR:\n",stderr)

    if stdout:
        print("subprocess success!")
        print(stdout.decode("utf-8"))
        return True

    if stderr:
        print("Error "+stderr.decode("utf-8"))

    return False

### drop exrate data to save space
def TrimPlotData(plotObj):

    ### get encoding info.
    encObjList=[]
    ### encoding on top level of simple plot
    if "encoding" in plotObj.__dict__.keys():
        print(" - found encoding on top level")
        encObjList=[plotObj['encoding']]
    # maybe deeper level for complex plots
    else:
        print(" - no encoding found on top level. Digging...")
        ### digging through chart elements
        for d in plotObj.__dict__['_kwds'].keys():
            if d=="encoding":
                print("  - found {d} key")
                encObjList.append(plotObj.__dict__['_kwds'][d])
            if type(plotObj.__dict__['_kwds'][d])==type([]):
                for l in plotObj.__dict__['_kwds'][d]:
                    if "_kwds" not in l.__dict__.keys():
                        continue
                    print(l.__dict__['_kwds'].keys())
                    if "encoding" in l.__dict__['_kwds'].keys():
                        print(f"  - found encoding in {d} list")
                        encObjList.append(l.__dict__['_kwds']['encoding'])
    
    if len(encObjList)<1:
        print("No encoding information found. return original")
        return plotObj
    print(f" - found {len(encObjList)} encodings")

    ### define and populate new encoding dictionary
    print(f"Make list of encoding channels...")
    encList=[]
    # check encodings are loopable
    for encObj in encObjList:
    # loop over encodings
        if "_kwds" not in encObj.__dict__.keys():
            print(f"no \"_kwds\" key in encoding\n\t{encObj.__dict__.keys()}")
            continue
        for enc in encObj.__dict__['_kwds']:
            # print(enc)
            try:
                # print(f"{enc}: {encObj[enc]}, --> {type(encObj[enc])}" )
                if str(encObj[enc])!='Undefined':
                    if str(encObj[enc]['field'])!='Undefined':
                        encStr=encObj[enc]['field'].split(':')[0].split('(')[-1].split(')')[0]
                        print(f" - appending {enc} field:",encStr)
                        encList.append(encStr)
                    else:
                        if str(encObj[enc]['shorthand'])!='Undefined':
                            encStr=encObj[enc]['shorthand'].split(':')[0].split('(')[-1].split(')')[0]
                            print(" - appending {enc} shorthand:",encStr)
                            encList.append(encStr)
                        if str(encObj[enc]['condition'])!='Undefined':
                            encStr=encObj[enc]['condition']['field']
                            print(f" - appending {enc} condition:",encStr)
                            encList.append(encStr)
            except KeyError:
                # print(f"no key {enc}")
                continue
            except TypeError:
                # print(f"strange type: {enc}")
                continue
        
    ### remove any duplicates
    encList=list(dict.fromkeys(encList))
    # print(" - list:",encList)
    print("- return trimmed data")
    print(f"  - encList:\n\t{encList}")
    # print(f"  - data columns:\n\t{plotObj['data'].columns}")
    print(f"  - missing:\n\t{np.setdiff1d(encList,plotObj['data'].columns)}")
    # keep original df columns (shorthand retrieval can include nick names)
    subList=list(set(encList) & set(plotObj['data'].columns))
    plotObj['data']=plotObj['data'][subList]
    return plotObj


### translate aggregate report to save space
def MakeAggregatePlot(plotObj):
    
    ### if not bar chart then return
    try:
        if plotObj.__dict__['_kwds']['mark']!="bar":
            print(f"  - not a bar chart: {plotObj.__dict__['_kwds']['mark']}")
            print(plotObj.__dict__)
            return plotObj
    except KeyError:
        print("  - missing _mark_ information")
        print(plotObj.__dict__.keys())
        return plotObj

    ### get encoding info.
    encObj=plotObj['encoding']

    ### define and populate new encoding dicitonary
    # print(f" - make new encoding dictionary...")
    newEncDict={'title':plotObj.__dict__['_kwds']['title']}
    # loop over encodings
    for enc in ['color','x','y']: # encObj.__dict__['_kwds']
        try:
            # print(encObj[enc])
            if str(encObj[enc]['field'])!='Undefined':
                print(" - appending field:",encObj[enc]['field'])
                newEncDict[enc]=encObj[enc]['field']
                newEncDict[enc]=newEncDict[enc]+":"+encObj[enc]['type'].title()[0]
            else:
                if str(encObj[enc]['shorthand'])!='Undefined':
                    print(" - appending shorthand:",encObj[enc]['shorthand'])
                    newEncDict[enc]=encObj[enc]['shorthand']

            # get format: VARIABLE:T , T-->type initial
        except KeyError:
            # print(f"no key {enc}")
            continue

    # print(f"  - new encoding dictionary:\n{newEncDict}")
        
    ### loop over encodings and find aggregate
    # print("- search for  aggregate...")
    df_agg=pd.DataFrame()
    for enc in ['color','x','y']: # encObj.__dict__['_kwds']
        aggField=None
        ### check if count aggregation is used
        try:
            if str(encObj[enc]['aggregate'])!='Undefined':
                print(f"- {encObj[enc]['field']} is has appropriate aggregate: {encObj[enc]['aggregate']}")
                aggField=encObj[enc]['field']
            else:
                print(f"- {encObj[enc]['field']} has undefined aggregate")
        except KeyError:
            # print(f" - {enc} is has no aggregate key")
            pass

        ### check if shorthand is used 
        if aggField==None:
            try:
                if str(encObj[enc]['shorthand'])!='Undefined':
                    ### and has brackets --> aggregation
                    if "(" in str(encObj[enc]['shorthand']) and ")" in str(encObj[enc]['shorthand']):
                        print(f" - using shorthand: {encObj[enc]['shorthand']}")
                        aggField=encObj[enc]['shorthand'].split(':')[0].split('(')[-1].split(')')[0]
                    else:
                        print(f" - shorthand not aggregate: {encObj[enc]['shorthand']}")
                else:
                    print(f"- shorthand is undefined")
            except KeyError:
                # print(f" - {enc} is has no shorthand key")
                pass

        ### if aggragation is found
        if aggField!=None:
            print(f"- updating aggragate: {aggField}")

            if df_agg.empty:
                df_plot=plotObj['data']
            else:
                df_plot=df_agg
            print(f" - length: {len(df_plot.index)}")
            # display(df_plot)

            ### check aggField in df columns (shorthand may have nickname?)
            if aggField not in df_plot.columns:
                print(f" - aggField: {aggField} missing from df columns. return original")
                return plotObj

            # group to make count aggreagation
            df_agg=df_plot.groupby(by=f'{aggField}').agg(agg=(f'{aggField}','count')).rename(columns={'agg':aggField+"_agg"})
            # concat aggragation to first of each group
            df_agg=pd.concat([df_agg[aggField+"_agg"],df_plot.groupby(by=f'{aggField}').first()], axis=1).reset_index()

            ### update new encoding dict to use aggragate
            newEncDict[enc]=aggField+"_agg:Q"

            # display(df_agg)
            # print("Column names:",df_agg.columns)
    

    ### if no aggregated data then return original
    if df_agg.empty:
        print(f"- return old plot")
        return plotObj
    ### make new aggragated plot
    print(f"- make new plot")
    newPlot=alt.Chart(df_agg).mark_bar().encode(
                    x=alt.X(newEncDict['x']),
                    y=alt.Y(newEncDict['y']),
                    color=alt.Color(newEncDict['color']),
                    tooltip=[newEncDict['x'],newEncDict['y'],newEncDict['color']]
                ).properties(
                    width=600,
                    title=newEncDict['title']+" (AGGREGATED)"
                ).interactive()
    ### return new plot
    return newPlot


def DataPaneChunk(myClient, standardPlots, customPlots, repSpec, fullSpec):
    ### datetime & credit
    dateStr=GetDatetime("date")
    timeStr=GetDatetime("time")
    credStr=MakeCreditStr(myClient)
    print(f"temporal location: {timeStr} @ {dateStr}")

    ### object map to cast types
    dpMap={'plot':dp.Plot,'line':dp.Plot,'timeline':dp.Plot,'hist':dp.Plot,'df':dp.DataTable,'tab':dp.Table,'text':dp.Text}

    ### datapane pagination
    pages=[]
    # front page
    madeStr="### Made on: "+dateStr+" @ "+timeStr
    madeStr+=", by "+credStr
    quoteTxt="esse est percipi"
    if "quote" in repSpec.keys():
        quoteTxt=repSpec['quote']
    pages.append(dp.Page(title="Notes", blocks=[
        dp.Text("# "+repSpec['reportName']),
        dp.Text(madeStr),
        dp.Text(" --- "),
        dp.Text("## Brought to you by ITk-reports "),
        dp.Text(f"> {quoteTxt}"),
        dp.Text("Check *Broom Cupboard* tab for report json"),
        dp.Text("### git repo: [itk-reports](https://gitlab.cern.ch/wraight/itk-reports)"),
        dp.Text("## Tot ziens! ")
            ]))
    
    # data pages
    print("### Standard Plots")
    for sp in standardPlots:
        print("packing:",sp['name'])
        # print(up)
        blockList=[]
        for dl in sp['dictList']:
            print("in dictlist")
            for k,v in dl.items():
                print(f"working on {k}")
                if "df" in k.lower():
                    if v.empty:
                        print(" - skipping empty dataframe")
                        continue
                    if "dropDFs" in repSpec.keys() and repSpec['dropDFs']==True:
                        print(f" - skipping dataframe, repSpec['dropDFs']={repSpec['dropDFs']}")
                        continue
                    print(f"- df rows: {len(v.index)}")
                else:
                    if v==None:
                        print("\t this is None. skipping")
                        continue
                    if "plot" in k.lower() or "line" in k.lower():
                        plotObj=v
                        ### trimming (for all charts)
                        if "trim" in repSpec.keys() and repSpec['trim']==True:
                            print(" - trimming plot data")
                            plotObj=TrimPlotData(plotObj)
                        ### aggregation for plots
                        if "plot" in k.lower():
                            if "aggregate" in repSpec.keys() and repSpec['aggregate']==True:
                                ### transform plot and add to blocklist 
                                print(" - aggregating plot data")
                                # aggPlot=MakeAggregatePlot(v)
                                plotObj=MakeAggregatePlot(plotObj)
                        blockList.append(dpMap[k](plotObj))
                        continue
                    else:
                        if "text" in k.lower() and len(v)<1:
                            v="[empty string]"
                print(f"\t uploading: {dpMap[k]}") #"({v})")
                # print(f"\t {str(type(dpMap[k])}")
                try:
                    blockList.append(dpMap[k](v))
                except TypeError:
                    print("Type Error for ("+k+"):",dpMap[k])
        if len(blockList)>0:
            pages.append(dp.Page(title=sp['name'], blocks=blockList))
        else:
            print(f"Ignoring empty blocklist for {sp['name']}")

    # custom plots (if available)
    print("### Custom Plots")
    if len(customPlots)>0:
        pages.append(dp.Page(title="Custom Plots", blocks=[
        dp.Text("## Custom Plots"),
        *[dpMap[k](v) for cp in customPlots for k,v in cp.items() if k in dpMap.keys()]
            ]))
    
    # report specifications
    print("### Report Spec")
    ### remove sensitive stuff
    theSpec=copy.deepcopy(fullSpec)
    for k in ['user','password','path','datapaneCode','token','id','alert']:
        for dist in theSpec['distribution']:
            if k in dist.keys():
                dist[k]="CENSORED"
    ### write specs page
    pages.append(dp.Page(title="Broom Cupboard", blocks=[
        dp.Text(f"### This report: {repSpec['alias']}"),
        dp.Code(code=str(json.dumps(theSpec, indent=4)), language="python")
    # dp.Text("## Input Specifications"),
    # *[str(k)+": "+str(v) for k,v in repSpec.items()]
        ]))


    repInfo=repSpec
    ### saving report

    # remote flag - check
    remoteSave=False
    if "remote" in repInfo['location'].lower():
        print("Uploading remotely.")
        ### datapane
        if "datapane".lower() in repInfo['location']:
            print("- datapane host")
            if "code" not in repInfo.keys() or repInfo['code']==None:
                print("### Cannot upload report. Please enter datapane code to generate URL. Skip remote save.")
            else:
                print("- Got datapane info.")
                dp.login(token=repInfo['code'])
                dp.upload_report(pages, name=repInfo['reportName'], publicly_visible=True)
                remoteSave=True
        if "gitlab".lower() in repInfo['location']:
            print("- gitlab host")
            ### gitLab
            if "id" not in repInfo.keys() or repInfo['id']==None or "token" not in repInfo.keys() or repInfo['token']==None :
                print("Cannot upload report. Please enter git ID & token to generate URL. Skip remote save.")
            else:
                print("- Got gitRepo info.")
                dp.save_report(pages, path="./temp_report.html")
                if ScriptCommit("https://gitlab.cern.ch", repInfo['id'], "docs/"+repInfo['reportName']+".html", repInfo['token'], "master", "./temp_report.html"):
                    print("Sent to gitlab successfully")
                    remoteSave=True
                else:
                    print("Something went awry")
        if "eos".lower() in repInfo['location']:
            print("- eos host")
            if "user" not in repInfo.keys() or repInfo['user']==None or "password" not in repInfo.keys() or repInfo['password']==None or "path" not in repInfo.keys() or repInfo['path']==None:
                print("### Cannot upload report. Please enter eos user, password and path to generate URL. Skip remote save.")
            else:
                print("- Got eos info.")
                dp.save_report(pages, path="./temp_report.html")
                if repInfo['path'][-1]==".":
                    repInfo['path']=repInfo['path'][0:-1]
                if repInfo['path'][-1]!="/":
                    repInfo['path']=repInfo['path']+"/"
                if AltCopy(repInfo['user'], repInfo['password'], repInfo['path']+repInfo['reportName']+".html", "./temp_report.html"):
                    print("Sent to eos successfully")
                    remoteSave=True
                else:
                    print("Something went awry")
        if remoteSave==False:
            print("### No remote save successful. Save locally...")
        else:
            print("### Remote saving successful :)")


    ### save local report
    if "local" in repInfo['location'].lower() or remoteSave==False:
        print("Saving locally.")
        if "reportDir" not in repInfo.keys() or repInfo['reportDir']==None:
            print("- No reportDir specified. Saving locally.")
            if "reportName" not in repInfo.keys() or repInfo['reportName']==None:
                print(f"  - No reportName specified. Saving: {madeStr.replace(' ','_')}")
                dp.save_report(pages, path="report_"+madeStr.replace(' ','_')+".html")
            else:
                dp.save_report(pages, path=repInfo['reportName']+".html")
        else:
            dp.save_report(pages, path=repInfo['reportDir']+"/"+repInfo['reportName']+".html")
    
###############
### alerting part
###############
def SendEmail(eDict): #subject, body, sender, recipients, password):
    msg = MIMEText(eDict['body'])
    msg['Subject'] = eDict['subject']
    msg['From'] = eDict['from']
    msg['To'] = eDict['to']
    with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp_server:
       smtp_server.login(eDict['from'], eDict['pwd'])
       smtp_server.sendmail(eDict['from'], eDict['to'], msg.as_string())
    print("Message sent!")
