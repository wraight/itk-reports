
import numpy as np
# import copy
# !{sys.executable} -m pip install itkdb==0.4
import itkdb
import itkdb.exceptions as itkX
import datetime
import requests


###################
### PDB population parts
###################

### use project code to get list of institute codes in cluster
### if no code supplied a list of project codes is returned
def GetProjectInstitutes(myClient, projCode=None):
    # get list of projects
    # if no project code input then return list of codes
    print("Get Project List")
    listFail=False
    for z in range(0,10,1):
        try:
            instList=myClient.get('listInstitutions', json={})
            break
        except requests.exceptions.ConnectionError as e:
            print(f"ERROR\n{e}")
            print(f"CONNECTION ISSUE - TRY AGAIN {z}")
        if z==9:
            listFail=True
    if listFail:
        print("- list retrieval has failed!")

    if projCode==None:
        print("No project code given. Returning all institutions")
        return [x['code'] for x in instList.data if type(x)==type({}) and "code" in x.keys()]
    # check institutions on project from list of components per institution
    myInstCodes=[x['code'] for x in instList.data if type(x)==type({}) and "code" in x.keys() and "componentTypes" in x.keys() and type(x['componentTypes'])==type([]) and projCode in list(set(y['code'] for y in x['componentTypes'] if type(y)==type({}) and "code" in y.keys())) ] 
    print(f"found {len(myInstCodes)} institutes in project")
    return myInstCodes


### use cluster code to get list of institute codes in cluster
### if no code supplied a list of cluster codes is returned
def GetClusterInstitutes(myClient, clusCode=None):
    # get list of clusters    
    print("Get Cluster List")
    listFail=False
    for z in range(0,10,1):
        try:
            clusList=myClient.get('listClusters', json={})
            break
        except requests.exceptions.ConnectionError as e:
            print(f"ERROR\n{e}")
            print(f"CONNECTION ISSUE - TRY AGAIN {z}")
        if z==9:
            listFail=True
    if listFail:
        print("- list retrieval has failed!")
    # if no cluster code input then return list of codes
    if clusCode==None:
        print("No cluster code given. Listing:")
        print([x['code'] for x in clusList])
        return clusList
    # check cluster code list for input code
    foundItem=next((item for item in clusList if item['code'] == clusCode), None)
    # return None if no matching code found
    if foundItem==None:
        print("No cluster code found. Listing:")
        print([x['code'] for x in clusList])
        return foundItem
    # return institute code list of matching cluster
    myInstCodes=[x['code'] for x in foundItem['instituteList']]
    print(f"found {len(myInstCodes)} institutes in cluster")
    return myInstCodes


### Get list of components (of type in compDict) from institutes in instList
### based on matching currentLocation
def GetComponentInfo(myClient, instList, compDict, locKey="currentLocation"):
    # list for matching components
    foundComps=[]
    # check input is a list (if not make it one)
    if type(instList)!=type([]):
        print("casting input as list")
        myInstCodes=[instList]
    else:
        myInstCodes=instList
    # loop through codes in list
    for inst in myInstCodes:
        print(f"listing components at {inst}")
        # get components with institute as currentLocation
        listFail=False
        for z in range(0,10,1):
            try:
                compList=myClient.get('listComponents', json={'filterMap':{'componentType':compDict['compTypeCode'], 'project':compDict['projCode'], locKey:inst, 'state':"ready"} } )
                break
            except requests.exceptions.ConnectionError as e:
                print(f"ERROR\n{e}")
                print(f"CONNECTION ISSUE - TRY AGAIN {z}")
            if z==9:
                listFail=True
        if listFail:
            print("- list retrieval has failed! Skipping")
            continue
        # add exception for case where code is missing
        try:
            compCodes=[x['code'] for x in compList.data]
        except AttributeError:
            compCodes=[x['code'] for x in compList]
        except KeyError:
            print("no code key found. skipping")
            continue
        # add to list of matching components
        foundComps.extend([{'code':c} for c in compCodes])
        print(f"found components: {len(foundComps)}")
    print(f"found components: {len(foundComps)}")
    return foundComps


### Get child component information based on parent codes and compDict
### chunk used to limit size of request to database and avoid timeout errors
def GetChildInfo(myClient, parentCodes, compDict, chunk=100):
    # list to keep components
    childComps=[]
    # get parent components info. in chunks
    foundList=[]
    for x in range(0,int(np.ceil(len(parentCodes)/chunk))):
        print(f"component loop {x}: [{x*chunk}:{(x+1)*chunk}]")
        bulkFail=False
        for z in range(0,10,1):
            try:
                foundChunk=myClient.get('getComponentBulk', json={'component': parentCodes[x*chunk:(x+1)*chunk] })
                break
            except itkdb.exceptions.ServerError as e:
                print(f"ERROR\n{e}")
                print(f"SERVER ISSUE - TRY AGAIN {z}")
            except requests.exceptions.ConnectionError as e:
                print(f"ERROR\n{e}")
                print(f"CONNECTION ISSUE - TRY AGAIN {z}")
            if z==9:
                bulkFail=True
        if bulkFail:
            print("- bulk retrieval has failed. Skipping chunk!")
            continue
        foundList.extend(foundChunk )

    # process component info.
    for e,comp in enumerate(foundList):
        print(f"working on ({e}/{len(foundList)}): {comp['code']}")
        # check children exist
        try:
            if len(comp['children'])<1:
                print("no children found")
                continue
        except KeyError:
            print("no children key found")
            continue
        # check children match input dictionary and keep codes
        try:
            myChildCode=next((item['component']['code'] for item in comp['children'] if item['componentType']['code'] == compDict['compTypeCode']), None)
        except KeyError:
            print('component code KeyError')
            myChildCode=None
        except TypeError:
            print('component code TypeError')
            myChildCode=None
        # check children match input dictionary and keep serialNumbers
        try:
            myChildSN=next((item['component']['serialNumber'] for item in comp['children'] if item['componentType']['code'] == compDict['compTypeCode']), None)
        except KeyError:
            print('component SN KeyError')
            myChildSN=None
        except TypeError:
            print('component SN TypeError')
            myChildSN=None
        # fudge to catch [nan] (or odd returns)
        if type(myChildCode)!=type("str") and type(myChildCode)!=type(None):
            print("fix type:",type(myChildCode))
            myChildCode=None
        if type(myChildSN)!=type("str") and type(myChildCode)!=type(None):
            myChildSN=None
        print("found child:",myChildCode)
        # add to list of matching components
        childComps.append({'childCode':myChildCode,'parentCode':comp['code'],'childSN':myChildSN,'parentSN':comp['serialNumber'], 'curLoc':comp['currentLocation']['code']})
    return childComps

### use inDict to direct digging through tarDict + compare value at end
def MagicDigger(filtDict,tarDict):
    ### loop through inDict keys
    for k in filtDict.keys():
        # if dictionary do recursive
        if type(filtDict[k])==type({}):
#             print(f"Digging> {k}")
            try:
                # keep filtDict and tarDict synchronised
                return MagicDigger(filtDict[k],tarDict[k])
            except KeyError:
#                 print(f"issue with {k} (key)")
#                 print(f"{tarDict[k]}")
                return "issue with key: "+k
            except TypeError:
#                 print(f"issue with {k} (type)")
#                 print(f"{tarDict[k]}")
                return "issue with type: "+str(type(tarDict[k]))
        # if not dictionary return based on inDict[k]-tarDict[k] match
        else:
            try:
#                 print(f"Got! {k}: {tarDict[k]} (cf. {filtDict[k]})")
                if type(filtDict[k])==type("str"):
                    if "==" in filtDict[k]:
                        if tarDict[k]==filtDict[k].split('==')[1]:
                            return "pass"
                        else: 
                            return tarDict[k]
                    else:
                        if tarDict[k].__contains__(filtDict[k]):
                            return "pass"
                        else: 
                            return tarDict[k]
                else:
                    if tarDict[k]==filtDict[k]:
                        return "pass"
                    else: 
                        return tarDict[k]
            except KeyError:
#                 print(f"issue with {k}")
#                 print(f"{tarDict[k]}")
                pass
            except TypeError:
                pass
            except AttributeError:
                return None
            
### check spec function
def CheckWithSpec(inThing,specList):
    retList=[]
    ### if input list, loop over elements to find match
    if type(inThing)==type([]):
        # print("it's a list")
        ### thingPass is OR of all items in list: default False, True if any pass
        thingPass=False
        for thing in inThing:
            # print(thing)
            ### listPass is OR of all spec in list: default False, True if any pass
            listPass=False
            for specDict in specList:
#                 print(specDict)
                ### itemPass is AND of all spec in dict: default True, False if any not pass
                itemPass=True
                for k,v in specDict.items():
                    retVal=MagicDigger({k:v},thing)
                    retList.append(retVal)
                    if str(retVal).lower()!="pass":
                        itemPass=False
#                         print(f"returned: {retVal}")
                        break
                if itemPass:
                    listPass=True
            if listPass:
                thingPass=True
#                         else:
#                             print(retVal)
        if thingPass:
            return str(thingPass)
        else:
            return ("__").join([str(r) for r in retList]) #thingPass

    ### if input dictionary, find match
    elif type(inThing)==type({}):
        # print("it's a dict")
#         print(inThing)
        ### listPass is OR of all spec in list: default False, True if any pass
        listPass=False
        for specDict in specList:
#             print(specDict)
            ### itemPass is AND of all spec in dict: default True, False if any pass
            itemPass=True
            for k,v in specDict.items():
                retVal=MagicDigger({k:v},inThing)
                retList.append(retVal)
                if str(retVal).lower()!="pass":
                    itemPass=False
#                     print(f"returned: {retVal}")
                    break
            if itemPass:
                listPass=True
        if listPass:
            return str(listPass)
        else:
            return ("__").join([str(r) for r in retList]) #listPass
    else:
        print(f"Don't know what to do with {type(inThing)}")
        
        return None
    
