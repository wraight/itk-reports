# !{sys.executable} -m pip install pandas==1.3.4
import pandas as pd
import altair as alt
import numpy as np
import operator
import copy
import math
# import copy
# !{sys.executable} -m pip install itkdb==0.4
import itkdb
import itkdb.exceptions as itkX
###
import datapane as dp
from commonPopulation import CheckWithSpec
# import shared data vis functions
import dataVisFunctions


###############
### dataframe style part
###############

alt.data_transformers.enable('default', max_rows=1000000)

# format summary parts
# operator dictionary
ops = {
    "=": operator.eq,
    ">": operator.gt,
    "<": operator.lt
}
# casting dictionary
matchMap={'int':int,'float':float,'list':list,'dict':dict,'str':str,'bool':bool}

def highlight_op(val, compVal, compOp, color_if_true, color_if_false):
    global ops
    global matchMap
    op_func = ops[compOp]
    try:
        mVal=matchMap[ type(compVal).__name__ ](val)
        color = color_if_true if op_func(mVal,compVal) else color_if_false
    except ValueError:
        color = color_if_false
    return 'background-color: {}'.format(color)

def highlight_window(val, compValLo, compValHi, color_if_true, color_if_false):
    try:
        color = color_if_true if float(val)>compValLo and float(val)<compValHi else color_if_false
    except ValueError:
        color = color_if_false
    return 'background-color: {}'.format(color)

def highlight_match(val, matchVal, color_if_true, color_if_false):
    try:
        color = color_if_true if int(val) == int(matchVal) else color_if_false
    except ValueError:
        color = color_if_false
    return 'background-color: {}'.format(color)

def highlight_nan(val, color_nan):
    try:
        if pd.isna(val):
            return 'background-color: %s ; color: %s' % (color_nan,color_nan)
        else:
            return 'background-color: {}'.format('')
    except ValueError:
        return 'background-color: %s ; color: %s' % (color_nan,color_nan)

###############
### matching relatives part
###############

def AlignLengths(arr1,arr2):
    try:
        if len(arr1)>len(arr2):
            arr1=arr1[0:len(arr2)]
            print("array length adjusted")
        else:
            arr1
            # print("array length ok")
    except TypeError:
        print("TypeError arr1:",arr1)
        print("TypeError arr2:",arr2)
    except ValueError:
        print("ValueError arr1:",arr1)
        print("ValueError arr2:",arr2)
    return arr1

### match relatives based on codes
def MatchRelatives(row,df):
    # try to find parent
    df['parents']=df['parents'].astype(str)
    try:
        childStr=df[df['parents'].str.join('').str.contains(row['compCode'])]['compCode'].unique()[0]
        return row['compCode']+"_"+childStr
    except IndexError:
        pass
    # try to find child
    df['children']=df['children'].astype(str)
    try:
        parStr=df[df['children'].str.join('').str.contains(row['compCode'])]['compCode'].unique()[0]
        return parStr+"_"+row['compCode']
    except IndexError:
        pass
    return None

### Get connection of parent to child
def GetRelativesMap(myClient, df_pc, matchCol):
    print("trying to connect relatives")
    # get components from codes
    allComps=myClient.get('getComponentBulk', json={'component':list(df_pc['compCode'].unique()) })
    # print(json.dumps(allComps[0], indent=4))
    # slim this and use only codes
    for pc in ['parents','children']:
        df_pc[pc]=df_pc['compCode'].apply(lambda x: [comp[pc] for comp in allComps if comp['code']==x][0])
        df_pc[pc]=df_pc[pc].apply(lambda x: [y['component']['code'] if type(y['component'])==type({}) and "code" in y['component'].keys() else None for y in x] if type(x)==type([]) else x)
    df_pc[matchCol+'_code']=df_pc.apply(lambda row: MatchRelatives(row, df_pc), axis=1)
    df_pc[matchCol]=df_pc[matchCol+'_code'].apply(lambda x: GetpcSN(x,allComps) if x!=None else x)
    return df_pc

### convert parent['code']-child['code'] --> parent['serialNumber']-child['serialNumber'] 
def GetpcSN(pc,allComps):
    parSN=next((s['serialNumber'] for s in allComps if s['code']==pc.split('_')[0]), "None")
    chiSN=next((s['serialNumber'] for s in allComps if s['code']==pc.split('_')[1]), "None")
    return parSN+"_"+chiSN

### get related paraCode to dictionary
def GetRelatedPC(paraCode, paraCodeList, exact=False):
    for pc in paraCodeList:
        # if not looking for exact match then loop through
        if exact==False:
            # if can't find related code then skip
            if paraCode not in pc:
                continue
            else: 
                return pc
        # if exact match sought
        else:
            if paraCode==pc:
                return pc                
    return None


###############
### summaries
###############

def GetStageOrder(projCode, compTypeCode):
    ### get compType schema
    compTypeInfo=myClient.get('getComponentTypeByCode', json={'project':projCode,'code':compTypeCode})
    stageOrderList = [x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]

    return stageOrderList

def SpecialString(row,c,retC):
    if row[c]==True or row[c]=="True":
        try:
            return row[retC]+","+c
        except TypeError:
            return c
    else:
        return row[retC]


### components

def OverviewSummary(myClient, df_compInfo, nameStr=None):

    uploads=[]
    df_sum=df_compInfo.copy(deep=True)
    # print(df_sum.columns)
    
    for compType in df_sum['componentType'].unique():
        if nameStr==None:
            uploads.append({'dictList':[], 'name':compType+" Overview DI Summary"})
        else:
            uploads.append({'dictList':[], 'name':nameStr+" Overview DI Summary"})

        print(f"look for {compType}")
        df_compType=df_sum.query('componentType=="'+compType+'"')
        if df_compType.empty:
            print(f"But I don't see any: {compType}")
            continue
    
        projCode=df_compType['project'].unique()[0]
        ### get compType schema
        compTypeInfo=myClient.get('getComponentTypeByCode', json={'project':projCode,'code':compType})

        locChart=HorizontalBarPlot(df_compType, 'type', 'currentLocation', 'type', "descending", True)
        uploads[-1]['dictList'].append({'text':"### Current Locations", 'plot':locChart})

        ### and stage order
        if "stages" in compTypeInfo.keys():
            stageOrderList=[x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]

            curChart=HorizontalBarPlot(df_compType, 'type', 'currentStage', 'type', stageOrderList, True)
            uploads[-1]['dictList'].append({'text':"### Current Stage", 'plot':curChart})
            
        uploads[-1]['dictList'].append({'text':"### Summary Info.", 'df':df_compType})

    # ### make report page per type
    # for ct in df_sum['type'].unique():
    #     uploads.append({'dictList':[], 'name':ct})
    return uploads

def ComponentSummary(myClient, df_compInfo, nameStr=None):

    uploads=[]
    df_sum=df_compInfo.copy(deep=True)
    
    for compType in df_sum['componentType'].unique():
        if nameStr==None:
            uploads.append({'dictList':[], 'name':compType+" Comp DI Summary"})
        else:
            uploads.append({'dictList':[], 'name':nameStr+" Comp DI Summary"})

        print(f"look for {compType}")
        df_compType=df_sum.query('componentType=="'+compType+'"')
        if df_compType.empty:
            print(f"But I don't see any: {compType}")
            continue
    
        projCode=df_compType['project'].unique()[0]
        ### get compType schema
        compTypeInfo=myClient.get('getComponentTypeByCode', json={'project':projCode,'code':compType})
        ### and stage order
        stageOrderList=None
        if "stages" in compTypeInfo.keys():
            stageOrderList=[x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]
        
        ### loop over types - so no point using color to encode types (for there is only one!)
        for a in df_compType["type"].unique():

            locChart=HorizontalBarPlot(df_compType[df_compType["type"]==a], 'type', 'currentLocation', 'currentLocation', "descending", True)
            uploads[-1]['dictList'].append({'text':"### Current Locations"})
            uploads[-1]['dictList'].append({'text':str(a), 'plot':locChart})

            if stageOrderList!=None:
                curChart=HorizontalBarPlot(df_compType[df_compType["type"]==a], 'currentLocation', 'currentStage', 'currentLocation', stageOrderList, True)
                uploads[-1]['dictList'].append({'text':"### Current Stage"})
                uploads[-1]['dictList'].append({'text':str(a), 'plot':curChart})

            uploads[-1]['dictList'].append({'text':"### Summary Info."})
            uploads[-1]['dictList'].append({'text':str(a), 'df':df_compType})

    # ### make report page per type
    # for ct in df_sum['type'].unique():
    #     uploads.append({'dictList':[], 'name':ct})
    return uploads

### stages

### Get stage information
def StageSummary(myClient, df_stageInfo):

    repPage=[]
    df_sum=df_stageInfo.copy(deep=True)
    # print(df_sum.columns)
    
    for compType in df_sum['componentType'].unique():
        repPage.append({'dictList':[], 'name':str(compType)+" Stage DI Summary"})

        df_compType=df_sum.query('componentType=="'+compType+'"')
    
        projCode=df_compType['project'].unique()[0]
        ### get compType schema
        compTypeInfo=myClient.get('getComponentTypeByCode', json={'project':projCode,'code':compType})
        ### and stage order
        try:
            stageOrderList=[x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]
        except KeyError:
            stageOrderList="descending"

        df_scl_hoz=pd.DataFrame()
        df_scl_vert=pd.DataFrame()

        # ### try to pick useful identifier (use code if nothing else)
        # idCol="serialNumber"
        # for id in ['serialNumber','code']: #,'alternativeIdentifier'
        #     if id in df_compType.columns and len(df_compType[id].unique())!=1:
        #         idCol=id
        #         break
        idCol="code" # stage plots don't use identifier, only count total objects
        print(f"using identifier: {idCol}")

        # display(df_compType)
        ### stage check list
        for e,sn in enumerate(df_compType[idCol].unique()):
            try:
                serNum=df_compType.query(f'{idCol}=="{sn}"')[idCol].values[0]
            except IndexError:
                print(f"Can't work with {sn}")
                # serNum=None
                continue
            print("working on:",serNum)
            if "stages" not in compTypeInfo.keys():
                print(" - skipping: no stage data")
                continue
            df_scl=pd.DataFrame(compTypeInfo['stages'])[['code','name','order','final','initial']]
            df_scl['special']=None
            for c in ['final','initial']:
                df_scl['special']=df_scl.apply(lambda row: SpecialString(row,c,"special"), axis=1)
            df_scl=df_scl.drop(columns=['initial','final']).reset_index(drop=True)
            df_scl['compCheck']="False"
            df_scl['compCheck']=df_scl.apply(lambda row: "True" if row['code'] in df_compType.query(f'code=="{sn}"')['stage_code'].to_list() else "False", axis=1)
            # note current stage
            # rowInd=df_scl.index[df_scl['code'] == compInfo[e]['currentStage']['code']].tolist()[0]
            # df_scl.at[rowInd,'compCheck']="current"
            df_scl['date']=None
            df_scl['date']=df_scl.apply(lambda row: df_compType.query(f'code=="{sn}" | stage_code=="{row["code"]}"')['stage_dateTime'].values[0] if row['code'] in df_compType.query(f'code=="{sn}"')['stage_code'].to_list() else None, axis=1)
        #     display(df_scl)
            df_scl['ident']=serNum # can't call it code! - confusion with stage code
            df_scl['type']=df_compType.query(f'{idCol}=="{sn}"')['type'].values[0]
            if serNum==None:
                serNum="None_"+str(e)
            print(f"e_{e}: {serNum}")
            if e==0:
                df_scl_hoz=df_scl.rename(columns={'compCheck':serNum+"_compCheck",'date':serNum+"_date"}).drop(columns=[idCol])
                df_scl_vert=df_scl
            else:
                df_scl_hoz=pd.concat([df_scl_hoz, df_scl[['compCheck','date']] ], axis=1).rename(columns={'compCheck':serNum+"_compCheck",'date':serNum+"_date"})
                df_scl_vert=pd.concat([df_scl_vert, df_scl], axis=0)
        
        print(df_compType[idCol].unique())
        # display(df_scl_vert)

        if df_scl_vert.empty:
            print(f"No data here: {compType}")
            continue
        df_scl_vert=df_scl_vert.query('compCheck!="False"').reset_index(drop=True)

        # another hack to avoid JSON serilsation error
        df_scl_vert['date']=df_scl_vert['date'].astype(str)

        for ct in df_scl_vert['type'].unique():
            repPage[-1]['dictList'].append({'text':"## Stages Visited"})
            df_plot=df_scl_vert.query('type=="'+str(ct)+'"')
            rect=alt.Chart(df_plot).mark_rect().encode(
                y=alt.Y('code:N', axis=None, sort=stageOrderList),
                color=alt.Color('count(ident):Q', legend=None, scale=alt.Scale(scheme=alt.SchemeParams(name='purples'))), # title="#entries"),
                tooltip=['code:N','count(ident):Q']
            )
            text=alt.Chart(df_plot).mark_text(lineBreak=r'\n').encode(
                y=alt.Y('code:N', sort=stageOrderList),
                text=alt.Text('label:N')
            ).transform_joinaggregate(
                count='count(ident):Q',
                groupby=["code"]
            ).transform_calculate(
                label=alt.datum.code + " \ncount: " + alt.datum.count
            )
            stageCombPlot=(rect+text).properties(width=1000, height=200, title=str(ct)+" Stages Visited")
            repPage[-1]['dictList'].append({'text':str(ct), 'plot':stageCombPlot})

    return repPage


def StageTimeLines(myClient, df_stageInfo):
    
    repPage=[]
    df_sum=df_stageInfo.copy(deep=True)
    # print(df_sum.columns)

    for compType in df_sum['componentType'].unique():
        repPage.append({'dictList':[], 'name':compType+" Stage Timeline"})

        df_compType=df_sum.query('componentType=="'+compType+'"')
    
        projCode=df_compType['project'].unique()[0]
        ### get compType schema
        compTypeInfo=myClient.get('getComponentTypeByCode', json={'project':projCode,'code':compType})
        ### check stage info exists
        if "stages" not in compTypeInfo.keys():
            print(f"No stages here: {compType}")
            continue
        
        ### and stage order
        try:
            stageOrderList=[x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]
        except KeyError:
            stageOrderList="descending"

        ### try to pick useful identifier (use code if nothing else)
        idCol="serialNumber"
        for id in ['serialNumber','code']: #,'alternativeIdentifier'
            if id in df_compType.columns and len(df_compType[id].unique())!=1:
                idCol=id
                break
        print(f"using identifier: {idCol}")

        df_compType.sort_values(by=[idCol,'stage_dateTime'], inplace=True)
        df_compType=df_compType.reset_index(drop=True)

        ### upload timelines per type
        for ct in df_compType['type'].unique():
            print(f"##### {ct}")
            repPage[-1]['dictList'].append({'text':"## Stage History"})
            stageChart=alt.Chart(df_compType.query('type=="'+str(ct)+'"')).mark_line(point=True).encode(
                x=alt.X('stage_dateTime:T',axis = alt.Axis(title = "Date", format = ("%b %Y"))),
            #     x=alt.X('date:N', axis = alt.Axis(title = "Date", format = ("%b %Y"))),
                y=alt.Y('stage_code:N',title="Stage",sort=stageOrderList),
                color=alt.Color(idCol+':N', legend=None),
                order='stage_dateTime',
                tooltip=['stage_dateTime:T','stage_code:N',idCol+':N']
            ).configure_point(size=60).properties(width=600, height=300, title=str(ct)+" Stage History").interactive()
            repPage[-1]['dictList'].append({'text':str(ct), 'timeline':stageChart,'df':df_compType.query(f'type=="{ct}"')})

    return repPage



### tests

def TestSummary(myClient, df_runInfo):

    repPage=[]
    df_sum=df_runInfo.copy(deep=True)
    print(df_sum.columns)

    for compType in df_sum['compTypeCode'].unique():
        repPage.append({'dictList':[], 'name':compType+" Test DI Summary"})

        df_compType=df_sum.query('compTypeCode=="'+compType+'"')
    
        projCode=df_compType['projCode'].unique()[0]
        ### get compType schema
        compTypeInfo=myClient.get('getComponentTypeByCode', json={'project':projCode,'code':compType})
        ### and stage order
        try:
            stageOrderList=[x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]
        except KeyError:
            stageOrderList="descending"
            
        ### get compType schema
        compTypeInfo=myClient.get('getComponentTypeByCode', json={'project':projCode,'code':compType})

        df_scl=pd.DataFrame(compTypeInfo['stages'])
        df_tcl_vert=None
        df_tcl_vert_comb=pd.DataFrame()
        # display(df_scl)
        for stgCode in df_scl['code'].unique():
            print("working on:",stgCode)
            df_tcl=df_scl.query('code=="'+stgCode+'"').copy(deep=True).reset_index(drop=True)
            df_tcl=df_tcl.explode('testTypes').reset_index(drop=True) #.explode('testTypes')
        #     display(df_tcl)
            df_tcl['testType']=None
            for i,row in df_tcl.iterrows():
                try:
                    print(i,row['testTypes']['testType'])
                    retVal=myClient.get('getTestType', json={'id':row['testTypes']['testType']['id']})
                    df_tcl.at[i,'testType']=retVal['code']
                except TypeError:
                    pass
        #     df_tcl=df_tcl['testType']=df_tcl['testTypes'].apply(lambda x: GetTT(x['testType']) if x!=None else x)
            for c in ['order','receptionTest','receptionTestOnly','nextStage']:
                try:
                    df_tcl[c]=df_tcl['testTypes'].apply(lambda x: x[c])
                except TypeError:
                    df_tcl[c]=None
            df_tcl['special']=None
            for c in ['alternative','final','initial']:
                df_tcl['special']=df_tcl.apply(lambda row: SpecialString(row,c,"special"), axis=1)
            df_tcl=df_tcl.drop(columns=['alternative','final','initial'])
            df_tcl['special2']=None
            for c in ['receptionTest','receptionTestOnly','nextStage']:
                df_tcl['special2']=df_tcl.apply(lambda row: SpecialString(row,c,"special2"), axis=1)
            df_tcl_hoz=df_tcl.copy(deep=True)
            df_tcl_vert=df_tcl.copy(deep=True)
            
            ### per component loop
            ### try to pick useful identifier (use code if nothing else)
            idCol="serialNumber"
            for id in ['serialNumber','alternativeIdentifier','compCode']:
                if id in df_compType.columns and len(df_compType[id].unique())!=1:
                    idCol=id
                    break
            print(f"using identifier: {idCol}")
            
            for sn in df_runInfo[idCol].unique():
                # print("\t- checking:",sn)
                df_tcl_vert[idCol]=sn
                df_tcl_vert['typeCode']=df_runInfo.query(idCol+'=="'+sn+'"')['typeCode'].values[0]
                df_tcl_vert['compCheck']=False
                df_tcl_vert['compCheck']=df_tcl_vert['testType'].apply(lambda x: True if x in df_runInfo.query(idCol+'=="'+sn+'" & stage=="'+stgCode+'"')['testType'].to_list() else False)
                for c in ['passed','date']:
                    df_tcl_vert[c]=None
                    df_tcl_vert[c]=df_tcl_vert.apply(lambda row: df_runInfo.query('stage=="'+stgCode+'" & testType=="'+row['testType']+'"')[c].values[0] if row['compCheck']==True else None, axis=1)
                try:
                    df_tcl_vert_comb=pd.concat([df_tcl_vert_comb,df_tcl_vert]).reset_index(drop=True)
                except TypeError:
                    df_tcl_vert_comb=df_tcl_vert
        df_tcl_vert_comb=df_tcl_vert_comb.drop(columns=['receptionTest','receptionTestOnly','nextStage','testTypes'])
        df_tcl_vert_comb=df_tcl_vert_comb[['testType','typeCode','order','code','special','special2']+[idCol,'compCheck','date','passed'] ].rename(columns={'code':"stageCode"})
        
        # another hack to avoid JSON serilsation error
        df_tcl_vert_comb['date']=df_tcl_vert_comb['date'].astype(str)

        ### test population bubble map with pass=True/False seperation
        for ct in df_tcl_vert_comb['typeCode'].unique():
            repPage[-1]['dictList'].append({'text':"## Populations Per Test"})
            df_plot=df_tcl_vert_comb.query(f'compCheck==True & typeCode=="{str(ct)}"')
            testOrderList=list(df_plot.sort_values(by=['order'])['testType'].unique())
            testOrderList.reverse()
            bubTrue=alt.Chart(df_plot.query('passed==True')).mark_circle(color="green", xOffset=15).encode(
                x=alt.X('stageCode:N', title=None, axis=alt.Axis(labelAngle=0), sort=stageOrderList),
                y=alt.Y('testType:N', axis=None, sort=testOrderList),
                size=alt.Size('count('+idCol+'):Q', legend=None), #title="#entries"),
                tooltip=['count('+idCol+'):Q',]
            )
            bubFalse=alt.Chart(df_plot.query('passed==False')).mark_circle(color="red", xOffset=-15).encode(
                x=alt.X('stageCode:N', sort=stageOrderList),
                y=alt.Y('testType:N', sort=testOrderList),
                size=alt.Size('count('+idCol+'):Q',),
                tooltip=['testType:N','count('+idCol+'):Q',]
            )
            rect=alt.Chart(df_plot).mark_rect(filled=False).encode(
                x=alt.X('stageCode:N', sort=stageOrderList),
                y=alt.Y('testType:N', sort=testOrderList)
            )
            text=alt.Chart(df_plot).mark_text().encode(
                x=alt.X('stageCode:N', sort=stageOrderList),
                y=alt.Y('testType:N', sort=testOrderList),
                text=alt.Text('testType:N')
            )
            testCombPlot=(bubTrue+bubFalse+rect+text).properties(width=1000, height=700, title={
                "text": ["passed tests"],
                "subtitle": ["failed tests"],
                "color": "green",
                "subtitleColor": "red",
                }).interactive()
            repPage[-1]['dictList'].append({'text':str(ct), 'plot':testCombPlot})


        ### stage-test bar map
        for ct in df_tcl_vert_comb['typeCode'].unique():
            repPage[-1]['dictList'].append({'text':"## Tests Per Stage"})
            df_plot=df_tcl_vert_comb.query(f'compCheck==True & typeCode=="{str(ct)}"')
            testOrderList=list(df_plot.sort_values(by=['order'])['testType'].unique())
            bar=alt.Chart(df_plot).mark_bar().encode(
                x=alt.X('stageCode:N', title=None, axis=alt.Axis(labelAngle=0), sort=stageOrderList),
                y=alt.Y('count(testType):Q'),
                color=alt.Color('testType:N', sort=testOrderList),
                size=alt.Size('passed:N'),
                tooltip=['count(testType):Q','testType:N']
            ).properties(width=300, title=str(ct)+" Tests Per Stage")
            # display(bar)
            repPage[-1]['dictList'].append({'text':str(ct), 'plot':bar})

    return repPage


def TestTimeLines(myClient, df_runInfo):
    
    repPage=[]
    df_sum=df_runInfo.copy(deep=True)
    # print(df_sum.columns)

    for compType in df_sum['compTypeCode'].unique():
        repPage.append({'dictList':[], 'name':compType+" Test Timeline"})

        df_compType=df_sum.query('compTypeCode=="'+compType+'"')
    
        projCode=df_compType['projCode'].unique()[0]
        ### get compType schema
        compTypeInfo=myClient.get('getComponentTypeByCode', json={'project':projCode,'code':compType})
        ### and stage order
        stageOrderList=[x['code'] for x in sorted(compTypeInfo['stages'], key=lambda d: d['order'])]

        # timeline
        for ct in df_runInfo['typeCode'].unique():
            repPage[-1]['dictList'].append({'text':"## Tests Uploads"})
            df_plot=df_runInfo.query(f'typeCode=="{str(ct)}"').reset_index(drop=True)
            ### try to pick useful identifier (use code if nothing else)
            idCol="serialNumber"
            for id in ['serialNumber','alternativeIdentifier','compCode']:
                if id in df_compType.columns and len(df_compType[id].unique())!=1:
                    idCol=id
                    break
            print(f"using identifier: {idCol}")
            testChart=alt.Chart(df_plot).mark_circle(size=60).encode(
                x=alt.X('date:T',axis = alt.Axis(title = "Date", format = ("%b %Y"))),
            #     x=alt.X('date:N', axis = alt.Axis(title = "Date", format = ("%b %Y"))),
                y=alt.Y('stage:N',title="stage",sort=stageOrderList),
                color=alt.Color('testType:N'),
                shape=alt.Shape('passed:N'),
                tooltip=[idCol+':N','date:T','stage:N','testType:N','passed:N']
                ).properties(width=600, height=300, title=str(ct)+" Test History").interactive()
            repPage[-1]['dictList'].append({'text':str(ct), 'timeline':testChart, 'df':df_runInfo.query(f'typeCode=="{ct}"')})
            # display(testChart)
        
    return repPage

###############
### standard plots
###############

HorizontalBarPlot=dataVisFunctions.HorizontalBarPlot

MakeStatDict=dataVisFunctions.MakeStatDict

TimelineAndHistogram=dataVisFunctions.TimelineAndHistogram

### standard plots - for each parameter in testType, plot timeline and data
### standard plots - for each parameter in testType, plot timeline and data
def DQPlotting_testRuns(df_testRuns):
    # use large number in case of many rows in dataframe
    alt.data_transformers.enable('default', max_rows=1000000)
    # list of report data
    uploads=[]
    df_sum=df_testRuns.copy(deep=True)

    ### skip empty
    if df_sum.empty:
        return pd.DataFrame()
    
    for compType in df_sum['compTypeCode'].unique():
        # uploads.append({'dictList':[], 'name':compType+" DI Summary"})
        # uploads[-1]['dictList'].append({'text':"These plots are _interactive_"})
                                        
        df_compType=df_sum.query('compTypeCode=="'+compType+'"')
    
        projCode=df_compType['projCode'].unique()[0]
        testCode=df_compType['testCode'].unique()[0]
        ### get testType schema
        testSchema=myClient.get('getTestTypeByCode', json={'project':projCode, 'componentType':compType, 'code':testCode})
        df_testParameters=pd.json_normalize(testSchema['parameters'],sep='_')
        df_testParameters=pd.concat([df_testParameters,pd.json_normalize(testSchema['properties'],sep='_')])

        # display(df_testParameters)

        ### try to pick useful identifier (use code if nothing else)
        idCol="serialNumber"
        for id in ['serialNumber','alternativeIdentifier','compCode']:
            if id in df_compType.columns and len(df_compType[id].unique())!=1:
                idCol=id
                break
        print(f"using identifier: {idCol}")

        # loop over parameter codes
        for i,row in df_testParameters.iterrows():
            print(f"checking {row['code']}")# ({row['valueType']})")
            # display(df_testRuns.query('paraCode=="'+row['code']+'"'))

            # only take data from relevant test parameter
            df_sub=df_sum.query('paraCode=="'+row['code']+'"')
            if df_sub.empty:
                print(" - nothing matches")
                print(df_sum['paraCode'].unique)
                continue
            
            ### check if object id dictionary (will have columns)
            if df_sub['paraValue'].transform(lambda x: x.apply(type).eq(dict)).all():
                print("dictionary found")
                df_dict=pd.json_normalize(df_sub['paraValue'])
                df_total=pd.DataFrame()
                # if dictionary, go through columns and match to "pointed" parameter (i.e. "-->PARA")
                for e,c in enumerate(df_dict.columns):
                    print("working on",c)
                    df_col=df_sub.copy(deep=True)
                    df_col['paraValue']=df_col['paraValue'].apply(lambda x: x[c] if isinstance(x, (dict)) and c in x.keys() else None )
                    df_col['paraCode']=row['code']+'->'+c
                    if e==0:
                        df_total=df_col
                    else:
                        df_total=pd.concat([df_total,df_col])
                df_sub=df_total.copy(deep=True)
            
            for pc in df_sub['paraCode'].unique():
                print("paraCode:",pc)
                print(f"- type: {str(type(pc))}")

                uploads.append({'dictList':[], 'name':pc+" DI"})
                uploads[-1]['dictList'].append({'text':"Per componentType sub-type plots. Full dataset at the end."})
                uploads[-1]['dictList'].append({'text':" - Timeline plots are _interactive_"})
                

                df_pc=df_sub.query('paraCode=="'+pc+'"')
                ### tidy nans
                if 'paraValue' in df_pc.columns:
                    df_pc = df_pc[df_pc['paraValue'].notna()]
                    df_pc = df_pc[df_pc['paraValue']!= "nan"]
                    # df_pc = df_pc.dropna(subset=df_pc.select_dtypes(np.floating).columns, how='all')
                
                if df_pc.empty:
                    print(f">>> Nothing left after tidying: {pc}")
                    continue

                ### looping over subTypes
                for st in df_pc['subType'].unique():
                    df_st=df_pc.query(f'subType=="{str(st)}"')

                    # plotting for non-lists
                    if True not in (df_st.applymap(type) == list)['paraValue'].to_list():
                        print(" - does NOT look like a list")

                        ### skip strings
                        if True not in (df_st.applymap(type) == str)['paraValue'].to_list():
                            print(f" - does NOT look like a string")
                            ### plot timeline of component test uploads
                            print(f" - so make timeline and histogram plot combination")
                            combPlot=TimelineAndHistogram(df_st, pc)
                            # add to report 
                            uploads[-1]['dictList'].append({'text':st, 'hist':combPlot})
                            continue

                    # plotting for lists or strings
                    print(" - looks like a list or string --> make timeline")

                    if True not in (df_st.applymap(type) == str)['paraValue'].to_list():
                        print(f" - does NOT look like a string")

                        # explode lists into individual points
                        df_array=df_st.explode('paraValue')
                        df_array['paraValue']=df_array['paraValue'] #.astype('float').abs() ### keep raw for the moment
                        ### plot timeline of component test uploads
                        # altair visualisation
                        timeline=alt.Chart(df_array).mark_boxplot(extent='min-max').encode(
                            x=alt.X('date:T'),
                            y=alt.Y('paraValue:Q', title=st),
                            color=alt.Color('institution:N'),
                            tooltip=['date:T','paraValue:Q','institution:N',idCol+':N']
                        ).properties(
                            width=600,
                            title=pc+" timeline"
                        ).interactive()
                        # display(timeline)

                        uploads[-1]['dictList'].append({'text':st, 'timeline':timeline})

                    else:
                        print(f" - assume string by process of elimination")

                        df_str=df_st

                        timeline=alt.Chart(df_str).mark_point().encode(
                            x=alt.X('date:T'),
                            y=alt.Y('paraValue:N', title=st),
                            color=alt.Color('institution:N'),
                            tooltip=['date:T','paraValue:N','institution:N',idCol+':N']
                        ).properties(
                            width=600,
                            title=pc+" timeline"
                        ).interactive()
                        # display(timeline)

                        print(f" - make frequecy plot")
                        # ### debugging checks
                        # for val in df_str['paraValue'].unique():
                        #     valLen=len(df_str.query(f'paraValue=="{val}"'))
                        #     print(f"\t- {val}: {valLen}")
                        
                        valueFreq=alt.Chart(df_str).mark_bar().encode(
                            y=alt.Y('paraValue:N', title=st),
                            x=alt.X('count(paraValue):Q'),
                            color=alt.Color('paraValue:N'),
                            tooltip=['paraValue:Q','count(paraValue):Q']
                        ).properties(
                            width=600,
                            title=pc+" frequency"
                        ).interactive()

                        uploads[-1]['dictList'].append({'text':st, 'timeline':timeline, 'plot':valueFreq})

                ### put data at the end
                uploads[-1]['dictList'].append({'text':"Full dataset",'df':df_pc})

    ### return report data
    return uploads

### standard plots - for each parameter in testType, plot timeline and data
def DQPlotting_testList(df_testRuns, testList):
    # use large number in case of many rows in dataframe
    alt.data_transformers.enable('default', max_rows=1000000)
    # list of report data
    uploads=[]
    
    # loop over parameter codes
    for row in testList:
        print(f"checking {row['paraCode']}")# ({row['valueType']})")
        # display(df_testRuns.query('paraCode=="'+row['code']+'"'))

        # only take data from relevant test parameter
        df_sub=df_testRuns.query('paraCode.str.contains("'+row['paraCode']+'")')
        # add report page for institute
        uploads.append({'dictList':[], 'name':row['paraCode']}) 
        uploads[-1]['dictList'].append({'text':"These plots are _interactive_"})
        
        ### check if object id dictionary (will have columns)
        if df_sub['paraValue'].transform(lambda x: x.apply(type).eq(dict)).all():
            print("dictionary found")
            df_dict=pd.json_normalize(df_sub['paraValue'])
            df_total=pd.DataFrame()
            # if dictionary, go through columns and match to "pointed" parameter (i.e. "-->PARA")
            for e,c in enumerate(df_dict.columns):
                print("working on",c)
                df_col=df_sub.copy(deep=True)
                df_col['paraValue']=df_col['paraValue'].apply(lambda x: x[c] if isinstance(x, (dict)) and c in x.keys() else None )
                df_col['paraCode']=row['code']+'->'+c
                if e==0:
                    df_total=df_col
                else:
                    df_total=pd.concat([df_total,df_col])
            df_sub=df_total.copy(deep=True)
     
        ### per component loop
        ### try to pick useful identifier (use code if nothing else)
        idCol="serialNumber"
        for id in ['serialNumber','alternativeIdentifier','compCode']:
            if id in df_sub.columns and len(df_sub[id].unique())!=1:
                idCol=id
                break
        print(f"using identifier: {idCol}")
        
        for pc in df_sub['paraCode'].unique():
            print("paraCode:",pc)
            
            # plotting for non-lists
            if True not in (df_sub.query('paraCode=="'+pc+'"').applymap(type) == list)['paraValue'].to_list():
                print("does NOT look like a list")

                ### plot timeline of component test uploads
                # altair visualisation
                timeline=alt.Chart(df_sub.query('paraCode=="'+pc+'"')).mark_point().encode(
                    x=alt.X('date:T'),
                    y=alt.Y('paraValue:Q', title=pc),
                    color=alt.Color('institution:N', title="Component Combination"),
                    tooltip=['date:T','paraValue:Q','institution:N',idCol+':N']
                ).properties(
                    width=600,
                    title=pc+" timeline"
                ).interactive()
                # display(timeline)

                ### plot histogram of parameter
                # altair visualisation
                hist=alt.Chart(df_sub.query('paraCode=="'+pc+'"')).mark_bar().encode(
                alt.X('paraValue:Q', bin=True, title=pc),
                y='count()',
                color=alt.Color('institution:N'),
                tooltip=['paraValue:Q','institution:N']
                ).properties(
                    width=600,
                    title=pc+" distribution"
                ).interactive()
                # display(hist)
                
                # add to report data
                uploads[-1]['dictList'].append({'text':pc, 'timeline':timeline,'hist':hist,'df':df_sub.query('paraCode=="'+pc+'"')})

            # plotting for lists
            else:
                print("looks like a list")
                
                # explode lists into individual points
                df_array=df_sub.query('paraCode=="'+pc+'"').explode('paraValue')
                df_array['paraValue']=df_array['paraValue'] #.astype('float').abs() ### keep raw for the moment
                ### plot timeline of component test uploads
                # altair visualisation
                timeline=alt.Chart(df_array).mark_boxplot(extent='min-max').encode(
                    x=alt.X('date:T'),
                    y=alt.Y('paraValue:Q', title=pc),
                    color=alt.Color('institution:N'),
                    tooltip=['date:T','paraValue:Q','institution:N',idCol+':N']
                ).properties(
                    width=600,
                    title=pc+" timeline"
                ).interactive()
                # display(timeline)
                
                # add to report data
                uploads[-1]['dictList'].append({'text':pc, 'timeline':timeline,'df':df_sub.query('paraCode=="'+pc+'"')})

    ### return report data
    return uploads

###############
### correlation plot part
###############

### extra plotting for correlation and difference plots
### use parentDict and childDict to define comparison to plot
def CorrelationPlot(df_testRuns_in, parentDict, childDict, matchCol):
    # use large number in case of many rows in dataframe
    alt.data_transformers.enable('default', max_rows=1000000)
    ### list of report data
    bespokes=[]
    
    # shorthand:
    df_testRuns=df_testRuns_in.copy(deep=True)
    ptt, ctt = parentDict, childDict
                
    # diff value placeholder
    df_testRuns['diff']=np.NaN
        
    # loop over connections
    for con in df_testRuns[matchCol].unique():
        if con=="None" or con==None:
            continue
        print("working on:",con)
        #parent info        
        # find related paraCode
        ppc=GetRelatedPC(ptt['paraCode'],df_testRuns['paraCode'].unique())
        # if can't find related code then skip
        if ppc==None:
            continue
        df_t1=df_testRuns.query(matchCol+'=="'+con+'" & paraCode=="'+ppc+'"')
        ### check df_t1 not returned empty
        if df_t1.empty:
            print("empty df :( Skipping")
            continue
                
        # get latest values if multiple test are found
        df_t1.sort_values(by='date', inplace=True)
        try:
            para1=df_t1.tail(1)['paraValue'].astype(float).values[0]
            # not index to update appropriate row in dataframe
            paraInd=df_t1.tail(1).index[0]
        except IndexError:
            # display(df_t1)
            print(f"no para1 data for {con}")
            continue
        
        # child info       
        # find related paraCode
        cpc=GetRelatedPC(ctt['paraCode'],df_testRuns['paraCode'].unique())
        # if can't find related code then skip
        if cpc==None:
            continue
        df_t2=df_testRuns.query(matchCol+'=="'+con+'" & paraCode=="'+cpc+'"')
        ### check df_t1 not returned empty
        if df_t2.empty:
            print("empty df :( Skipping")
            continue
            
        # get latest values if multiple test are found
        df_t2.sort_values(by='date', inplace=True)
        try:
            para2=df_t2.tail(1)['paraValue'].astype(float).values[0]
        except IndexError:
            print(f"no para2 data for {con}")
            continue

        # print("### para1:",para1)
        # print("### para2:",para2)
        # print("### diff",para1-para2)
        
        # update diff value with parent - child (using row of parent defined above index)
        df_testRuns.at[paraInd,'diff']=para1-para2
        # bespokes.append({'df':df_total, 'test':"diff calc"})

    ### difference histogram
    # drop any missing results
    df_diff=df_testRuns[~df_testRuns['diff'].isna()]
    # altair visualisation
    altDiff=alt.Chart(df_diff).mark_bar().encode(
        alt.X("diff:Q", bin=True, title="Measurement difference"),
        y='count()',
        color=alt.Color('institution:N',title="Institution"),
        tooltip=['diff:Q','institution:N']
    ).properties(
        width=600,
        title="Difference in Module-Sensor measurements"
    ).interactive()
    # add to report data
    bespokes.append({ 'text':"Diff plot", 'plot':altDiff})

    ### Correlation plot
    df_total=pd.DataFrame()
    testColumns=[ 
                {'paraCode':ppc}, #,'compType':parentDict['compTypeCode']},
                {'paraCode':cpc}  #,'compType':childDict['compTypeCode']}
            ]

    # loop over columns
    for tc in testColumns:
        print(f"working on: {tc['paraCode']}")#@{tc['compType']}")
        # begin with list of SNs & codes
        if df_total.empty:
            df_total=df_testRuns[[matchCol,'institution']].copy(deep=True)
            df_total=df_total.drop_duplicates().reset_index(drop=True)

        # get test subset
        newColName=tc['paraCode'] 
        df_paraCode=df_testRuns.query('paraCode=="'+tc['paraCode']+'"').rename(columns={"paraValue": newColName})
        #display(df_paraCode)
            
        # match test data to codes
        df_total[newColName]=np.NaN

        for i,row in df_total.iterrows():
            if row[matchCol]==None:
                continue
    #         print(con)
            try:
                df_total.at[i,newColName]=df_paraCode.query(matchCol+'=="'+row[matchCol]+'"').tail(1)[newColName].values[0]
            except IndexError:
                pass
            except ValueError:
                pass
    # drop empties and reset index
    df_total=df_total.dropna().reset_index(drop=True)
    # add to report data
    bespokes.append({'text':"diff comp", 'df':df_total})

    print(f"'{ppc}'")
    print(f"'{cpc}'")
    # altair visualisation
    altCorr=alt.Chart(df_total).mark_point().encode(
        x=alt.X(ppc.replace('[','\[').replace(']','\]')+":Q"),
        y=alt.Y(cpc.replace('[','\[').replace(']','\]')+":Q"),
        color=alt.Color("institution:N"),
        tooltip=[ppc+":Q",cpc+":Q"]
    ).properties(
        width=600,
        title="Correlation: "+ppc+"  - "+cpc
    ).interactive()
    # add to report data
    bespokes.append({'text':"Correlation", 'plot':altCorr})

    ### return report data
    return bespokes


###############
### combinations part
###############

### standard plots - for each component connection, the test upload line and data
### use bespoke definitions to plot
def MatchedCombinations(df_testRuns, comb, matchCol):
    # use large number in case of many rows in dataframe
    alt.data_transformers.enable('default', max_rows=1000000)
    # list of report data
    uploads=[]
    
    # loop over parent-child connection
    for con in df_testRuns[matchCol].unique():
        print(f"checking {con}")
        if con==None:
            continue
        df_sub=df_testRuns.query(matchCol+'=="'+con+'"')

        # use defined plot settings
        # for b in combinations:
            # print(f"combination: {comb['yCode']} vs. {comb['xCode']}")
        print(f"combination: {comb['yCode']} vs. {comb['xCode']}")
        # add report page for connection
        uploads.append({'dictList':[], 'name':con})
        
        ### concatonate x & y parameters in dataframe
        # start with x parameter
        
        # find related paraCode
        xpc=GetRelatedPC(comb['xCode'],df_testRuns['paraCode'].unique(),True)
        # if can't find related code then skip
        if xpc==None:
            continue
        
        df_x=df_sub.query('paraCode=="'+xpc+'"')

        ### check df_t1 not returned empty
        if df_x.empty:
            print("empty df :( Skipping")
            continue

        # rename x column to parameter code
        df_x=df_x.rename(columns={"paraValue": xpc}).reset_index(drop=True)
        
        ### move on to y parameter
        
        # find related paraCode
        ypc=GetRelatedPC(comb['yCode'],df_testRuns['paraCode'].unique(),True)
        # if can't find related code then skip
        if ypc==None:
            continue
        
        df_y=df_sub.query('paraCode=="'+ypc+'"')
            
        ### check df_t1 not returned empty
        if df_y.empty:
            print("empty df :( Skipping")
            continue

        df_y=df_y.rename(columns={"paraValue": ypc}).reset_index(drop=True)
        
        # concatonate parameters
        df_xy=pd.concat([df_x,df_y[ypc]], axis=1)
        if df_xy.empty:
            print("derived dataframe is empty")
            continue
        # tidy of columns
        df_xy=df_xy[['serialNumber', 'alternativeIdentifier', 'compTypeCode','paraCode', 'date', 'institution', xpc, ypc, 'valueType', 'testIndex']]
        # display(df_xy)

        ### force arrays to be same size to explode to points (== df rows)
        df_xy[xpc]=df_xy.apply(lambda row: AlignLengths(row[xpc],row[ypc]), axis=1)
        df_xy[ypc]=df_xy.apply(lambda row: AlignLengths(row[ypc],row[xpc]), axis=1)
        # explode together
        df_xy=df_xy.explode([xpc,ypc])
        # absolutise!
        df_xy[xpc]=df_xy[xpc].astype('float').abs()
        df_xy[ypc]=df_xy[ypc].astype('float').abs()

        ### make x-y line plot
        # altair visualisation
        bespoke=alt.Chart(df_xy).mark_line(point=True).encode(
                x=alt.X(xpc+':Q'),
                y=alt.Y(ypc+':Q'),
                color=alt.Color('compTypeCode:N'),
                shape=alt.Shape('testIndex:N'),
                strokeDash=alt.StrokeDash('testIndex:N', legend=None),
                detail=alt.Detail('date:N'),
                tooltip=[xpc+':Q',ypc+':Q','serialNumber:N','testIndex:N']
            ).properties(
                width=600,
                title=con+": "+ypc+" vs. "+xpc
            ).interactive()
        # add to report data
        uploads[-1]['dictList'].append({'text':con.split('-')[0],'plot':bespoke,'df':df_xy})

    ### return report data
    return uploads

###############
### Yields
###############

def CalcYield(myClient, df_compInfo, specList, nameStr=None, chunk=100):

    repPage=[]
    df_sum=df_compInfo.copy(deep=True)
    # print(df_sum.columns)
            
    for compType in df_sum['componentType'].unique():
        if nameStr==None:
            repPage.append({'dictList':[], 'name':compType+" Yield Calculation"})
        else:
            repPage.append({'dictList':[], 'name':nameStr+" Yield Calculation"})

        print(f"look for {compType}")
        df_compType=df_sum.query('componentType=="'+compType+'"')
        if df_compType.empty:
            print(f"But I don't see any: {compType}")
            continue        
        
        for ct in df_compType['type'].unique():
            repPage[-1]['dictList'].append({'text':"## Yield Calculation"})
            
            ### per type
#             df_spec=df_compType.query(f'type=="{str(ct)}"')
            df_spec=df_compType[df_compType['type']==ct]
            if df_spec.empty:
                print(f"But I don't see any: {ct}")
                continue
            
            ### Initial sample
            yieldList=[]
            print(f"\tdenom: {len(df_spec)}")
            yieldList.append({'name':"original",'count':len(df_spec),'description':"denominator"})

            ### loop over yield specs
            for e,spec in enumerate(specList):
                print(f"\tSpec:{e}")
                print(spec)
                specName="default"
                if "name" in spec.keys():
                    specName=spec['name']
                df_spec['the_yield']=False
                
                ### simple assembled
                if "keyValue" in spec.keys():
                    print("\tusing keyValue")

                    df_spec['the_yield']=df_spec.apply(lambda row: CheckWithSpec(dict(row),spec['keyValue']), axis=1) 
                    yieldList.append({'name':specName,'type':"keyValue",'count':len(df_spec.query('the_yield=="True"')),'description':spec})
                    
                    print(f"\tYield results:")
                    for val in df_spec['the_yield'].unique():
                        print(f"\t\t{val}: {len(df_spec[df_spec['the_yield']==val])}")
                    
                if "parent" in spec.keys() or "child" in spec.keys():
                    print("\tusing relatives...")
                    relatives="children"
                    relation="child"
                    if "parent" in spec.keys():
                        relatives="parents"
                        relation="parent"
                    print(f"\t{relatives}")
                    
                    # get relative
                    print(f"\tusing {relation} spec")
                    df_rel=df_spec[['code','serialNumber',relatives]]
                    df_rel=df_rel.explode(relatives)
                    df_rel[relation+'Code']=df_rel[relatives].apply(lambda x: x['component'] if type(x)==type({}) and "component" in x.keys() else None)
                    df_rel[relation+'Code']=df_rel[relation+'Code'].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else None)
                    df_rel=df_rel[df_rel[relation+'Code'].notna()].reset_index(drop=True)
                    print(f"\tlength: {len(df_rel)}")

                    # display(df_compInfo)

                    print(f"\tgetting {relation} info")
                    compInfoList=df_rel.query(relation+'Code!=None')[relation+'Code'].to_list()
                    df_rel['the_yield']=False
                    for r in range(0,int(np.ceil(len(compInfoList)/chunk))):
                        print(f"\t{relatives} loop {r}: [{r*chunk}:{(r+1)*chunk}]")
                        bulkFail=False
                        for z in range(0,10,1):
                            try:
                                relInfo=myClient.get('getComponentBulk', json={'component': compInfoList[r*chunk:(r+1)*chunk] })
                                break
                            except itkdb.exceptions.ServerError as e:
                                print(f"ERROR\n{e}")
                                print(f"SERVER ISSUE - TRY AGAIN {z}")
                            if z==9:
                                bulkFail=True
                        if bulkFail:
                            print("- bulk retrieval has failed. Skipping chunk!")
                            continue
#                         relInfoList.extend(retInfo)
#                         print(f"\t{len(relInfo)} from PDB")
                        
                        foundCount=0
                        for ri in relInfo:
                            df_rel.loc[df_rel[relation+'Code'] == ri['code'], "the_yield"] = CheckWithSpec(ri, spec[relation])
            
                    yieldList.append({'name':specName,'type':relation,'count':len(df_rel.query('the_yield=="True"')),'description':spec})

                    print(f"\tYield results:")
                    for val in df_rel['the_yield'].unique():
                        print(f"\t\t{val}: {len(df_rel[df_rel['the_yield']==val])}")

            df_yield=pd.DataFrame(yieldList)
            df_yield['yield']=df_yield['count'].apply(lambda x: 100*float(x)/yieldList[0]['count'])

    #         display(df_yield)
            repPage[-1]['dictList'].append({'text':ct, 'df':df_yield})
    
    return repPage


###############
### custom plotting part
###############

### define function
### get settings from altair "source" - follow the TRHS dots
def MakeCustomChart(df_in,chartSpec_in=None):

    # paracode conversion
    df_plot=pd.DataFrame()
    # keep list of things to explode later
    explodeList=[]
    # spec copy
    chartSpec=copy.deepcopy(chartSpec_in)
    # loop over settings
    for k,v in chartSpec.items():
        # get dictionaries with "paracode" in value of "field" key
        if type(v)==type({}):
            # print(f"got {k}: {v}")
            for l,w in v.items():
                if type(w)==type({}):
                    # print(f"got {l}: {w}")
                    if "field" in w.keys():
                        print(f"got {w}")
                        if "paracode" in w['field'].lower():
                            # found paracode
                            # skip if counting
                            if "count()" in w['field']:
                                continue
                            paraStr=w['field'].split('=')[-1].split(':')[1]
                            print(f"found {w['field']}, get: {paraStr}")
                            # get relevant subset of data
                            df_sub=df_in.query('paraCode=="'+paraStr+'"').copy(deep=True)
                            df_sub[paraStr]=df_sub['paraCode']
                            print(f"updating: {k}, {l}, field to {paraStr}")
                            chartSpec[k][l]['field']=paraStr
                            if "tooltip" in chartSpec['encoding']:
                                print("got tooltip")
                                for t in range(0,len(chartSpec['encoding']['tooltip']),1):
                                    if w['field'] in chartSpec['encoding']['tooltip'][t]['field'] and "paraCode" in chartSpec['encoding']['tooltip'][t]['field']:
                                        print(f"updating: associated tooltip field to {paraStr}")
                                        chartSpec['encoding']['tooltip'][t]['field']=paraStr
                            explodeList.append(paraStr)
                            if df_sub.empty:
                                print(f"problem with this field: {w['field']}!")
                            if df_plot.empty:
                                df_plot=df_sub
                            else:
                                df_plot=pd.concat([ df_plot, df_sub ])
                        # else:
                        #     if "count()" in w['field']:
                        #         continue
                            # if df_plot.empty:
                            #     df_plot=df_in[w['field']]
                            # else:
                            #     df_plot=pd.concat([ df_plot, df_in[w['field']] ])

    df_plot=df_plot.reset_index(drop=True)
    print("initial columns",df_plot.columns)
    # check data exists
    if df_plot.empty:
        print("empty dataframe. exit MakeCustomChart")
        return None
    print("pivotting columns")
    # display(df_plot.groupby(by=['serialNumber','alternativeIdentifier','institution','date','testIndex']).count().query('testType>2'))

    df_plot=df_plot.pivot(index=['serialNumber','alternativeIdentifier','institution','date','testIndex'], columns='paraCode', values='paraValue').reset_index()
    # display(df_plot)
    print("pivotted columns",df_plot.columns)

    df_plot=df_plot.explode(explodeList)

    # set chart data
    chart=alt.Chart(df_plot)
    
    print(chartSpec)

    # loop over settings
    for k,v in chartSpec.items():
        if k=="plotType":
            continue
        try:
            chart[k]=v
        # except altair.exceptions.SchemaValidationError:
        except:
            print(f"cannot set property: {k} to {v}")

    # make name
    text="Custom Plot"
    # set title
    if "title" in chartSpec.keys():
        chart=chart.properties( title=chartSpec['title'] )
        text=chartSpec['title']
    # return
    return {'text':text, 'plot':chart, 'df':df_plot}


### define function to make plot from data + spec
def MakeCustomChartOld(df,chartSpec=None):
    # set chart data
    df_in=df
    print(chartSpec)
    for k in ['x','y','X','Y']:
        try:
            if "paraCode" in chartSpec[k]:
                print(f"found {chartSpec[k]}, get: {chartSpec[k].split('=')[-1].split(':')[0]}")
                df_in=df.query('paraCode=="'+chartSpec[k].split('=')[-1].split(':')[0]+'"')
        except KeyError:
            pass
    if df_in.empty:
        print("empty dataframe. exit MakeCustomChart")
        return None
    chart=alt.Chart(df_in)
    # set chart type
    if chartSpec['type']=="point":
        chart=chart.mark_point()
    elif chartSpec['type']=="circle":
        chart=chart.mark_circle()
    elif chartSpec['type']=="rect":
        chart=chart.mark_rect()
    elif chartSpec['type']=="bar" or chartSpec['type']=="hist":
        chart=chart.mark_bar()
    else:
        print("chart type unknown")
        return None
    # check keys
    chartSpecKeys=[x.lower() for x in chartSpec.keys()]
    if 'x' not in chartSpecKeys or 'y' not in chartSpecKeys:
        print("'x' or 'y' missing from chart specifications")
        return None
    # set keys
    altMap={'x':alt.X,'y':alt.Y,'color':alt.Color,'size':alt.Size,'shape':alt.Shape} # channel map
    for k,v in chartSpec.items():
        if k.lower() in ["title","width","type"]:
            continue
        try:
            pv=v
            if "paraCode" in v:
                pv="paraValue:"+v.split(':')[-1]
            if "hist" in chartSpec['type'] and "x"==k.lower():
                chart=chart.encode( altMap[k.lower()](pv, bin=True) )
            else:
                chart=chart.encode( altMap[k.lower()](pv) )
        except KeyError:
            print(f"no matching altair channel for {k.lower()}")

    text="Custom Plot"
    # set title
    if "title" in chartSpecKeys:
        chart=chart.properties( title=chartSpec['title'] )
        text=chartSpec['title']
    # return
    return {'text':text, 'plot':chart, 'df':df_in}



###############
### alerting part
###############
### collect outliers using definitons (outDict) - suitable for numeric values only!
def GetOutliers(df_paraData, PARAMETER_CODE, outDict=None):

    print(f"Getting outliers for {PARAMETER_CODE}")
    ### make stat dict for plot dressing (2 sigma default)
    statDict=MakeStatDict(df_paraData)

    if outDict!=None and type(outDict)==type({}):
        print(f" - got outlier dictionary: {outDict}")
        if "sigma" in outDict.keys():
            print(f"\t - setting sigmas: {outDict['sigma']}")
            statDict['lo']=statDict['mean']-outDict['sigma']*statDict['std']
            statDict['hi']=statDict['mean']+outDict['sigma']*statDict['std']
        minList=["min","<","<="]
        for ml in minList:
            if ml in outDict.keys():
                print(f"\t - setting min: {outDict[ml]}")
                statDict['lo']=outDict[ml]        
        maxList=["max",">",">="]
        for ml in maxList:
            if ml in outDict.keys():
                print(f"\t - setting max: {outDict[ml]}")
                statDict['hi']=outDict[ml]

    ### outs, nots & equals (mixes permitted)
    df_not=pd.DataFrame()
    df_eq=pd.DataFrame()
    df_out=pd.DataFrame()
    # check dictionary defined
    if outDict!=None and type(outDict)==type({}):
        # outs (do if any present)
        outList=["min","max","sigma"]
        if any(map(lambda x: x in [o.lower() for o in outDict.keys()], outList)):
            df_out=df_paraData.query(f'paraValue<{statDict["lo"]} or paraValue>{statDict["hi"]}').reset_index(drop=True)
            # print(df_paraData.columns)
            if not df_out.empty:
                df_out=df_out[['serialNumber','institution','stage','paraCode','paraValue']]
                df_out['reason']="outlier: x<{:.2f}, x>{:.2f}".format(round(statDict['lo'],2),round(statDict['hi'],2))
        # nots (use correct key)
        notList=["not","!="]
        for nl in notList:
            if nl.lower() in [o.lower() for o in outDict.keys()]:
                df_not=df_paraData.query(f'paraValue!={outDict[nl]}').reset_index(drop=True)
                if not df_not.empty:
                    df_not=df_not[['serialNumber','institution','stage','paraCode','paraValue']]
                    df_not['reason']="not: x=={:.2f}".format(round(outDict[nl],2))
        # equals (use correct key)
        eqList=["equals","equal","==","is"]
        for el in eqList:
            if el.lower() in [o.lower() for o in outDict.keys()]:
                df_eq=df_paraData.query(f'paraValue=={outDict[el]}').reset_index(drop=True)
                if not df_eq.empty:
                    df_eq=df_eq[['serialNumber','institution','stage','paraCode','paraValue']]
                    df_eq['reason']="equal: x!={:.2f}".format(round(outDict[el],2))
    # if no dictionary defined then use default 2 sigma
    else:
        df_out=df_paraData.query(f'paraValue<{statDict["lo"]} or paraValue>{statDict["hi"]}').reset_index(drop=True)
        # print(df_paraData.columns)
        if not df_out.empty:
            df_out=df_out[['serialNumber','institution','stage','paraCode','paraValue']]
            df_out['reason']="outlier: x<{:.2f}, x>{:.2f}".format(round(statDict['lo'],2),round(statDict['hi'],2))

    df_alert=pd.DataFrame()
    # loop over dfs
    for df in [df_out, df_not, df_eq]:
        # keep if non-empty
        if not df.empty:
            # equal or update
            if df_alert.empty:
                df_alert=df
            else:
                df_alert=pd.concat([df_alert,df]).reset_index(drop=True)

    if df_alert.empty:
        print("No outliers found!")
        print(statDict)

    # display(df_alert)

    return df_alert


### Outlier checks - for each parameter in testType, check alter definitions (alertDict)
def ParameterAlerts(myClient, df_testRuns, alertDict=None):
    # use large number in case of many rows in dataframe
    alt.data_transformers.enable('default', max_rows=1000000)
    # list of report data
    uploads=[]
    df_sum=df_testRuns.copy(deep=True)

    ### skip empty
    if df_sum.empty:
        return pd.DataFrame()
    
    for compType in df_sum['compTypeCode'].unique():
        uploads.append({'dictList':[], 'name':compType+" DQ Checks"})

        df_compType=df_sum.query('compTypeCode=="'+compType+'"')
    
        projCode=df_compType['projCode'].unique()[0]
        testCode=df_compType['testCode'].unique()[0]
        ### get testType schema
        testSchema=myClient.get('getTestTypeByCode', json={'project':projCode, 'componentType':compType, 'code':testCode})
        df_testParameters=pd.json_normalize(testSchema['parameters'],sep='_')

        ### try to pick useful identifier (use code if nothing else)
        idCol="serialNumber"
        for id in ['serialNumber','alternativeIdentifier','compCode']:
            if id in df_compType.columns and len(df_compType[id].unique())!=1:
                idCol=id
                break
        print(f"using identifier: {idCol}")

        # loop over parameter codes
        for i,row in df_testParameters.iterrows():
            print(f"checking {row['code']}")# ({row['valueType']})")
            # display(df_testRuns.query('paraCode=="'+row['code']+'"'))

            # only take data from relevant test parameter
            df_sub=df_sum.query('paraCode=="'+row['code']+'"')
            
            ### check if object id dictionary (will have columns)
            if df_sub['paraValue'].transform(lambda x: x.apply(type).eq(dict)).all():
                print("dictionary found")
                df_dict=pd.json_normalize(df_sub['paraValue'])
                df_total=pd.DataFrame()
                # if dictionary, go through columns and match to "pointed" parameter (i.e. "-->PARA")
                for e,c in enumerate(df_dict.columns):
                    print("working on",c)
                    df_col=df_sub.copy(deep=True)
                    df_col['paraValue']=df_col['paraValue'].apply(lambda x: x[c] if isinstance(x, (dict)) and c in x.keys() else None )
                    df_col['paraCode']=row['code']+'->'+c
                    if e==0:
                        df_total=df_col
                    else:
                        df_total=pd.concat([df_total,df_col])
                df_sub=df_total.copy(deep=True)
            
            for pc in df_sub['paraCode'].unique():
                print("paraCode:",pc)
                
                # plotting for non-lists
                if True not in (df_sub.query('paraCode=="'+pc+'"').applymap(type) == list)['paraValue'].to_list():
                    print(" - does NOT look like a list")

                    ### skip strings
                    if True not in (df_sub.query('paraCode=="'+pc+'"').applymap(type) == str)['paraValue'].to_list():
                        print(f" - does NOT look like a string")

                        ### identify outliers (using dictionary)
                        df_alert=pd.DataFrame()
                        if alertDict!=None:
                            # check if dictionary and parameter code present
                            if type(alertDict)==type({}) and pc in alertDict.keys():
                                df_alert=GetOutliers(df_sub.query('paraCode=="'+pc+'"'), pc, alertDict[pc])
                            else: # default 2 sigma
                                df_alert=GetOutliers(df_sub.query('paraCode=="'+pc+'"'), pc)
                        
                        # add to report 
                        if df_alert.empty:
                            uploads[-1]['dictList'].append({'text':f"{pc} - _no_ alerts"})
                        else:
                            uploads[-1]['dictList'].append({'text':f"{pc} - {len(df_alert.index)} alerts", 'df':df_alert})

                        continue

                # plotting for lists
                print(" - looks like a list (or string)")
    

    ### return report data
    return uploads

