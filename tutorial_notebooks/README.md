# Tutorial notebooks

Notebooks to help understand reporting structure.

### Required packages

- [itkdb](https://pypi.org/project/itkdb/#description)
- [pandas](https://pandas.pydata.org)
- [altair](https://altair-viz.github.io)
- [datapane](https://datapane.com)

---

## Tutorials

Tutorials cover 4 main parts of reporting:

 1. [population definition](#population-definition)
 2. [extraction of data](#extraction-of-data)
 3. [visualisation of data](#visualisation-of-data)
 4. [distribution of reports](#distribution-of-reports)

--- 

## Population Definition

Examples of defining populations of components:

 - list of component serialNumbers
 - component specifications with institution code
 - component specifications with cluster code
 - component specifications and use project code

---

## Extraction of Data

Examples of extracting data from defined population:

 - define testType and extract _all_ parameters

---

## Visualisation of Data

Examples of visualising extracted data:

 - use _DQ_ flag to generate data quality plots
     - timeline of parameter uploads and histograms of parameter values 
 - defined custom plots, e.g. histogram and timeline

---

## Distribution of Reports

Examples of generating and distributing reports:

 - local report: _html_ saved to local file system
 - remote report: data uploaded and hosted on _datapane_ servers
     - requires _datapane_ account

---
