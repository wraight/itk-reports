# Advanced notebooks

Notebooks with examples of report generation

- generated via script (*notebook_builder.py*) from example notebooks
- read functions from _commonCode_ directory

### Required packages

- [itkdb](https://pypi.org/project/itkdb/#description)
- [pandas](https://pandas.pydata.org)
- [altair](https://altair-viz.github.io)
- [datapane](https://datapane.com)

---

## Reporting examples

Example notebooks...

Dashboards:

 - **testType dashboard**: checking data quality of PDB uploads per test type
     - population: strips (S) MODULEs at Glasgow (GL)
     - extraction: 
         - testType: MODULE_METROLOGY
     - visualisation: data quality plots
     - distribution: local

 - **componentType dashboard**: checking component locations, stage progression, quality control
    - population: strips (S) MODULEs at Rutherford (RAL)
    - extraction: 
        - component summary information
        - stage summary information
        - test summary information
    - visualisation: 
        - component summary plots
        - stage summary plots
        - test summary plots
    - distribution: local

 - **inventory dashboard**: checking components at institution
    - population: strips (S) components at Oxford (OX)
    - extraction: 
        - stages
        - tests
    - visualisation: stages & tests
    - distribution: local

 Comparisons:
 
 - **parameter comparison**: comparing test parameters for correlations
     - population: strips (S) MODULEs at Rutherford (RAL) and child SENSORs
     - extraction: 
         - parent: MODULE_METROLOGY_>PB_POSITION_>PB_P1[0]
         - child: ATLAS18_SHAPE_METROLOGY_V1->BOWING
     - visualisation: data quality plots and comparison
     - distribution: local

 - **parameter consistency**: comparing consistency of test runs per component over production
     - population: strips (S) MODULEs at Glasgow (GL) and child SENSORs
     - extraction: 
         - parent: MODULE_IV_PS_V1->VOLTAGE, MODULE_IV_PS_V1->CURRENT
         - child: ATLAS18_IV_TEST_V1->VOLTAGE, ATLAS18_IV_TEST_V1->CURRENT
     - visualisation: per component plots (serve as data quality) and parent-child comparison
     - distribution: local

All reports can be editted using the _settingDict_ object defined in the notebook.

<!-- ```python
def mthfunc():
    print("thing")
    return None
``` -->