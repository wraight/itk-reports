# base image
FROM python:3.10

# Update package manager and get useful packages
USER root
RUN apt-get update
RUN apt-get install -y cron nano

# make and set working directory
RUN mkdir /code
WORKDIR /code 
# get useful python packages (from requirements file)
COPY requirements.txt requirements.txt
RUN true \
    && pip3 install -r requirements.txt \
    && pip3 install urllib3==1.26.15 requests-toolbelt==0.10.1

ARG MY_USR=None
ARG MY_PWD=None
# set up git and clone repository
RUN true \ 
    && git clone --depth 1 https://$MY_USR:$MY_PWD@gitlab.cern.ch/wraight/itk-reports.git \
    && pip3 install -r itk-reports/requirements.txt
# copy commonCode dir to top so that scripts can find it later
RUN true \ 
    && mkdir -p /code/commonCode \
    && cp -r /code/itk-reports/commonCode/ /code/

### make and fill directories
WORKDIR /code
RUN true \
    && cp /code/itk-reports/commonCode/* ./commonCode/ \
    && mkdir ./specFiles \
    && mkdir ./outputs 
COPY cronJobs/*.py /code/
COPY cronJobs/*.json ./specFiles/

### add user other than root (for cern deployment)
RUN adduser appuser

### set permissions
RUN true \ 
    && mkdir /.config \
    && chown -R appuser:appuser /.config \
    && chmod 777 -R /.config \
    && mkdir .webcache \
    && chown -R appuser:appuser .webcache \
    && chmod 777 -R .webcache \
    && chown -R appuser:appuser outputs \
    && chmod 777 -R outputs 

### switch to appuser (for Cern)
USER appuser

ARG MY_AC1=None
ARG MY_AC2=None

ENV AC1=$MY_AC1
ENV AC2=$MY_AC2

RUN echo $(date)
# CMD python3 OEC_dash_script.py --file specFiles/OEC_spec.json 
CMD /bin/echo "ready "$(date)

