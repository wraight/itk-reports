# Running cronjobs

Some instructions are given to run scheduled reporting tasks using Docker on Cern's OpenShift platform

---

## Contents

1. [Dockerfiles](#dockerfiles) 

    - setting up and testing locally

2. [Running on OpenShift](#running-on-openshift)

    - running with OKD

3. [Example scripts](#example-scripts)


---

## Dockerfiles

The docker file examples here follow general scheme:

- set up python friendly container
- clone itk-reports repository
- set permissions for non-root usage (this allows compatibility with Cern's Openshift)

These example docker files makes use of build arguments to avoid storing passwords in repository. The required build arguments are:

- MY_USR: gitlab user name
- MY_PWD: gitlab user password
- MY_AC1: PDB access token 1
- MY_AC2: PDB access token 2

Schematic to build locally:

> docker build -f DOCKER_FILE_NAME -t SOME_TAG --build-arg MY_USR=GITLAB_USER_NAME --build-arg MY_PWD=GITLAB_USER_PASSWORD --build-arg MY_AC1=PDB_ACCESS_TOKEN1 --build-arg MY_AC2=PDB_ACCESS_TOKEN1 .

Schematic to run locally:

> docker run SOME_TAG python REPORTING_SCRIPT -f SPEC_FILE

Check for expected outputs.

---

## Running on OpenShift

Dockerfiles can be run from Cern's Openshift platform. This is set-up using [OKD](https://paas.cern.ch).

### Add build to project

<figure markdown>
   ![image info](/images/cronJobs/okd_add.png){ width="400" }
  <figcaption>Add build to project </figcaption>
</figure>

<figure markdown>
   ![image info](/images/cronJobs/okd_git_import.png){ width="400" }
  <figcaption>Import from git </figcaption>
</figure>

<figure markdown>
   ![image info](/images/cronJobs/okd_docker.png){ width="400" }
  <figcaption>Dockerfile selection </figcaption>
</figure>

<figure markdown>
   ![image info](/images/cronJobs/okd_general.png){ width="400" }
  <figcaption>Dockerfile selection </figcaption>
</figure>

<figure markdown>
   ![image info](/images/cronJobs/okd_advanced.png){ width="400" }
  <figcaption>Advanced selection </figcaption>
</figure>

### Topology

<figure markdown>
   ![image info](/images/cronJobs/okd_topo_build.png){ width="400" }
  <figcaption>Build in topology </figcaption>
</figure>

<figure markdown>
   ![image info](/images/cronJobs/okd_topo_build_success.png){ width="400" }
  <figcaption>Completed build in topology </figcaption>
</figure>

### Setting up build

Follow the steps:

<figure markdown>
   ![image info](/images/cronJobs/okd_role.png){ width="400" }
  <figcaption>Role selection </figcaption>
</figure>

<figure markdown>
   ![image info](/images/cronJobs/okd_build.png){ width="400" }
  <figcaption>Build selection </figcaption>
</figure>

<figure markdown>
   ![image info](/images/cronJobs/okd_dots.png){ width="400" }
  <figcaption>Settings dots </figcaption>
</figure>

 - Navigate to project
 - Set _Developer_ view from lefthand side tabs
 - Select _Builds_ from lefthand side tabs 
    - get list of builds
 - Click three dots to righthand side of build and selecct _Edit BuildConfig_ 
 - Use _YAML view_ from top bar options
 - set up file:

```
  strategy:
    type: Docker
    dockerStrategy:
      dockerfilePath: PATH_TO_DOCKERFILE_IN_REPO
      buildArgs:
        - name: MY_USR
          value: GITLAB_USER_NAME
        - name: MY_PWD
          value: GITLAB_USER_PASSWORD
        - name: MY_AC1
          value: PDB_ACCESS_TOKEN1
        - name: MY_AC2
          value: PDB_ACCESS_TOKEN2
```

### Setting up cronJob

<figure markdown>
   ![image info](/images/cronJobs/okd_workloads.png){ width="400" }
  <figcaption>Administrator workloads </figcaption>
</figure>

Follow the steps:

 - Navigate to project
 - Set _Administrator_ view from lefthand side tabs
 - Select Workloads > CronJobs from lefthand sidr bar
 - Click _Create CronJob_ button
 - Edit the YAML file...

__Scheduling__

See [here](https://crontab.guru) for a useful tool to set schedule in expected format

``` 
  spec:
    schedule: SOME_FREQUENCY
    concurrencyPolicy: Replace
```

__NB__ if you do not see the concurrencyPolicy key on creation then save without, then reopen and set using "Edit CronJob" from options on three dots to RHS

__Running__

Set runtime arguments:

- reporting script: what kind of report to run
- spec file: specification file to set reporting details

```
  jobTemplate:
    spec:
      template:
        spec:
          containers:
            - name: alerting-test-s
              image: 'image-registry.openshift-image-registry.svc:5000/itk-reporting-cronjobs/alerting-test:latest'
              args:
                - python
                - REPORTING_SCRIPT
                - '-f'
                - SPEC_FILE
```


<figure markdown>
   ![image info](/images/cronJobs/okd_topo_cron.png){ width="400" }
  <figcaption>Build with cron job </figcaption>
</figure>

---

## Using Common Images across Multiple Projects

Docker hierarchies are useful ways to share common infrastructure across projects. Common code can be group in a _base_ image, which it then used by _higher_ images to complete the application. 
Higher level images will inherit all the settings of the base repository such as directory structure, installed packages, port settings, read/write credentials.

A _base_ image can be shared by:

1. Generate image (see [above](#dockerfiles)).

2. Upload image to an accessible repository:

  - [dockerHub](https://hub.docker.com): docker's online image repository. __NB__ Limitations depending on price plan. See [docker cheatsheet](https://docs.docker.com/get-started/docker_cheatsheet.pdf) for push/pull instruction.

  - [Cern repository](https://registry.cern.ch/): Cern online image repository. (for motivation see [here](https://indico.cern.ch/event/995485/contributions/4258063/attachments/2209856/3739754/EBocchi%2C%20Distribution%20of%20Containers.pdf))

    - [documentation](https://paas.docs.cern.ch/2._Deploy_Applications/Deploy_From_Git_Repository/9-use-harbor-for-s2i-builds/)

3. Access image in Dockerfile.

  ```
  FROM IMAGE_NAME:OPTIONAL_TAG
  ```

  For example...

   - local: USER_NAME/IMAGE_NAME

   - docker: USER_NAME/IMAGE_NAME

   - harbour: registry.cern.ch/PROJECT_NAME/REPOSITORY_NAME

### General Instructions for Cern Repository

To upload images:

1. Build locally:

  > docker build -f DOCKER_FILE -t IMAGE_TAG.

2. Tag image:

  > docker tag IMAGE_NAME  registry.cern.ch/PROJECT_NAME/REPOSITORY_NAME

3. Login to [harbour](https://registry.cern.ch/):

 - docker login registry.cern.ch/PROJECT_NAME/REPOSITORY_NAME
    
  - Create robot account (if not one already): [instructions](https://goharbor.io/docs/2.4.0/working-with-projects/project-configuration/create-robot-accounts/#add-a-robot-account)
    
  - Add robot account username, then secret

4. Push to harbour

  > docker push registry.cern.ch/PROJECT_NAME/REPOSITORY_NAME

#### Cross Architecture Caveat

If cross-architecture, e.g. writing on _Mac_ and pushing as _linux_:

- Use (big) command:

  > docker buildx build --platform linux/amd64,linux/arm64 -f DOCKER_FILE -t registry.cern.ch/PROJECT_NAME/REPOSITORY_NAME --push .

   - Images go straight to repository

   - __NB__ buildx images are not listed on local machine (so import from directly from repository)

---

## Example Scripts

A couple of example scripts are provided to give an example for scheduled running.

### Dashboard example

This script will collect Pixels (P) Bare Half-Rings (OEC_BARE_HR) data from Manchester (MAN) and Genoa (INFN_GENOA). Summaries of component information and test meta-data are compiled as well as plotting test data for parameters from Reception Tests (RECEPTION_TESTS). The dashboard is generated locally (inside the container) and then sent to eos.

files:

- script: *dashboard_script_example.py*
- spec file: *dashbaord_spec_exampleP.json_*


### Alerting example

This script will collect Strips (S) Thermal foam (THERMALFOAMSET) compoennt data from across the whole project. Test data for parameters from Weighing Tests (WEIGHING) are plotted. Exceptional data is defined for alerting as Weight (WEIGHT) parameter values outside 2 sigma. The dashboard is generated locally (inside the container) and then sent to eos. The alert summary is sent to "recipient_email" using sender _gmail_ account along with a link to the full report.

files:

- script: *alert_script_example.py*
- spec file: *alert_script_exampleS.json_*
