### get itkdb for PDB interaction
import os
import sys
# !{sys.executable} -m pip install pandas==1.3.4
import pandas as pd
import altair as alt
import numpy as np
import copy
# !{sys.executable} -m pip install itkdb==0.4
import itkdb
import itkdb.exceptions as itkX
import datetime
import json
### common code
# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/commonCode')
from commonSpecReader import *
from commonCredentials import *
from commonPopulation import *
from commonExtraction import *
from commonVisualisation import *
from commonDistribution import *

alt.data_transformers.enable('default', max_rows=None)


#################
### custom functions bit
#################


#######
# main
#######
if __name__ == '__main__':

    settingDict=[]

    args=GetArgs('testType dashboard')
    chunk = 100
    if args.chunk!=None:
        chunk=args.chunk

    if args.file==None:
        print("No specification file provide.")
        print("Please provide spec file. See \"$(pwd)/testSpecs\" for examples")
        exit(0)
        # print(args)
    else:
        print("Reading specification file")
        settingDict=GetSpecFromFile(args.file)
        # print("\n### Initial settingDict ###\n",settingDict)
        # add any commandline stuff
        if args.keys!=None:
            settingDict=UpdateSpec(settingDict,args)
            if settingDict==None:
                print("Issue with spec update :(")
                exit(0)

    ### copy original dictionary to upload to report later
    origDict=copy.deepcopy(settingDict)

    print("\n### Final settingDict ###\n")
    print(json.dumps(settingDict, indent=4))

    ### PDB Client
    myClient=SetClient()

    #######
    # get components
    #######

    ### collect populations
    for pop in settingDict['population']:
        print(f"working on pop: {pop['alias']}")
        compInfo=[]
        ### if compList is available use it (priority)
        if "compList" in pop['spec'].keys():
            print(f"found component list: {pop['spec']['compList']}")
            compInfo=[{'code': x} for x in pop['spec']['compList']]
        ### else use XXXcode
        else:
            if 'clusCode' in pop['spec'].keys():
                print(f"found clusCode: {pop['spec']['clusCode']}")
                instList=GetClusterInstitutes(myClient, pop['spec']['clusCode'])
            elif 'instCode' in pop['spec'].keys():
                print(f"found instCode: {pop['spec']['instCode']}")
                instList=pop['spec']['instCode']
            elif 'instList' in pop['spec'].keys():
                print(f"found instList: {pop['spec']['instList']}")
                instList=pop['spec']['instList']
            else:
                print("no compList, instCode or clusCode found. Using project code")
                instList=GetProjectInstitutes(myClient, pop['spec']['projCode'])
            compInfo=GetComponentInfo(myClient, instList, pop['spec'])
        pop['compInfo']=compInfo
        print("===============")
        print(f"Found components for {pop['alias']}: {len(pop['compInfo'])}")
        print("===============\n")

    #######
    # get component testRun IDs
    #######
    for ext in settingDict['extraction']:
        print(f"working on ext: {ext['alias']}")
        compTestRunsInfo=[]
        for pop in settingDict['population']:
            if pop['alias'] in ext['usePopulations']:
                print(f"found pop:{pop['alias']}")

                ### get test IDs
                if "spec" in ext.keys():
                    compTestRuns=GetTestRunIDs(myClient, [x['code'] for x in pop['compInfo']], 100)
                    if len(compTestRuns)<1:
                        print(f"no test info for: {pop['alias']}")
                    else:
                        compTestRunsInfo.append(compTestRuns)
        
        ext['compTestRunsInfo']=compTestRunsInfo
        print("===============")
        print(f"testTypes for {ext['alias']} : {sum([len(ctri.keys()) for ctri in ext['compTestRunsInfo']])}")
        print("===============\n")

    ### list for matching testRuns
    for ext in settingDict['extraction']:
        print(f"working on ext: {ext['alias']}")
        matchedTestRuns=[]
        # get testTypes defined in settings
        if "spec" in ext.keys():
            if ext['spec']==None:
                print("skipping matchedTestRuns")
                matchedTestRuns=None
            elif "all" in ext['spec']:
                print("use *all* for matchedTestRuns")
                for ctri in ext['compTestRunsInfo']:
                    print(ctri)
                    for k,v in ctri.items():
                        print(k)
                        matchedTestRuns.extend(v)
            elif type(ext['spec'])==type([]):
                print("use subset for matchedTestRuns")
                for tc in set([x['testCode'] for x in ext['spec']]):
                    for ctri in ext['compTestRunsInfo']:
                        try:
                            matchedTestRuns.extend(ctri[tc])
                            print(f"found testType: {tc}")
                        except KeyError:
                            print(f"no matching testType: {tc}")
                            continue
            else:
                print("don't understand extraction spec:",ext['spec'])
        else:
            matchedTestRuns=None
        ### get test run data
        if matchedTestRuns==None:
            print(f"skipping testRuns for {ext['alias']}")
        else:
            testInfo=GetTestRunsData(myClient, matchedTestRuns)
            ext['testInfo']=testInfo
            print("===============")
            print(f"testRuns for {ext['alias']}: {len(ext['testInfo'])}")
            print("===============\n")


    #######
    # formatting
    #######
    ### list for matching testRuns
    for ext in settingDict['extraction']:
        print(f"working on ext: {ext['alias']}")

        if "compSummary" in ext.keys():
            compInfo=[]
            for pop in settingDict['population']:
                if pop['alias'] in ext['usePopulations']:
                    print(f"found pop:{pop['alias']}")
                    # get ids from dictionary of testTypes for matching testTypes (use set incase multiple same)
                    compInfo.extend(pop['compInfo'])
            df_compInfo=FormatComponentData(myClient,compInfo)
            ext['df_compInfo']=df_compInfo
                            
            print("===============")
            print(f"compSummary info. for {ext['alias']}: {ext['df_compInfo'].count()}")
            print("===============\n")

        elif "testSummary" in ext.keys():
            df_testInfo=FormatTestData(myClient,ext['testInfo'])
            ext['df_testInfo']=df_testInfo
                            
            print("===============")
            print(f"testSummary info. for {ext['alias']}: {ext['df_testInfo'].count()}")
            print("===============\n")
        
        else:
            df_testRuns=FormatTestRunData(ext['testInfo'])
            ext['df_testRuns']=df_testRuns
            print("===============")
            print(f"df_testRuns for {ext['alias']}: {ext['df_testRuns'].columns}")
            print("===============\n")


    #######
    # plotting
    #######
    ### list for matching testRuns
    for vis in settingDict['visualisation']:
        print(f"working on vis: {vis['alias']}")

        vis['standard_comp_plots']=[]
        df_compInfo=pd.DataFrame()
        vis['standard_test_plots']=[]
        df_testInfo=pd.DataFrame()
        vis['standard_DQ_plots']=[]
        df_DQ=pd.DataFrame()
        for ext in settingDict['extraction']:
            if ext['alias'] in vis['useExtractions']:
                print(f"found ext:{ext['alias']}")

                if "compSummary" in vis.keys():
                    if df_compInfo.empty:
                        df_compInfo=ext['df_compInfo']
                    else:
                        pd.concat([df_compInfo,ext['df_compInfo']])

                if "testSummary" in vis.keys():
                    if df_testInfo.empty:
                        df_testInfo=ext['df_testInfo']
                    else:
                        pd.concat([df_testInfo,ext['df_testInfo']])

                if "DQ" in vis.keys():
                    if df_DQ.empty:
                        df_DQ=ext['df_testRuns']
                    else:
                        pd.concat([df_DQ,ext['df_testRuns']])

        if not df_compInfo.empty:
            print("getting plots...")
            vis['standard_comp_plots']=ComponentSummary(myClient,df_compInfo)
        
        print("===============")
        print(f"compSummary info. for {vis['alias']}: {len(vis['standard_comp_plots'])}")
        print("===============\n")
        
        if not df_testInfo.empty:
            print("getting plots...")
            vis['standard_test_plots']=TestSummary(myClient,df_testInfo)
            vis['standard_test_plots'].extend(TestTimeLines(myClient,df_testInfo))
        
        print("===============")
        print(f"testSummary info. for {vis['alias']}: {len(vis['standard_test_plots'])}")
        print("===============\n")
        
        if not df_DQ.empty:
            print("getting plots...")
            vis['standard_DQ_plots']=DQPlotting_testRuns(myClient,df_DQ)
        
        print("===============")
        print(f"DQ Summary info. for {vis['alias']}: {len(vis['standard_DQ_plots'])}")
        print("===============\n")


    #######
    # sharing
    #######
    for dis in settingDict['distribution']:
        print(f"working on {dis['alias']}")
        # print(dis.keys())
        specChain={'distribution':dis, 'useVisualisations':[]}
        standard_plots=[]
        custom_plots=[]
        for vis in settingDict['visualisation']:
            if vis['alias'] in dis['useVisualisations']:
                print(f"found vis:{vis['alias']}")
                for pk in [k for k in vis.keys() if "_plots" in k]:
                    foundPlace=False
                    ### standard plot keys
                    if "standard" in pk:
                        print(f"found {len(vis[pk])} {pk} standard plots")
                        foundPlace=True
                        standard_plots.extend(vis[pk])
                    ### custom plot keys
                    elif "custom" in pk:
                        print(f"found {len(vis[pk])} {pk} custom plots")
                        foundPlace=True
                        custom_plots.extend(vis[pk])
                    ### other?
                    else:
                        print(f"don't recognise {len(vis[pk])} {pk} plots")
                    if not foundPlace:
                        print("Did not fiind a place for this plot.")
        # print(json.dumps(origDict, indent=4))
        DataPaneChunk(myClient, standard_plots, custom_plots, dis, origDict)

    ### get EOS stuff from environment
    eos_usr=CheckCredential("eos_usr")
    eos_pwd=CheckCredential("eos_pwd")

    # stop if eos info. missing
    if eos_pwd==None or eos_usr==None:
        print("EOS info. missing. Stop here.")
    else:
        print("Found EOS info. sending on.")
        for of in os.listdir("./outputs/"):
            if of[-5::]==".html":
                print(f"send {of}")
                if AltCopy(eos_usr, eos_pwd, "/eos/user/w/wraight/www/pixels-reports/"+of, "./outputs/"+of):
                    print("Sent to eos successfully")
                    remoteSave=True
                else:
                    print("Something went awry")

