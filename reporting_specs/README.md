# Reporting Specs

Specification files to steer reporting scripts.

Files are structred using _json_ formatting.

__NB__ when comparing to _settingDict_ in notebooks be careful with formatting:

- booleans, i.e. True --> true, False --> false
- None, i.e. None --> null
- use _double_-quotations, e.g. "key":'value' --> "key":"value"

---

## Steering Files

An example template is provided in *reporting_specs/report_template.json*

The steering file sections reflect the general reporting strategy:

1. __population__ - a list of specifications to find components
    - each element has a _spec_ dictionary to define components
        - a project code (_projCode_)
        - component type code (_compTypeCode_) to select components at institutions
        - further specification can be made using a cluster code (_clusCode_), institution code (_instCode_), list of institutions (_instList_) or list of component PDB _serialNumbers_  or _codes_ (_compList_)
        - any additional filters (_filters_) to be applied, e.g. child component specifications
    - a label (_alias_) is used to identify elements in the list
2. __extraction__ - a list of specifications to extract parameters from component test results
    - each element has a _spec_ list in case more than one test type is required per component type. Each element of the list is a dictionary to define test run parameters to extract per test type
        - a test type code (_testcode_)
        - a parameter code (_paraCode_)
        - a list of sub-parameter codes or list positions (_subs_)
    - a label (_usePopulations_) is used to associate data extraction to components defined in __population__
    - a label (_alias_) is used to identify elements in the list
3. __visualisation__ - a list of visualisation specifications
    - each element can toggle predefined data quality control plots (_DQ_). These will produce upload timelines, histograms and tables for each parameter defined in __data__
    - a list of custom plots (_custom_) can be defined. For each element a dictionary is defined to map parameters to (_altair_) visualisation channels: minimum _x_ & _y_ must be defined; other channels supported include _color_, _size_ & _shape_.
    - a label (_useExtractions_) is used to associate data extraction to components defined in __extraction__
    - a label (_alias_) is used to identify elements in the list
4. __distribution__ - a list of report specifications
    - the destination of the report can be set (_location_) to be to the _local_ files system or to _remote_ (datapane) servers
    - a report name (_reportName_) is used to identify the report
    - a list of visualisations (_useVisualisations_) to include in the report
    - if a _remote_ report is required, a _datapane_ token must be provided (_datapaneCode_)
    - if an _local_ report is required, a local directory can be provided (_reportDir_)
