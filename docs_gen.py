import os
from datetime import datetime
import shutil

#####################
### useful functions
#####################

def GetDateTimeNotice():
    dtArr=["!!! notice",
          f"\tPage generated: {datetime.now().date()} ({datetime.now().time()})",
          "\tFor updates/fixes contact: wraightATcern.ch"
        ]
    ### convert to string
    dtStr=""
    for ia in dtArr:
        dtStr+=f"{ia}\n\n"
    
    return dtStr


#####################
### main function
#####################
def main():

    ### find insteresting directories
    print("Copying READMEs")
    notebookFiles=[]
    otherFiles=[]
    for d in sorted(os.listdir(os.getcwd())):
        if os.path.isfile(d+"/README.md"):
            print(f"\tfound directory: {d}")
            fileName=d.split('/')[-1]
            print(f"\twriting: {fileName}")
            shutil.copyfile(d+"/README.md", f"docs/{fileName}.md")
            if "notebook" in fileName:
                notebookFiles.append(fileName)
            else:
                otherFiles.append(fileName)

    ### write index (with timestamp)
    print("Writing index")
    shutil.copyfile(os.getcwd()+"/README.md", "docs/index.md")
    shutil.copyfile(os.getcwd()+"/running_scripts.md", "docs/set-up.md")
    with open("docs/index.md", "a") as content_file:
        content_file.write(GetDateTimeNotice())


    ### update yml
    # notebooks
    with open("mkdocs.yml", "a") as content_file:
        content_file.write("  - Notebooks:\n")
        for cf in notebookFiles:
            newStr=f"    - {' '.join([x.title() for x in cf.split('/')[-1].split('_')])}: {cf+'.md'}\n"
            content_file.write(newStr)
    # others
    with open("mkdocs.yml", "a") as content_file:
        for cf in otherFiles:
            newStr=f"  - {' '.join([x.title() for x in cf.split('/')[-1].split('_')])}: {cf+'.md'}\n"
            content_file.write(newStr)

### for commandline running
if __name__ == "__main__":
    main()
    print("All done.")
