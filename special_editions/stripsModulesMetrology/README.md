# Strips Barrel Module Metrology

Strips Module Metrology summmary (May 2023)

Overview:
- US modules (PPA + PPB)
- Metrology tests
- Histos: with range for spec
- ABC points, Hybrids, PB points (colour by position)
- Tables: population, over/underflow

Tony's spec sheet: [here](https://docs.google.com/spreadsheets/d/1xrib5FaalLagrYohPyiZPikNHjxb-7b-EV7ww9mbCrA/edit#gid=0)

Exclusion info. [here](https://docs.google.com/spreadsheets/d/1QnDUvxa7LhvLFVYpcQ5P994WEcOImx9V4MFZSmLoOnY/edit#gid=615941927)

---

## Strips Barrel Module Metrology Report

Population definitions:
- __PowerBoards__: componentType PWB with child componentType AMAC
    - PPA: child type code TEST
    - PPB: child type code AMACSTAR
- __Hybrids__: componentType HYBRID_ASSEMBLY with child HCC 
    - PPA: child type name HCCStarv0 
    - PPB: child type name HCCStarv1
- __Modules__: componentType MODULE with child componentType SENSOR and type name ATLAS18

Extractions:
- component summary for each population
- stage summaries for each population 

Visualisations:
- component summary: per population
    - current locations
    - current stages
- stage summary: per population
    - stage history
- yields
    - Powerboards: both populations
        - parent componentType MODULE
        - not trashed
        - at stage: LOADED
        - at stage: MODULE_RCP
        - at stage: LOADED or MODULE_RCP
        - at stage: FAILED
    - Hybrids:  both populations
        - parent componentType MODULE
        - not trashed
        - at stage: ON_MODULE
        - at stage: AT_MODULE_ASSEMBLY_SITE
        - at stage: FINISHED_HYBRID
        - at stage: ON_MODULE or AT_MODULE_ASSEMBLY_SITE or FINISHED_HYBRID
        - at stage: FAILED
    - Modules:
        - not trashed
        - at stage: ON_CORE
        - at stage: AT_LOADING_SITE
        - at stage: FINISHED
        - at stage: ON_CORE or AT_LOADING_SITE or FINISHED
        - at stage: FAILED

---

## Running

From repository top directory.

For example:
> python special_editions/stripsPRR/stripsPRR_script.py --file special_editions/stripsPRR/stripsPRR_spec_UKChina.json 

__NB__ remember to export credentials in terminal session