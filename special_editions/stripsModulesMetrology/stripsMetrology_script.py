### get itkdb for PDB interaction
import os
import sys
import pandas as pd
import altair as alt
import numpy as np
import copy
import itkdb
import itkdb.exceptions as itkX
###


# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/commonCode')
from commonSpecReader import *
from commonCredentials import *
from commonPopulation import *
from commonExtraction import *
from commonVisualisation import *
from commonDistribution import *


#######
# main
#######
if __name__ == '__main__':

    settingDict=[]

    args=GetArgs('strips PRR reports')
    chunk = 100
    if args.chunk!=None:
        chunk=args.chunk

    if args.file==None:
        print("No specification file provide.")
        print("Please provide spec file. See \"$(pwd)/testSpecs\" for examples")
        exit(0)
        # print(args)
    else:
        print("Reading specification file")
        settingDict=GetSpecFromFile(args.file)
        # print("\n### Initial settingDict ###\n",settingDict)
        # add any commandline stuff
        if args.keys!=None:
            settingDict=UpdateSpec(settingDict,args)
            if settingDict==None:
                print("Issue with spec update :(")
                exit(0)

    ### copy original dictionary to upload to report later
    origDict=copy.deepcopy(settingDict)

    print("\n### Final settingDict ###\n")
    print(json.dumps(settingDict, indent=4))

    ### PDB Client
    myClient=SetClient()

    #######
    # define populations
    #######

    ### collect populations
    for pop in settingDict['population']:
        compInfo=[]
        ### if compList is available use it (priority)
        if "compList" in pop['spec'].keys():
            print("found component list")
            compInfo=[{'code': x} for x in pop['spec']['compList']]
        ### else use XXXcode
        else:
            locKey="currentLocation"
            if "locationKey" in pop.keys():
                locKey=pop['locationKey']
            if 'instList' in pop['spec'].keys():
                instList=pop['spec']['instList']
            elif 'clusCode' in pop['spec'].keys():
                instList=GetClusterInstitutes(myClient, pop['spec']['clusCode'])
            elif 'instCode' in pop['spec'].keys():
                instList=pop['spec']['instCode']
            else:
                print("no instCode, instList or clusCode found. Using project code")
                instList=GetProjectInstitutes(myClient, pop['spec']['projCode'],locKey)
            compInfo=GetComponentInfo(myClient, instList, pop['spec'],locKey)
            pop['compInfo']=compInfo
        print("===============")
        print(f"Found components for {pop['alias']}: {len(pop['compInfo'])}")
        print("===============\n")

    ### filter components
    for pop in settingDict['population']:
        print(f"working on {pop['alias']}")
        
        chunk=100
        filterInfo=[]
        print(f"\tcomponents length: {len(pop['compInfo'])}")
        for x in range(0,int(np.ceil(len(pop['compInfo'])/chunk))):
            print(f"component loop {x}: [{x*chunk}:{(x+1)*chunk}]")
            compInfo=myClient.get('getComponentBulk', json={'component': [p['code'] for p in pop['compInfo'][x*chunk:(x+1)*chunk]] })
            df_compInfo=pd.DataFrame(compInfo)

            ### filter components if required
            if len(pop['spec']['filters'])<1:
                # skip if no filters defined
                filterInfo.extend([{'code':code} for code in df_compInfo['code'].to_list()])
                continue
            else:
                ### loop over filter specs
                for e,filt in enumerate(pop['spec']['filters']):
                    print(f"\tFilter:{e}")
                    print(filt)
                    df_compInfo['filter']=False

                    ### simple assembled
                    if "keyValue" in filt.keys():
                        print("\tusing keyValue")
                        df_compInfo['filter']=df_compInfo.apply(lambda row: CheckWithSpec(dict(row),filt['keyValue']), axis=1) 

                        print(f"\tFilter results:")
                        for val in df_compInfo['filter'].unique():
                            print(f"\t\t{val}: {len(df_compInfo[df_compInfo['filter']==val])}")

                        filterInfo.extend([{'code':code} for code in df_compInfo.query('filter=="True"')['code'].to_list()])

                    if "parent" in filt.keys() or "child" in filt.keys():
                        print("\tusing relatives...")
                        relatives="children"
                        relation="child"
                        if "parent" in filt.keys():
                            relatives="parents"
                            relation="parent"
                        print(f"\t{relatives}")
                        
                        # get relative
                        print(f"\tusing {relation} spec")
                        df_rel=df_compInfo[['code','serialNumber',relatives]]
                        df_rel=df_rel.explode(relatives).reset_index(drop=True)
                        df_rel[relation+'Comp']=df_rel[relatives].apply(lambda x: x['component'] if type(x)==type({}) and "component" in x.keys() else None)
                        df_rel[relation+'Code']=df_rel[relation+'Comp'].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else None)
                        df_rel=df_rel[df_rel[relation+'Code'].notna()].reset_index(drop=True)
                        print(f"\t{relatives} length: {len(df_rel)}")


                        print(f"\tgetting {relation} info")
                        compInfoList=df_rel.query(relation+'Code!=None')[relation+'Code'].to_list()
                        df_rel['filter']=None
                        for r in range(0,int(np.ceil(len(compInfoList)/chunk))):
                            print(f"\tchild loop {r}: [{r*chunk}:{(r+1)*chunk}]")
                            relInfo=myClient.get('getComponentBulk', json={'component': compInfoList[r*chunk:(r+1)*chunk] })
                                
                            foundCount=0
                            for ri in relInfo:
                                df_rel.loc[df_rel[relation+'Code'] == ri['code'], "filter"] = CheckWithSpec(ri, filt[relation])

                        print(f"\tFilter results:")
                        for val in df_rel['filter'].unique():
                            print(f"\t\t{val}: {len(df_rel[df_rel['filter']==val])}")
                        
                        filterInfo.extend([{'code':code} for code in df_rel.query('filter=="True"')['code'].to_list()])
        
        print("===============")
        print(f"Unfiltered components for {pop['alias']}: {len(pop['compInfo'])}")
        pop['compInfo']=filterInfo
        print(f"Filtered components for {pop['alias']}: {len(pop['compInfo'])}")
        print("===============\n")


    if sum([len(pop['compInfo']) for pop in settingDict['population']])<0:
        print("no components found in any populations. exiting")
        exit(0)

    #######
    # extract info.
    #######

    for ext in settingDict['extraction']:
        print(f"working on ext: {ext['alias']}")
        compTestRunsInfo=[]
        for pop in settingDict['population']:
            if pop['alias'] in ext['usePopulations']:
                print(f"found pop:{pop['alias']}")

                ### get test IDs
                if "spec" in ext.keys():
                    compTestRuns=GetTestRunIDs(myClient, [x['code'] for x in pop['compInfo']], 100)
                    if len(compTestRuns)<1:
                        print(f"no test info for: {pop['alias']}")
                    else:
                        compTestRunsInfo.append(compTestRuns)
        
        ext['compTestRunsInfo']=compTestRunsInfo
        print("===============")
        print(f"testTypes for {ext['alias']} : {sum([len(ctri.keys()) for ctri in ext['compTestRunsInfo']])}")
        print("===============\n")


    ### list for matching testRuns
    for ext in settingDict['extraction']:
        print(f"working on ext: {ext['alias']}")
        matchedTestRuns=[]
        # get testTypes defined in settings
        if "spec" in ext.keys():
            if ext['spec']==None:
                print("skipping matchedTestRuns")
                matchedTestRuns=None
            elif type(ext['spec'])==type("str") and  ext['spec'].lower()=="all":
                print("use *all* for matchedTestRuns")
                for ctri in ext['compTestRunsInfo']:
                    print(ctri)
                    for k,v in ctri.items():
                        print(k)
                        matchedTestRuns.extend(v)
            elif type(ext['spec'])==type([]):
                print("use subset for matchedTestRuns")
                for tc in set([x['testCode'] for x in ext['spec']]):
                    for ctri in ext['compTestRunsInfo']:
                        try:
                            matchedTestRuns.extend(ctri[tc])
                            print(f"found testType: {tc}")
                        except KeyError:
                            print(f"no matching testType: {tc}")
                            continue
            else:
                print("don't understand extraction spec:",ext['spec'])
        else:
            matchedTestRuns=None
        ### get test run data
        if matchedTestRuns==None:
            print(f"skipping testRuns for {ext['alias']}")
        else:
            testInfo=GetTestRunsData(myClient, matchedTestRuns)
            ext['testInfo']=testInfo
            print("===============")
            print(f"testRuns for {ext['alias']}: {len(ext['testInfo'])}")
            print("===============\n")

    if sum([len(ext['testInfo']) for ext in settingDict['extraction']])<1:
        print("no testRun data found in any extractions. exiting")
        sys.exit()

    #######
    # formatting
    #######

    ### list for matching testRuns
    for ext in settingDict['extraction']:
        print(f"working on ext: {ext['alias']}")
        df_testRuns=FormatTestRunData(ext['testInfo'])
        ext['df_testRuns']=df_testRuns
        print("===============")
        print(f"df_testRuns for {ext['alias']}: {ext['df_testRuns'].columns}")
        print("===============\n")
    
    if any([ext['df_testRuns'].empty for ext in settingDict['extraction']]):
        print("no formatted data found in any extractions. exiting")
        sys.exit()


    #######
    # visualisation
    #######
    for vis in settingDict['visualisation']:
        print(f"working on vis: {vis['alias']}")
        
        df_testRuns=pd.DataFrame()
        customPlots=[]
        for ext in settingDict['extraction']:
            if ext['alias'] in vis['useExtractions']:
                print(f"found ext:{ext['alias']}")

                if "DQ" in vis.keys():
                    if df_testRuns.empty:
                        df_testRuns=ext['df_testRuns']
                    else:
                        pd.concat([df_testRuns])
                
                if "custom" in vis.keys():
                    for cus in vis['custom']:
                        # display(ext['df_testRuns'])
                        cusPlot=MakeCustomChart(ext['df_testRuns'],cus)
                        # display(cusPlot)
                        customPlots.append(cusPlot)
        
        standardPlots=DQPlotting_testRuns(myClient, df_testRuns)
        vis['standard_plots']=standardPlots
        print("===============")
        print(f"standard plots for {vis['alias']}: {len(vis['standard_plots'])}")
        print("===============\n")
                
        vis['custom_plots']=customPlots
        print("===============")
        print(f"custom plots for {vis['alias']}: {len(vis['custom_plots'])}")
        print("===============\n")


    if sum([len(vis['standard_plots']) for vis in settingDict['visualisation']])<1:
        if sum([len(vis['custom_plots']) for vis in settingDict['visualisation']])<1:
            print("no plots found in any visualisation. exiting")
            sys.exit()                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              

                    

        if not df_compInfo.empty:
            print("getting plots...")
            if "compSummary" in vis.keys():
                vis['standard_comp_plots']=ComponentSummary(myClient, df_compInfo, vis['alias'])
                print("===============")
                print(f"compSummary info. for {vis['alias']}: {len(vis['standard_comp_plots'])}")
                print("===============\n")
                
            if "yieldCalculation" in vis.keys():
                vis['standard_yield_plots']=CalcYield(myClient, df_compInfo, vis['spec'], vis['alias'], chunk)
                print("===============")
                print(f"yieldCalculation info. for {vis['alias']}: {len(vis['standard_yield_plots'])}")
                print("===============\n")

        if not df_stageInfo.empty:
            print("getting plots...")
            if "stageSummary" in vis.keys():
                vis['standard_stage_plots']=StageSummary(myClient, df_stageInfo, vis['alias'])
                vis['standard_stage_plots'].extend(StageTimeLines(myClient, df_stageInfo, vis['alias']))
                print("===============")
                print(f"stageSummary info. for {vis['alias']}: {len(vis['standard_stage_plots'])}")
                print("===============\n")
        
        if not df_testInfo.empty:
            print("getting plots...")
            if "testSummary" in vis.keys():
                vis['standard_test_plots']=TestSummary(myClient, df_testInfo, vis['alias'])
                vis['standard_test_plots'].extend(TestTimeLines(myClient, df_testInfo, vis['alias']))
                print("===============")
                print(f"testSummary info. for {vis['alias']}: {len(vis['standard_test_plots'])}")
                print("===============\n")

        
    #######
    # distribution
    #######

    for dis in settingDict['distribution']:
        print(f"working on {dis['alias']}")
        # print(dis.keys())
        specChain={'distribution':dis, 'useVisualisations':[]}
        standard_plots=[]
        custom_plots=[]
        for vis in settingDict['visualisation']:
            if vis['alias'] in dis['useVisualisations']:
                print(f"found vis: {vis['alias']}")
                for pk in [k for k in vis.keys() if "_plots" in k]:
                    ### standard plot keys
                    if "standard" in pk:
                        print(f"found {len(vis[pk])} {pk} standard plots")
                        standard_plots.extend(vis[pk])
                    ### custom plot keys
                    elif "custom" in pk:
                        print(f"found {len(vis[pk])} {pk} custom plots")
                        custom_plots.extend(vis[pk])
                    ### other?
                    else:
                        print(f"don't recognise {len(vis[pk])} {pk} plots")
        # print(json.dumps(origDict, indent=4))
        DataPaneChunk(myClient, standard_plots, custom_plots, dis, origDict)
