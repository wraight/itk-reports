# Strips Barrel Modules PRR 

Yields report for strips PRR (May 2023)

Andy's report: [here](https://docs.google.com/presentation/d/16o3-u1ye4c2LgsX-y8BYjE11OMKcIQXjwVguvys3p6E/edit#slide=id.g22df39eb97b_0_118)

---

## Strips Barrel Modules PRR Yields Report

Population definitions:
- __PowerBoards__: componentType PWB with child componentType AMAC
    - PPA: child type code TEST
    - PPB: child type code AMACSTAR
- __Hybrids__: componentType HYBRID_ASSEMBLY with child HCC 
    - PPA: child type name HCCStarv0 
    - PPB: child type name HCCStarv1
- __Modules__: componentType MODULE with child componentType SENSOR and type name ATLAS18

Extractions:
- component summary for each population
- stage summaries for each population 

Visualisations:
- component summary: per population
    - current locations
    - current stages
- stage summary: per population
    - stage history
- yields
    - Powerboards: both populations
        - parent componentType MODULE
        - not trashed
        - at stage: LOADED
        - at stage: MODULE_RCP
        - at stage: LOADED or MODULE_RCP
        - at stage: FAILED
    - Hybrids:  both populations
        - parent componentType MODULE
        - not trashed
        - at stage: ON_MODULE
        - at stage: AT_MODULE_ASSEMBLY_SITE
        - at stage: FINISHED_HYBRID
        - at stage: ON_MODULE or AT_MODULE_ASSEMBLY_SITE or FINISHED_HYBRID
        - at stage: FAILED
    - Modules:
        - not trashed
        - at stage: ON_CORE
        - at stage: AT_LOADING_SITE
        - at stage: FINISHED
        - at stage: ON_CORE or AT_LOADING_SITE or FINISHED
        - at stage: FAILED

---

## Running

From repository top directory.

For example:
> python special_editions/stripsPRR/stripsModulesPRR_script.py --file special_editions/stripsModulesPRR/stripsPRR_spec_UKChina.json 

__NB__ remember to export credentials in terminal session