### useful details

# ITk PDB access
def GetITkCredentials():
    print("Getting ITk PDB info.")
    return {'ac1':"ACCESS_TOKEN1",
            'ac2':"ACCESS_TOKEN2"}

# Distribution options
def GetDatapaneCredentials():
    print("Getting datapane info.")
    return {'token':"DATAPANE_TOKEN"}

def GetGitLabInfo():
    print("Getting gitlab info.")
    ### itk-reports-repository token
    return {'url':"https://gitlab.cern.ch",
            'token':"REPO_TOKEN",
            'id':REPO_ID,
            'branch':"BRANCH_NAME",
            'directory':"DIRECTORY"}

def EosCredentials():
    print("Getting eos info.")
    ### personal page
    return {'user':"USER_NAME",
            'password':"USER_PASSWORD",
            'directory':"EOS_DIRECTORY"
    }

def EmailCredentials():
    print("Getting email info.")
    ### personal Gmail by default
    ### goes with server: 'smtp.gmail.com', 465
    return {'email':"senderAddress",
            'pwd':"senderPassword"
            }
