### from:
### https://stackoverflow.com/questions/38193878/how-to-create-modify-a-jupyter-notebook-from-code-python
import nbformat as nbf
import sys
import os
import pprint
import argparse
import datetime

###############
### read-in parts
###############

### standard inputs
def GetArgs(descStr=None):
    my_parser = argparse.ArgumentParser(description=descStr)
    # Add the arguments
    my_parser.add_argument('--file', '-f', type=str, help='input notebook (ipynb file)')
    my_parser.add_argument('--dir','-d', type=str, help='directory for generated output')
    
    args = my_parser.parse_args()
    return args

#######
# main
#######
if __name__ == '__main__':

    ###################
    ### read inputs and define 
    ###################

    args=GetArgs('notebook builder')
    if args.file==None:
        print("No input file. Must be specified!")
        exit(0)
    else:
        print(f"Got file: {args.file}")
        exFileName=args.file
    
    if args.dir==None:
        print("No directory specied. Will use default: advanced_notebooks")
        genDir=os.getcwd()+"/advanced_notebooks/"
    else:
        genDir=args.file

    # check last directory character
    if genDir[-1]!="/":
        genDir+="/"
    
    genFileName=exFileName.replace('example','advanced')

    ###################
    ### check output notebook paths
    ###################

    if not os.path.isfile(exFileName):
        print(f"No original file found: {exFileName}")
        exit(0)

    print(f"Found input notebook: {exFileName}")

    ###################
    ### define imports for new file
    ###################

    ### get imports
    importList=[]
    print("Checking for imported packages used in notebook...")

    with open(exFileName, 'r') as exFile:
        for line in exFile.readlines():
            # print(line)
            if "import " not in line:
                continue
            # a=line.find("import ")
            # b=line[a::].find('\\n')
            # impName=line[a:a+b].replace('import','').strip()
            impName=line.strip().replace('\\n','').replace('"','').replace(',','')
            print(f" - found: {impName}")
            if "myDetails" in impName: # will be picked up later
                continue
            importList.append(impName)
   
    print(importList)


    ###################
    ### get functions list
    ###################

    funcList=[]
    print("Checking for defined functions used in notebook...")

    with open(exFileName, 'r') as exFile:
        for line in exFile.readlines():
            # print(line)
            if "def " not in line:
                continue
            a=line.find("def ")
            b=line[a::].find('(')
            funcName=line[a:a+b].replace('def','').strip()
            print(f" - found {funcName}")
            funcList.append(funcName)
    print(funcList)

    ###################
    ### compile preamble code
    ###################

    preList=["packList","itkdb.core.User","user.authenticate",'settingDict=']
    print("Checking for excutied code in notebook...")
    pre_cells=[]

    exNB=None
    ntbk = nbf.read(exFileName, nbf.NO_CONVERT)
    code_cells = [cl for cl in ntbk['cells'] if cl['cell_type']=="code"]
    print(f"found {len(code_cells)} code cells")
    # new_ntbk.cells = [cell for indx, cell in enumerate(ntbk.cells) if indx < number_cells_to_keep]

    with open(exFileName, 'r') as exFile:
        for cell in code_cells:
            # print(line)
            for pre in preList:
                if pre in cell['source']:
                    pre_cells.append(cell)

    print(f"found {len(pre_cells)} execution cells")

    ###################
    ### compile executable code
    ###################

    execList=['population','extraction','visualisation','distribution']
    print("Checking for excutied code in notebook...")
    exec_cells=[]

    exNB=None
    ntbk = nbf.read(exFileName, nbf.NO_CONVERT)
    code_cells = [cl for cl in ntbk['cells'] if cl['cell_type']=="code"]
    print(f"found {len(code_cells)} code cells")
    # new_ntbk.cells = [cell for indx, cell in enumerate(ntbk.cells) if indx < number_cells_to_keep]

    with open(exFileName, 'r') as exFile:
        for cell in code_cells:
            # print(line)
            for ed in execList:
                if f"for {ed[:3]} in " in cell['source']:
                    exec_cells.append(cell)

    print(f"found {len(exec_cells)} execution cells")

    ###################
    ### compile finctions in commonCode
    ###################

    ### commonCode directory
    ccDir=os.getcwd()+"/commonCode/"

    codeDict={}
    ### get function line positions (hopfully ordered?)
    print("Get code lines...")

    ccFiles=[f for f in  os.listdir(ccDir) if ".py" in f and "common" in f]
    print("common files: {ccFiles}")

    for cc in ccFiles:
        codeDict[cc.replace('.py','')]=[]
        with open(ccDir+'/'+cc, 'r') as ccFile:
            fileStr=ccFile.read()
            ccFile.seek(0)
            lastKey=None
            for line in ccFile.readlines():
                # print(line)
                if "def " in line:
                    fNm=line[len('def '):line.find('(')]
                    fNm=fNm.strip()
                    print(f" - found {fNm}")
                    startInd=fileStr.find(f'{line}') 
                    codeDict[cc.replace('.py','')].append(fNm)

    pprint.pprint(codeDict)    

    ### check functions in commonCode

    allFound=True

    for fl in funcList:
        print(f"Searching for: {fl}")
        found=False
        for k,v in codeDict.items():
            if fl in v:
                print(f"- found in {k}")
                found=True
        if found==False:
            print("- WAS NOT FOUND")
            allFound=False

    print("### final:",allFound)

    ###################
    ### copy, edit and write new notebook
    ###################

    print("Creating notebook...")
    nb = nbf.v4.new_notebook()
    nb['cells']=[]

    ### intro
    print(f"Copying functions from {ccDir}")
    nb['cells'].append( nbf.v4.new_markdown_cell(f"""# {genFileName.split('/')[-1].replace('.ipynb','').replace('_',' ')} \nScript-generated notebook by *notebook_builder.py*\n - Based on *{exFileName}*\n - Using functions from *{"commonCode/"}*\n - created {datetime.datetime.now()}""") )

    ### imports
    # original imports
    importStr="### copied imports \n"+"\n".join(importList)+"\n"
    # common code
    ccStr="### commonCode imports \nsys.path.insert(1, os.getcwd()+'/../commonCode') \n"
    for cd in set(codeDict.keys()):
        ccStr+="from "+cd+" import * \n"
    nb['cells'].append( nbf.v4.new_code_cell(f"""{importStr+ccStr}""") )

    ### preamble cells
    nb['cells'].extend( pre_cells )

    ### executive cells
    nb['cells'].extend( exec_cells )


    with open(genFileName, 'w') as f:
        nbf.write(nb, f)

    print("All done")
    exit(1)