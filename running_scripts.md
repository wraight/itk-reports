# Running

Scripts can be run from notebooks or commandline.

---

## Set-up the code

Follow the steps:

1. Clone the repository
> git clone https://gitlab.cern.ch/wraight/itk-reports

2. Get packages
> python -m pip install -r requirements.txt

---

## Running Notebooks

User credentials can be obtained in the notebooks:

``` python 
user = itkdb.core.User(access_code1="ACCESS_CODE1", access_code2="ACCESS_CODE2")
```

However, to make running notebooks easier and avoid copying access tokens into each notebook, a common file can be used to share the user credentials

- *myDetails_template* file in the repository top directory can be copied and editted for this purpose.

---

## Running Scripts

Set-up user PDB credentials:
> export ac1=ACCESS_CODE1
<br> export ac2=ACCESS_CODE2

Running reporting scripts from the commandline follow the pattern:
> python [reporting_script_name] --file [steering_file_name]

For example, running *testType_dashboard* report (from repository top directory):
> python reporting_scripts/testType_dashboard.py --file reporting_specs/testType_dashboard_example.json

Hopefully the output ends with:
> ### Report Spec
> Saving locally.
<br> No reportDir specified. Saving locally.
<br> App saved to ./GL_mod_met_DQ_local.html

You should now be able to open the report via a browser, e.g.:
> open ./GL_mod_met_DQ_local.html

To make changes to the steering file _on-the-fly_:
> python [reporting_script_name] --file [steering_file_name] --keys [formatted list of keys to replace (space separated)] --vals [corresponding list of values (space seperated)]

For example, changing the *reportName* (i.e. a key in the _0th_ element of _distribution_ list):
>  python reporting_scripts/testType_dashboard.py --file reporting_specs/testType_dashboard_example.json --keys distribution#0:reportName --vals thisGuy

Hopefully the output ends with:
> ### Report Spec
> Saving locally.
<br> No reportDir specified. Saving locally.
<br> App saved to ./thisGuy.html
!!! notice

	Page generated: 2023-09-04 (13:59:00.141249)

	For updates/fixes contact: wraightATcern.ch

