# Basic Notebook

Some fundamental reporting commands for reporitng on ITk Production Database (PDB).

### Required packages

- [itkdb](https://pypi.org/project/itkdb/#description)
- [pandas](https://pandas.pydata.org)


---

## Basic reporting

<!-- Reporting documentation exists elsewhere: [itk-reports]() -->

Database reporting gathers information from the database on a population of objects.

Information is retrieved via API calls to the PDB

- __NB__ requires two access codes (should be set up through [unicorn GUI](https://itkpd-test.unicorncollege.cz))

Accessing PDB is made easier using [itkdb](https://pypi.org/project/itkdb/) package (courtesy of Giordon Stark)

- several useful examples [here](https://itkdb.docs.cern.ch/0.4/examples/)


## Reporting Elements:

The basic elements:

 - define __population__, objects to interogate, e.g. pixels sensors
 - define __extraction__, parameter(s) to extract, e.g. depletion voltage
 - define __visualisation__, how to present data, e.g. table of averages per type, histogram
 - define __distribution__, how ot share the information, e.g. html

See _tutorial_examples_ for more explanation

---

## Basic counting examples

Get number of each componentTypes

### Getting componentType list

Find out things in project
- use API call: listComponentTypes
- project code: P, S, CE, CM

### Getting count per componentType 

Find out how many things in project
- use API call: getComponentCount
- loop over componentType codes

### Visualise in dataframe

Use pandas (convert dictionary into list of dictionaries)
- query by code
- query by count


---

## Basic data retrieval examples

Get components, identify tests, extract test data and visualise

### Getting component count

Find out how many things:

- use API call: getComponentCount
- project code: P, S, CE, CM
- componentType code: e.g. MODULE, SENSOR for strips, BARE_MODULE, SENSOR_TILE for pixels 


### Getting components and information

Collect component information:

- two steps:
    - Get component codes
        - use API call: listComponents
            - does not return complete component information: tests, children, parents
        - project code: P, S, CE, CM
        - componentType code: e.g. MODULE, SENSOR for strips, BARE_MODULE, SENSOR_TILE for pixels
        - additional filters optional
    - Get component information
        - use API call: getComponentBulk
            - full component information returned
        - component: list of component ids/codes/serialNumbers


### Get testRun information (not data)

Use component information to count tests and collate the _id_ of each testRun required to get data


### Get testRuns (i.e. test data)

Use testRun id to get measurement data:

- use API call: getTestRunBulk
    - full test information returned
- testRun: list of ids


### Format & Visualise

Use pandas package to wrangle data to tables


---

## Basic data visualisation

Using same testRun dataset as *basic_retrieval* example

### Simple plot - no dressing

Plot paramater values

- automatic binning

### Extract stats and dress plot

Get distribution statistics:
- total population
- mean
- limits: +/- 2σ from mean
- over&underflow population outside limits

Dress histogram using distribution statistics:
- range range from +/- 2σ from mean
- include totoal population, overflow and underflow infromation on plot

### Connected plots

Use altair's interactive chart functionality

- select histogram population from region of timeline plot


---

## Further information

PDB API: [here](https://uuapp.plus4u.net/uu-bookkit-maing01/78462435-41f76117152c4c6e947f498339998055/book/page?code=11425791)
- requires PDB access codes

ITkDB: [here](https://itkdb.docs.cern.ch/0.4/)
- includes examples section

Altair Gallery: [here](https://altair-viz.github.io/gallery/index.html)
- pretty plots
