#!/bin/bash

### build advanced notebooks

exampleDir="example_notebooks/"
outputDir="advanced_notebooks/"

for filename in $exampleDir*.ipynb
do
    echo "Working on "$filename
    python notebook_builder.py --file $filename --dir $outputDir
done
