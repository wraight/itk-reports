# Example notebooks

Notebooks with examples of report generation.

### Required packages

- [itkdb](https://pypi.org/project/itkdb/#description)
- [pandas](https://pandas.pydata.org)
- [altair](https://altair-viz.github.io)
- [datapane](https://datapane.com)

---

## Reporting examples

Example notebooks...

Dashboards:

 - **testType dashboard**: checking data quality of PDB uploads per test type

     - population: strips (S) MODULEs at Glasgow (GL)
     - extraction: 
         - testType: MODULE_METROLOGY
     - visualisation: data quality plots
     - distribution: local

 - **componentType dashboard**: checking component locations, stage progression, quality control

    - population: strips (S) MODULEs at Rutherford (RAL)
    - extraction: 
        - component summary information
        - stage summary information
        - test summary information
    - visualisation: 
        - component summary plots
        - stage summary plots
        - test summary plots
    - distribution: local

 - **inventory dashboard**: checking components at institution

    - population: strips (S) components at Oxford (OX)
    - extraction: 
        - stages
        - tests
    - visualisation: stages & tests
    - distribution: local

 - **assembly dashboard**: compile inventory of components and associated children
 
 - population: strips (S) components at Charles Univesity (CUNI)
 - extraction: 
    - stages
    - tests
 - visualisation: stages & tests
 - distribution: local

 Comparisons:
 
 - **parameter comparison**: comparing test parameters for correlations

     - population: strips (S) MODULEs at Rutherford (RAL) and child SENSORs
     - extraction: 
         - parent: MODULE_METROLOGY_>PB_POSITION_>PB_P1[0]
         - child: ATLAS18_SHAPE_METROLOGY_V1->BOWING
     - visualisation: data quality plots and comparison
     - distribution: local

 - **parameter consistency**: comparing consistency of test runs per component over production

     - population: strips (S) MODULEs at Glasgow (GL) and child SENSORs
     - extraction: 
         - parent: MODULE_IV_PS_V1->VOLTAGE, MODULE_IV_PS_V1->CURRENT
         - child: ATLAS18_IV_TEST_V1->VOLTAGE, ATLAS18_IV_TEST_V1->CURRENT
     - visualisation: per component plots (serve as data quality) and parent-child comparison
     - distribution: local

All reports can be editted using the _settingDict_ object defined in the notebook.

---

## Alerting example

Example notebook

**testType_dasboard_alert**: alert when parameters outside range

 - based on _testType dashboard_ 
 - alerts require addtional information:

    - definition of outliers (by default all (numeric) values > 2*sigma are outliers)

        - sigma: X, outliers exceed X*sigma
        - min: values _below_ minimum value
        - max: values _above_ maximum value
        - not: values which are _not_ value
        - equal: values which are _equal_ to value
    
    - (google) email address and password
    - recipient email address

<!-- ```python
def mthfunc():
    print("thing")
    return None
``` -->